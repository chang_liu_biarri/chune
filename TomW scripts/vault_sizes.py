"""
PURPOSE:
Correct the VaultSize attribute of the STRUCTURES layer in a .gdb file based on
    CHC feedback.
--------------------------------------------------------------------------------
OUTPUT:
This script (if it works...) will automatically update the attributes in the
    STRUCTURES layer.
It will create 3 new layers:
    -> 'report'    : point layer showing vaults that couldn't be fixed.
    -> 'fixed'     : point layer showing vaults that were altered.
    -> 'no_points' : polygon layer showing QA polygons that don't have a vault.
--------------------------------------------------------------------------------
BEFORE USE:
->Make sure you are using QGIS 6.1
->Import the .gdb layers for the hut/city you want to fix
->Import the 'DesignQaQcPolygon' layer
--------------------------------------------------------------------------------
Tom Witten - 21/03/2016
"""
import wayne.utils.base as W
from PyQt4.QtCore import QVariant
from collections import defaultdict
import time

start = time.clock()

print 'Setting up...'

#Create a dictionary for number of fibre endings and vault size
vault_size_dict = {0:'DV', 1:'DV', 2:'DV', 3:'DV', 4:'DV', \
                   5:'SV', 6:'SV', 7:'SV', 8:'SV', \
                   9:'LV', 10:'LV', 11:'LV', 12:'LV', \
                   13:'XLV', 14:'XLV'}

##The layers we need are DesignQaQcPolygon, STRUCUTRE, CONDUIT, FIBEREQUIPMENT,
##      SPLICECLOSURE and FDHPOINT
QA_Polygon_layer = W.get_layer_exact('DesignQaQcPolygon')
structure_layer = W.get_layer_exact('STRUCTURE')
conduit_layer = W.get_layer_exact('CONDUIT')
fiber_layer = W.get_layer_exact('FIBEREQUIPMENT')
splice_layer = W.get_layer_exact('SPLICECLOSURE')
fdh_layer = W.get_layer_exact('FDHPOINT')

##Only want to work with the design polygons that have status 'open' and the
##      title 'Vault oversized' or 'Vault undersized'.
print 'Extracting just the QA polygons we need for vault sizing...'
status_dict = defaultdict(list)
for feat in QA_Polygon_layer.getFeatures():
    status_dict[str(feat['status'])].append(feat)
open_feats = status_dict['OPEN']

title_dict = defaultdict(list)
for feat in open_feats:
    title_dict[str(feat['title'])].append(feat)
vault_list = [k for k in title_dict.iterkeys() if 'vault' in k.lower()]
sized_list = [key for key in vault_list if 'sized' in key.lower()]

QA_Features = title_dict[sized_list[0]] + title_dict[sized_list[1]]
print '    -> Found',len(QA_Features),'vault size polygons!'

##Make the spatial indexes/dictionaries we will need later
print 'Making spatial indexes...'
structure_index = QgsSpatialIndex()
for feat in structure_layer.getFeatures():
    structure_index.insertFeature(feat)
structure_dict = {f.id():f for f in structure_layer.getFeatures()}

conduit_index = QgsSpatialIndex()
conduit_dict = {}
for feat in conduit_layer.getFeatures():
    conduit_index.insertFeature(feat)
    conduit_dict[feat.id()] = feat  

fdh_index = QgsSpatialIndex()
fdh_dict = {}
for feat in fdh_layer.getFeatures():
    fdh_index.insertFeature(feat)
    fdh_dict[feat.id()] = feat

mst_index = QgsSpatialIndex()
mst_dict = {}
for feat in fiber_layer.getFeatures():
    #Not all fiber_layer features are msts ('EquipmentForm' == 'MST'):
    eq_form = feat['EquipmentForm']
    if 'MST' in eq_form:
        mst_index.insertFeature(feat)
        mst_dict[feat.id()] = feat

fap_index = QgsSpatialIndex()
fap_dict = {}
for feat in splice_layer.getFeatures():
    #Not all splice features are faps ('ClosureUse' == 'FAP' or 'FAP_MCA'):
    closure = feat['ClosureUse']
    if 'FAP' in closure:
        fap_index.insertFeature(feat)
        fap_dict[feat.id()] = feat

##Make a list of the STRUCTURE layer features that are inside a QA_Polygon
print 'Finding STRUCTURE features that have been flagged for vault size...'
flagged = []
no_points = []
for polygon in QA_Features:
    geom = polygon.geometry()
    feat_ids = structure_index.intersects(geom.boundingBox())
    if len(feat_ids) < 0.5:
        no_points.append(polygon)
    else:
        point_feats = [structure_dict[i] for i in feat_ids]
        count = 0
        for feat in point_feats:
            if geom.contains(feat.geometry()):
                flagged.append(feat)
                count += 1
        if count < 0.5:
            no_points.append(polygon)
print '    -> Found', len(flagged), 'flagged features!'

if len(flagged) < 0.5:
    print 'No flags found, aborting...'
else:
    ##For each feature in flagged, find out if there is an mst, fap and/or
    ##      an fdh there; count how many conduit features go through there; and
    ##      find out what the current vault size is.
    print 'Getting info we need to calculate correct vault size...'
    Is_mst = {}
    for st_feat in flagged:
        geom = st_feat.geometry()
        buff = geom.buffer(1e-9,5)
        mst = mst_index.intersects(buff.boundingBox())
        if len(mst) > 0.5:
            Is_mst[st_feat.id()] = 1
    print '    ->', len(Is_mst),'flagged vaults have msts in them'

    Is_fap = {}
    for st_feat in flagged:
        st_geom = st_feat.geometry()
        st_buffer = st_geom.buffer(1e-9,5)
        fap = fap_index.intersects(st_buffer.boundingBox())
        if len(fap) > 0.5:
            Is_fap[st_feat.id()] = 1
    print '    ->', len(Is_fap),'flagged vaults have faps in them'

    Is_fdh = {}
    for st_feat in flagged:
        st_geom = st_feat.geometry()
        st_buffer = st_geom.buffer(1e-9,5)
        fdh = fdh_index.intersects(st_buffer.boundingBox())
        if len(fdh) > 0.5:
            Is_fdh[st_feat.id()] = 1
    print '    ->', len(Is_fdh),'flagged vaults have fdhs in them'

    print 'Counting conduit intersections at flagged vaults...'
    conduit_counts = {}
    for feat in flagged:
        geom = feat.geometry()
        buff = geom.buffer(1e-9, 5)
        count_ids = conduit_index.intersects(buff.boundingBox())
        conduit_counts[feat.id()] = len(count_ids)

    print 'Getting current vault sizes...'
    current_vaultsize = {feat.id() : feat['VaultSize'] for feat in flagged}

    ##Create a fixed_layer and a report_layer
    fixed_layer = QgsVectorLayer("Point?crs=epsg:4326", "fixed", "memory")
    report_layer = QgsVectorLayer("Point?crs=epsg:4326", "report", "memory")

    reason = QgsField("reason", QVariant.String, "String", 60, 0, None)
    report_dp = report_layer.dataProvider()
    report_layer.startEditing()
    report_dp.addAttributes([reason])
    report_layer.updateFields()
    report_layer.commitChanges()

    ##For each feature in flag, calculate what the correct vault size is and if
    ##      necerssary, change it. Changed features will be copied to fixed_layer,
    ##      Unchanged features will be copied to report_layer with a reason
    fixed_feats = []

    structure_dp = structure_layer.dataProvider()
    report_field_index = report_dp.fields().fieldNameIndex("reason")
    size_field_index = structure_dp.fields().fieldNameIndex("VaultSize")

    print 'Calculating correct vault sizes...'
    v_sizes = ['DV','SV','LV','XLV']
    for feat in flagged:
        f_id = feat.id()
        count = conduit_counts[f_id]
        
        #if there is 14 or less conduit counts, then see what the corresponding
        #   vault size is.
        #   Otherwise, add the feat to the report_layer
        if count in range(0,15):
            conduit_size = vault_size_dict[count]
        else:
            print 'Found a vault with more than 14 fibre endings...'
            report_layer.dataProvider().addFeatures([under_feat])
            last_id = list(report_layer.getFeatures())[-1].id()
            attr = {report_field_index: str(count)+' intscts'}
            report_layer.dataProvider().changeAttributeValues({last_id:attr})

        #decide what the correct vault size actually is
        if f_id in Is_fdh:
            device_size = 'LV'
        else:
            if f_id in Is_fap or f_id in Is_mst:
                device_size = 'SV'
            else:
                device_size = 'DV'
        max_index = max(v_sizes.index(conduit_size), v_sizes.index(device_size))
        proper_vault_size = v_sizes[max_index]

        #if correct vault size is the same as the current vault size, then no
        #   fix required, just add feature to report_layer.
        #   Otherwise, fix it! And add to fixed_layer.
        current = current_vaultsize[f_id]
        current.strip()
        if proper_vault_size == current:
            report_layer.startEditing()
            report_layer.dataProvider().addFeatures([feat])
            last_id = list(report_layer.getFeatures())[-1].id()
            attr = {report_field_index:'correct size already'}
            report_layer.dataProvider().changeAttributeValues({last_id:attr})
            report_layer.commitChanges()
        else:
            attr = {size_field_index : proper_vault_size}
            structure_layer.startEditing()
            structure_layer.dataProvider().changeAttributeValues({f_id:attr})
            structure_layer.updateFields()
            structure_layer.commitChanges()
            fixed_feats.append(feat)

    #Add output layers
    no_points_layer = QgsVectorLayer("Polygon?crs=epsg:4326", "no_points", \
                                     "memory")
    no_points_layer.startEditing()
    no_points_layer.dataProvider().addFeatures(no_points)
    no_points_layer.commitChanges()

    fixed_layer.startEditing()
    fixed_layer.dataProvider().addFeatures(fixed_feats)
    fixed_layer.commitChanges()

    QgsMapLayerRegistry.instance().addMapLayer(fixed_layer)
    QgsMapLayerRegistry.instance().addMapLayer(report_layer)
    QgsMapLayerRegistry.instance().addMapLayer(no_points_layer)

end = time.clock()
total = end-start

print '\nDone!'
print 'Was able to fix',len(fixed_feats),'of',len(flagged),'flagged features.'
print 'It took',total,"seconds of your life 'that you are not getting back.'"








