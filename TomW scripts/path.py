"""
Script written by Tom Witten 17/03/2016
--------------------------------------------------------------------------------
PURPOSE:
Add strand to PATH layer in .gdb file where CHC have asked for it.
--------------------------------------------------------------------------------
OUTPUT:
This script will automatically add features to the PATH layer.
It will create 3 new polygon layers:
    -> '0 or 1 poles'   : shows qa polygons that contained less than 2 poles
    -> '2 poles'        : shows qa polygons that contained exactly 2 poles
    -> '3+ poles'       : shows qa polygons that contained more than 2 poles
--------------------------------------------------------------------------------
BEFORE USE:
->Make sure you are using QGIS 6.1
->Import the .gdb layers for the hut you want to fix
->Remove the HUTAREAS .gdb layer from the hut gdb you brough in
->You need to set the 'current_hut' value below:
->Import the 'HUT_LOCATIONS' .gdb file
->Import the 'DesignQaQcPolygon' layer
"""
from qgis.core import *
import wayne.utils.base as W
from PyQt4.QtCore import QVariant
import time

#current hut
current_hut = 'SDF113'

###Get layers we need###
QA_Polygon_layer = W.get_layer_exact('Response')
hutarea_layer = W.get_layer_exact('HUTAREA')
guyspan_layer = W.get_layer_exact('GUYSPAN')
pole_layer = W.get_layer_exact('poles_113')
path_layer = W.get_layer_exact('strand')

###Make some spatial indexes/dicts###
QA_poly_index = QgsSpatialIndex()
QA_poly_dict = {}
for feat in QA_Polygon_layer.getFeatures():
    QA_poly_index.insertFeature(feat)
    QA_poly_dict[feat.id()] = feat

guyspan_index = QgsSpatialIndex()
guyspan_dict = {}
for feat in guyspan_layer.getFeatures():
    guyspan_index.insertFeature(feat)
    guyspan_dict[feat.id()] = feat

pole_index = QgsSpatialIndex()
pole_dict = {}
for feat in pole_layer.getFeatures():
    pole_index.insertFeature(feat)
    pole_dict[feat.id()] = feat

#get the hut_carve feature for this hut
hut_list = []
for feat in hutarea_layer.getFeatures():
    if feat['HUTID'] == current_hut:
        hut_list.append(feat)
hut_feat = hut_list[0]

###Extract the QA_poly features we actually want###
#get all the QApolygons from this hut (including overlaps/close ones)
QA_box_ids = QA_poly_index.intersects(hut_feat.geometry().boundingBox())
QA_box_feats = [QA_poly_dict[i] for i in QA_box_ids]

#cut these down to ones that are actually completely within this hut
QA_hut_feats = []
for feat in QA_box_feats:
    if hut_feat.geometry().contains(feat.geometry()):
        QA_hut_feats.append(feat)

#now get just the features with the right error_code
QA_feats = []
for feat in QA_hut_feats:
    if feat['error_code'] == 'D2493':
        if feat['status'] == 'OPEN':
            QA_feats.append(feat)

#divide into 3 lists: 0-1 poles, 2 poles, 3 or more poles
zero_one_poles = []
two_poles = []
three_plus_poles=[]
for feat in QA_feats:
    pole_ids = pole_index.intersects(feat.geometry().boundingBox())
    if len(pole_ids) < 1.5:
        zero_one_poles.append(feat)
    else:
        pole_feats = [pole_dict[i] for i in pole_ids]
        final_poles = []
        for pole in pole_feats:
            if feat.geometry().contains(pole.geometry()):
                final_poles.append(pole)
        num_poles = len(final_poles)
        if num_poles < 1.5:
            zero_one_poles.append(feat)
        elif num_poles > 2.5:
            three_plus_poles.append(feat)
        else:
            two_poles.append(feat)

#make some layers showing what we have found
zero_one_layer = QgsVectorLayer("Polygon?crs=epsg:4326", "0 or 1 poles", "memory")
zero_one_layer.startEditing()
zero_one_layer.dataProvider().addFeatures(zero_one_poles)
zero_one_layer.commitChanges()
QgsMapLayerRegistry.instance().addMapLayer(zero_one_layer)

two_poles_layer = QgsVectorLayer("Polygon?crs=epsg:4326", "2 poles", "memory")
two_poles_layer.startEditing()
two_poles_layer.dataProvider().addFeatures(two_poles)
two_poles_layer.commitChanges()
QgsMapLayerRegistry.instance().addMapLayer(two_poles_layer)

three_plus_layer = QgsVectorLayer("Polygon?crs=epsg:4326", "3+ poles", "memory")
three_plus_layer.startEditing()
three_plus_layer.dataProvider().addFeatures(three_plus_poles)
three_plus_layer.commitChanges()
QgsMapLayerRegistry.instance().addMapLayer(three_plus_layer)

###Now, add aerial path to the 2pole ones###
#get field ids
CreationUser_field_index = path_layer.dataProvider().fields().fieldNameIndex("CreationUser")
LastUser_field_index = path_layer.dataProvider().fields().fieldNameIndex("LastUser")
Placement_field_index = path_layer.dataProvider().fields().fieldNameIndex("Placement")
StrandSize_field_index = path_layer.dataProvider().fields().fieldNameIndex("StrandSize")
SurfaceType_field_index = path_layer.dataProvider().fields().fieldNameIndex("SurfaceType")
InstallMethod_field_index = path_layer.dataProvider().fields().fieldNameIndex("InstallMethod")
Density_field_index = path_layer.dataProvider().fields().fieldNameIndex("Density")
Status_field_index = path_layer.dataProvider().fields().fieldNameIndex("Status")

#make the new feature/add feature to layer/add attributes
for poly in two_poles:
    #get the 2 poles inside
    all_pole_ids = pole_index.intersects(poly.geometry().boundingBox())
    all_pole_feats = [pole_dict[i] for i in all_pole_ids]
    poles = []
    for feature in all_pole_feats:
        if poly.geometry().contains(feature.geometry()):
            poles.append(feature)
    #create a line feature going between those 2 poles
    new_feat = QgsFeature()
    new_points = [p.geometry().asPoint() for p in poles]
    new_geom = QgsGeometry().fromPolyline(new_points)
    new_feat.setGeometry(new_geom)
    #add to layer
    path_layer.startEditing()
    path_layer.dataProvider().addFeatures([new_feat])
    #add attributes
    path_ids = [f.id() for f in path_layer.getFeatures()]
    last = path_ids[-1]
    attr = {CreationUser_field_index:'Paul Sulisz', LastUser_field_index:'Paul Sulisz',\
            Placement_field_index:'Aerial', StrandSize_field_index:'1_4', \
            SurfaceType_field_index:'NA', InstallMethod_field_index:'Aerial', \
            Density_field_index:'RS', Status_field_index:'PSR'} #may need more attributes
    path_layer.dataProvider().changeAttributeValues({last:attr})
    path_layer.commitChanges()
