"""
_______________________________________________________________________________
RUNNING:
Copy this script to Users/user.name/.qgis2/python

Then in the python console in qgis, type import hoa as h
Then you can run the code by typing h.run()
_______________________________________________________________________________
OUTPUT:
The code outputs a 'hoa_mdu_qa' polygon layer containing CHC polygons that
    the script found error(s) in. The features will have the attribute
    'details' explaining what the script found.

There may also be a 'nolead' point layer which highlights address features that
    should have a leadin but don't.
_______________________________________________________________________________
PROCESS:
Currently, this is what the code looks for:

ERROR: | CHECK:
J0101   ------------
J0102   checking for one leadin per address inside polygon, and exactly one address per parcel
J0103   checking that each polygon contains addresses in just one parcel, and that there is just one leadin in the polygon
J0104   checking that each polygon has exactly one leadin
J0105   checking that each polygon has exactly one leadin
J0106   checking that number of leadins == number of addresses, and that only ug_leadins are used
J0107   ------------
_______________________________________________________________________________
Tom Witten, 31/03/16
_______________________________________________________________________________
"""
from qgis.core import *
import wayne.utils.base as W
from PyQt4.QtCore import QVariant

def run():
    """SET UP"""
    #list that will be used to update attributes
    check_feats = []

    #get layers we need to do stuff
    original_poly_layer = W.get_layer('mdu_hoa_feed_method_')
    poly_layer = W.copy_layer(original_poly_layer, 'hoa_mdu_qa')
    poly_layer.commitChanges()

    address_layer = W.get_layer_exact('addresses')
    parcels_layer = W.get_layer_exact('parcels')
    cand_edges_layer = W.get_layer_exact('candidate_edges')

    #create details field and add to layer
    details_field = QgsField("details", QVariant.String, "String", 254, 0, None)
    poly_layer.startEditing()
    poly_layer.dataProvider().addAttributes([details_field])
    poly_layer.updateFields()
    poly_layer.commitChanges()
    field_index = poly_layer.dataProvider().fields().fieldNameIndex("details")

    #spatial indexes
    address_index = QgsSpatialIndex()
    address_dict = {}
    for feat in address_layer.getFeatures():
        address_index.insertFeature(feat)
        address_dict[feat.id()] = feat

    parcels_index = QgsSpatialIndex()
    parcels_dict = {}
    for feat in parcels_layer.getFeatures():
        parcels_index.insertFeature(feat)
        parcels_dict[feat.id()] = feat

    leadin_index = QgsSpatialIndex()
    leadin_dict = {}
    for feat in cand_edges_layer.getFeatures():
        if 'UG_LEADIN' in feat['TYPE']:
            leadin_index.insertFeature(feat)
            leadin_dict[feat.id()] = feat
        elif 'AER_LEADIN' in feat['TYPE']:
            leadin_index.insertFeature(feat)
            leadin_dict[feat.id()] = feat

    #map addresses to polygons
    polys_to_adds = {}
    for poly in poly_layer.getFeatures():
        polys_to_adds[poly.id()] = []
        add_ids = address_index.intersects(poly.geometry().boundingBox())
        for a_id in add_ids:
            if poly.geometry().contains(address_dict[a_id].geometry()):
                polys_to_adds[poly.id()].append(address_dict[a_id])

    #classify curation polys
    (J0101,J0102,J0103,J0104,J0105,J0106,J0107) = ([],[],[],[],[],[],[])
    for feat in poly_layer.getFeatures():
        if 'J0101' in feat['error_code']:
            J0101.append(feat)
        elif 'J0102' in feat['error_code']:
            J0102.append(feat)
        elif 'J0103' in feat['error_code']:
            J0103.append(feat)
        elif 'J0104' in feat['error_code']:
            J0104.append(feat)
        elif 'J0105' in feat['error_code']:
            J0105.append(feat)
        elif 'J0106' in feat['error_code']:
            J0106.append(feat)
        elif 'J0107' in feat['error_code']:
            J0107.append(feat)
        else:
            print 'either there is an unidentified error_code, OR:'
            print '...this shit is not working'
    ################################################################################
    """J0101 CHECKING"""

    ################################################################################
    """J0102 CHECKING"""
    if len(J0102) > 0.5:
        for poly_feat in J0102:
            check = False
            reason = 'J0102:'
            poly_geom = poly_feat.geometry()
            
            #get addresses within poly_feat
            adds_in_poly = polys_to_adds[poly_feat.id()]
            
            #get parcels within poly_feat
            parcel_ids_near_poly = parcels_index.intersects(poly_geom.boundingBox())
            parcels_in_poly = []
            if len(parcel_ids_near_poly) > 0.5:
                for parcel_id in parcel_ids_near_poly:
                    parcel_feat = parcels_dict[parcel_id]
                    if poly_geom.intersects(parcel_feat.geometry()):
                        parcels_in_poly.append(parcel_feat)
            
            #check that there is one address per parcel
            mult_adds = []
            if len(parcels_in_poly) > 0.5:
                for parcel in parcels_in_poly:
                    count = 0
                    near_ids = address_index.intersects(parcel.geometry().boundingBox())
                    near_feats = [address_dict[i] for i in near_ids]
                    if len(near_feats) > 0.5:
                        for address in near_feats:
                            if parcel.geometry().contains(address.geometry()):
                                count += 1
                    if count > 1.5:
                        mult_adds.append(parcel)
                if len(mult_adds) > 0.5:
                    check = True
                    reason = reason + ' ' + str(len(mult_adds))
                    reason = reason + ' parcel(s) have multiple addresses.'
            
            #check that there is one drop per address
            no_drops = []
            if len(adds_in_poly) > 0.5:
                for feat in adds_in_poly:
                    geom = feat.geometry()
                    buffer = geom.buffer(1e-6, 5)
                    leadins=leadin_index.intersects(buffer.boundingBox())
                    if len(leadins) < 0.5:
                        no_drops.append(feat)
                if len(no_drops) > 0.5:
                    check = True
                    reason = reason + ' ' +str(len(no_drops))
                    reason = reason + ' addresse(s) have no leadins.'

            #add results to check_feats
            if check:
                check_feats.append((poly_feat,reason))

    ################################################################################
    """J0103 CHECKING"""
    if len(J0103) > 0.5:
        for poly_feat in J0103:
            check = False
            reason = 'J0103:'
            
            #get addresses under poly_feat
            adds = polys_to_adds[poly_feat.id()]
            
            #see how many different parcels the adds are in
            parcels = []
            leadins = []
            if len(adds) > 0.5:
                for address in adds:
                    parcel_id = parcels_index.intersects(address.geometry().buffer(1e-7,5).boundingBox())
                    parcel_feat = parcels_dict[parcel_id[0]]
                    leadin_ids = leadin_index.intersects(address.geometry().buffer(1e-7,5).boundingBox())
                    if len(leadin_ids) > 0.5:
                        for lead in leadin_ids:
                            if leadin_dict[lead] not in leadins:
                                leadins.append(leadin_dict[lead])
                    count = len(leadins)
                    if parcel_feat in parcels:
                        pass
                    else:
                        parcels.append(parcel_feat)
                if len(parcels) > 1.5:
                    check = True
                    reason = reason + ' polygon contains addresses in multiple parcels.'
                else:
                    #see how many leadins there are
                    if count > 1.5:
                        check = True
                        reason = reason + ' more than one leadin.'
            
            #output
            if check:
                check_feats.append((poly_feat,reason))

    ################################################################################
    """J0104 CHECKING"""
    if len(J0104) > 0.5:
        for poly_feat in J0104:
            check = False
            reason = 'J0104:'

            geom = poly_feat.geometry()
            leadin_ids = leadin_index.intersects(geom.boundingBox())
            count = 0
            if len(leadin_ids) > 0.5:
                for i in leadin_ids:
                    feat = leadin_dict[i]
                    end_points = feat.geometry().asPolyline()
                    if geom.contains(end_points[0]):
                        count += 1
                    elif geom.contains(end_points[-1]):
                        count += 1

            if count < 0.5:
                check = True
                reason += ' no leadin under polygon'
            elif count > 1.5:
                check = True
                reason += ' more than one leadin under polygon'

            if check:
                check_feats.append((poly_feat,reason))

    ################################################################################
    """J0105 CHECKING"""
    if len(J0105) > 0.5:
        for poly_feat in J0105:
            check = False
            reason = 'J0105:'
            
            geom = poly_feat.geometry()
            leadin_ids = leadin_index.intersects(geom.boundingBox())
            count = 0
            if len(leadin_ids) > 0.5:
                for i in leadin_ids:
                    feat = leadin_dict[i]
                    end_points = feat.geometry().asPolyline()
                    if geom.contains(end_points[0]):
                        count += 1
                    elif geom.contains(end_points[-1]):
                        count += 1

            if count < 0.5:
                check = True
                reason += ' no leadin under polygon'
            elif count > 1.5:
                check = True
                reason += ' more than one leadin under polygon'

            if check:
                check_feats.append((poly_feat,reason))

    ################################################################################
    """J0106 CHECKING"""
    for poly_feat in J0106:
        check = False
        reason = 'J0106:'
        
        #get number of addresses
        addresses = polys_to_adds[poly_feat.id()]
        if len(addresses) > 0.5:
            adds = [a for a in addresses if a['address_ty'] != 'NA']
        else:
            adds = []
        num_adds = len(adds)
        
        #get leadins
        leadins = []
        address_no_lead = []
        if len(addresses) > 0.5:
            for address in adds:
                buff = address.geometry().buffer(1e-7,5)
                lead_ids = leadin_index.intersects(buff.boundingBox())
                if len(lead_ids) > 0.5:
                    for i in lead_ids:
                        lead = leadin_dict[i]
                        if lead not in leadins:
                            leadins.append(lead)
                else:
                    address_no_lead.append(address)
            num_leads = len(leadins)
        else:
            num_leads = 0
        
        if num_adds != num_leads:
            check = True
            reason = reason + ' number of addresses not equal to number of leadins.'
        if len(leadins) > 0.5:
            for lead in leadins:
                if 'AER_LEADIN' in lead['TYPE']:
                    check = True
                    reason = reason + ' aer_leadins in use'
        
        if check:
            check_feats.append((poly_feat,reason))

    ################################################################################
    """FINALISE OUTPUT"""
    unwanted = ['OBJECTID','feature_id','global_id','anno_id','type','weight',\
                'status','quantity','history_0','history_1','history_2',\
                'history_3','history_4','Shape_Leng','Shape_Area']
    delete_me = [poly_layer.dataProvider().fields().fieldNameIndex(name) \
                 for name in unwanted]
    attr_map = {}

    safe = [f[0].id() for f in check_feats]

    kill_me = [f.id() for f in poly_layer.getFeatures() if f.id() not in safe]

    poly_layer.startEditing()
    if len(check_feats) > 0.5:
        for combo in check_feats:
            f=combo[0]
            d=combo[1]
            attr_map[f.id()] = {field_index:d}
        poly_layer.dataProvider().changeAttributeValues(attr_map)

    poly_layer.dataProvider().deleteFeatures(kill_me)

    poly_layer.dataProvider().deleteAttributes(delete_me)
    poly_layer.commitChanges()

    if len(address_no_lead) > 0.5:
        id_field = QgsField("ID", QVariant.String, "String", 254, 0, None)
        nolead_layer = QgsVectorLayer("Point?crs=epsg:4326", "nolead", "memory")
        nolead_layer.startEditing()
        nolead_layer.dataProvider().addAttributes([id_field])
        nolead_layer.updateFields()
        nolead_layer.dataProvider().addFeatures(address_no_lead)
        id_index = nolead_layer.dataProvider().fields().fielNameIndex("ID")
        id_map = {f.id() : {id_index : f.id()} for f in nolead_layer.getFeatures()}
        nolead_layer.dataProvider().changeAttributeValues(attr_map)
        nolead_layer.updateFields()
        nolead_layer.commitChanges()
        QgsMapLayerRegistry.instance().addMapLayer(nolead_layer)
