from bncommon.spatial.shapelyutil import RegionTree
import logging
from bncommon import reporting
from shapely.geometry import LineString,Point
from bncommon.gis.gistypes import Feature
from fileloaders.common_files_loader import CommonFilesLoader
import pyproj
from bncommon.gis import gisio
from lib_features.transform import (
    transform_features,
    multi_part_features_to_single,
    transform_geometry
)

def build_node_map(features):
    node_map = {}
    for i,feature in enumerate(features,0):
        node_map[i] = feature

    node_tree = RegionTree([feature.geom for feature in features])
    return node_tree, node_map

def linestring_points(linestring):
    return [Point(point) for point in linestring.coords[:]]

def find_nearby_nodes(node_tree,other_features,buffer=None):
    nearby_nodes = []

    for feature in other_features:
        if buffer:
            geom = feature.geom.buffer(buffer)
        else:
            geom = feature.geom
        hits = node_tree.query_region(geom)
        nearby_nodes.append(hits)
    return nearby_nodes

def coord_equality(coord1,coord2):
    return coord1[0]==coord2[0] and coord1[1]==coord2[1]

def remove_duplicate_coords(coords):
    new_coords = []
    for i in range(len(coords)-1):
        if not coord_equality(coords[i],coords[i+1]):
            new_coords.append(coords[i])
    new_coords.append(coords[-1])

    if len(new_coords)<2:
        message = "Snapping compacted a linestring into zero length"
        logging.error(message)
        reporting.error(message,LineString(coords))
        return []
    else:
        return new_coords

def snap_coordinates(node_tree,node_map,other_features,radius):
    new_features = []

    nearby_nodes = find_nearby_nodes(node_tree,other_features,radius)

    for nearby, feature in zip(nearby_nodes,other_features):
        points = linestring_points(feature.geom)
        nearby_points = [node_map[i].geom for i in nearby]
        candidate_moves = []
        new_coord = []
        for point in points:
            candidate_move = []
            for nearby_point in nearby_points:
                r  = point.distance(nearby_point)
                if r<=radius:
                    candidate_move.append([r,nearby_point])

            candidate_moves.append(sorted(candidate_move))

        for point,move in zip(points,candidate_moves):
            if move:
                new_coord.extend(move[0][1].coords[:])
            else:
                new_coord.extend(point.coords[:])

        new_coord = remove_duplicate_coords(new_coord)
        if new_coord:
            new_features.append(Feature(LineString(new_coord),feature.data))

    return new_features

def splitfeatures(node_features,line_feautures):
    node_list = [node.geom.coords[0] for node in node_features]
    new_features = []
    for feature in line_feautures:
        new_lines = []
        coords = feature.geom.coords[:]
        if len(coords) == 2:
            new_features.append(feature)
        elif len(coords) >2:
            j = 0
            for i in range(1,len(coords)-1):
                if coords[i] in node_list:
                        new_feature = Feature(LineString(coords[j:i+1]),feature.data)
                        new_features.append(new_feature)
                        j = i
            new_feature = Feature(LineString(coords[j:i+2]),feature.data)
            new_features.append(new_feature)

    return new_features

def enforce_coord_near_node(node_tree,node_map,line_features,radius):
    nearby_nodes = find_nearby_nodes(node_tree,line_features,radius)
    new_line_features = []
    for nearby,feature in zip(nearby_nodes, line_features):
        line = feature.geom
        points = linestring_points(line)

        for node in nearby:
            hit = False
            node_geom  = node_map[node].geom
            for point in points:
                distance = abs(node_geom.distance(point))
                if distance<radius:
                    hit=True
                    break

            if hit == False:
                intersection = line.difference(node_geom.buffer(radius))
                coords = []
                for geom in intersection.geoms:
                    coords.extend(geom.coords[:])
                line = LineString(coords)

        new_line_features.append(Feature(line,feature.data))

    return new_line_features

def splitnearpoints(node_features,line_features,radius,split=False):
    node_tree, node_map = build_node_map(node_features)
    if split:
        line_features = enforce_coord_near_node(node_tree,node_map,line_features,radius)
    line_features = snap_coordinates(node_tree,node_map,line_features,1.0001*radius)
    split_feature_list = splitfeatures(node_features,line_features)
    return split_feature_list


def test_snapping():
    a = LineString([(0,0),(0.01,0.0),(0.5,0.5),(1.0,1.0)])
    line_a = Feature(a,{})
    line_features = [line_a]

    b = Feature(Point([(0,0)]), {})
    c = Feature(Point([0.56,0.56]),{})
    d = Feature(Point([0.8,0.7]),{})

    node_features = [b,c,d]

    split_lines = splitnearpoints(node_features,line_features,0.1,False)
    assert len(split_lines) == 2

def test_splitting():
    a = LineString([(0,0),(0.01,0.0),(0.5,0.5),(1.0,1.0)])
    line_a = Feature(a,{})
    line_features = [line_a]

    b = Feature(Point([(0,0)]), {})
    c = Feature(Point([0.56,0.56]),{})
    d = Feature(Point([0.72,0.74]),{})

    node_features = [b,c,d]

    split_lines = splitnearpoints(node_features,line_features,0.1,True)
    assert len(split_lines) == 3


def transform_design(projector, design, inverse=False):
    def transform(features):
        original_length = len(features)
        features = multi_part_features_to_single(features)
        features = transform_features(projector, features, inverse=inverse)
        features = list(features)

        if len(features) != original_length:
            raise ValueError("I can't handle true multi-part geometries in my input!")

        return features

    return {k: transform(v) for k, v in design.iteritems()}

def fix_null_fields(features):
    for feature in features:
        for field,data in feature.data.iteritems():
            if data is None:
                feature.data[field] = ''


def run_splitnearpoints():
    dir = ""
    nodes = "poles_refactored.shp"
    lines = "path_AER.shp"
    radius = 0.1
    loader= CommonFilesLoader

    with loader(dir) as ld:
        ld.load_layer(nodes,"nodes")
        ld.load_layer(lines,"lines")

    design = ld()
    line_layer_configs = design.lines
    design_features = {'lines':design.lines['features'],'nodes':design.nodes['features']}
    projection = "+proj=merc"
    projector = pyproj.Proj(projection.encode('utf-8'))

    data = transform_design(projector, design_features)

    node_features = data['nodes']
    line_features = data['lines']
    fix_null_fields(line_features)
    line_features = splitnearpoints(node_features,line_features,radius,split=True)
    #output the new lines
    output_lines = list(transform_features(projector,line_features,inverse=True))
    line_layer_configs['features'] = output_lines

    line_layer_configs['filename'] = '/aer_spans.shp'
    line_layer_configs['name'] = 'aer_spans'
    gisio.write_layer(line_layer_configs,output_formats=['SHP'],filename='aer_spans.shp')

    return

if __name__ == '__main__':
    print 'run split near points ...'
    run_splitnearpoints()
    print 'Done'
