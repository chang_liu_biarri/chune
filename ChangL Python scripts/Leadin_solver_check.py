import os, json

directory = 'C:\\Users\\chang\\Documents\\Solve\\ONUG_Southaven\\output_leadin\\archive\obj'

for dirpath,_,filenames in os.walk(directory):
    for f in filenames:
        if f == 'input_data.json':
            with open(os.path.abspath(os.path.join(dirpath,f))) as ff:
                inputj = json.load(ff)
                dem = inputj.get('demand_groupings','no info')
                dem_num = len(dem)
                
                #args = inputj.get('args','no info')
                
                idg = inputj.get('input_design_graph','no info')
                n1 = idg['nodes']
                nodes_num = len(n1)
                l1 = idg['links']
                lead_num = len(l1)
                solver = inputj.get('solver','no info')
