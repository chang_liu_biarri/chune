import numpy as np
from osgeo import gdal
from osgeo import gdal_array
from osgeo import osr
import matplotlib.pylab as plt
import ogr
import math
from shapely.geometry import Point
from shapely.geometry import Polygon

array = np.array(( (0.1, 0.2, 0.3, 0.4),
                   (0.2, 0.3, 0.4, 0.5),
                   (0.3, 0.4, 0.5, 0.6),
                   (0.4, 0.5, 0.6, 0.7),
                   (0.5, 0.6, 0.7, 0.8) ))
# My image array      
lat = np.array(( (10.0, 10.0, 10.0, 10.0),
                 ( 9.5,  9.5,  9.5,  9.5),
                 ( 9.0,  9.0,  9.0,  9.0),
                 ( 8.5,  8.5,  8.5,  8.5),
                 ( 8.0,  8.0,  8.0,  8.0) ))
lon = np.array(( (20.0, 20.5, 21.0, 21.5),
                 (20.0, 20.5, 21.0, 21.5),
                 (20.0, 20.5, 21.0, 21.5),
                 (20.0, 20.5, 21.0, 21.5),
                 (20.0, 20.5, 21.0, 21.5) ))
# For each pixel I know it's latitude and longitude.
# As you'll see below you only really need the coordinates of
# one corner, and the resolution of the file.

xmin,ymin,xmax,ymax = [lon.min(),lat.min(),lon.max(),lat.max()]
nrows,ncols = np.shape(array)
xres = (xmax-xmin)/float(ncols)
yres = (ymax-ymin)/float(nrows)
geotransform=(xmin,xres,0,ymax,0, -yres)   
# That's (top left x, w-e pixel resolution, rotation (0 if North is up), 
#         top left y, rotation (0 if North is up), n-s pixel resolution)
# I don't know why rotation is in twice???

output_raster = gdal.GetDriverByName('GTiff').Create('myraster.tif',ncols, nrows, 1 ,gdal.GDT_Float32)  # Open the file
output_raster.SetGeoTransform(geotransform)  # Specify its coordinates
srs = osr.SpatialReference()                 # Establish its coordinate encoding
srs.ImportFromEPSG(4326)                     # This one specifies WGS84 lat long.
                                             # Anyone know how to specify the 
                                             # IAU2000:49900 Mars encoding?
output_raster.SetProjection( srs.ExportToWkt() )   # Exports the coordinate system 
                                                   # to the file
output_raster.GetRasterBand(1).WriteArray(array)   # Writes my array to the raster



# 1. Define rows and columns of pixels and NoData_value of new raster 
NoData_value = -9999
cols = 30
rows = 30

pixel_size = 1

# 2. Filenames for input and output 
input = r"C:\Users\chang\Downloads\hsv hut point\OnePoly.shp"
output = r"C:\Users\chang\Downloads\hsv hut point\OnePoly1.tif"

# 3. Open Shapefile
source_ds = ogr.Open(input)
source_layer = source_ds.GetLayer()
x_min, x_max, y_min, y_max = source_layer.GetExtent()

# driver = ogr.GetDriverByName("ESRI Shapefile")
# dataSource = driver.Open(input,0)
# layer = dataSource.GetLayer()

for feature in source_layer:  # Python has to read layer using a for loop?! #
    geom = feature.GetGeometryRef()
    
    

# 4. pixel size
x_res = (x_max-x_min)/float(rows)
y_res = (y_max-y_min)/float(cols)
#geotransform = (x_min,x_res,0,y_max,0, -y_res)   
geotransform = (x_min,x_res,1,y_min,0, y_res) 
# That's (top left x, w-e pixel resolution, rotation (0 if North is up), 
#         top left y, rotation (0 if North is up), n-s pixel resolution)
# I don't know why rotation is in twice???
# 5. Define center and true distance
cen_cols = cols/2
cen_rows = rows/2

def true_dist(point1, point2):
    # Haversine formula
    dLat = math.radians(point2.GetY()) - math.radians(point1.GetY())
    dLon = math.radians(point2.GetX()) - math.radians(point1.GetX())
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(math.radians(point1.GetY())) * math.cos(math.radians(point2.GetY())) * math.sin(dLon/2) * math.sin(dLon/2)
    distance = 6371 * 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    return distance

# 6. Find FSPL
FSPL = np.zeros((rows,cols))
freq = 2

for r in range(rows):
    row_diff = abs(cen_rows - r - 1)
    for c in range(cols):
        col_diff = abs(cen_cols - c - 1)
        P1 = ogr.Geometry(ogr.wkbPoint)
        P1.AddPoint(x_min + r* x_res, y_min + c* y_res)
        P2 = ogr.Geometry(ogr.wkbPoint)
        P2.AddPoint(x_min + (cen_rows - 1) * x_res, y_min + (cen_cols-1)* y_res)
        #P1 = Point(x_min + r* x_res, y_min + c* y_res)
        #P2 = Point(x_min + (cen_rows - 1) * x_res, y_min + (cen_cols-1)* y_res)
        #dist_km = math.sqrt((row_diff * x_res)**2 + (col_diff * y_res)**2)/1000
        dist_km = true_dist(P1,P2)
        if (dist_km == 0):
            FSPL[r,c] = 0
        else: 
            FSPL[r,c] = 20 * math.log10(dist_km) + 20 * math.log10(freq) + 92.45
        # elif geom.Contains(P1):
            # FSPL[r,c] = 20 * math.log10(dist_km) + 20 * math.log10(freq) + 92.45
        # else:
            # FSPL[r,c] = NoData_value
            
            
geotransform = (x_min,x_res,0,y_min,0, y_res) 
output = r"C:\Users\chang\Downloads\hsv hut point\OnePoly5.tif"
#output_raster = gdal.GetDriverByName('GTiff').Create(output,rows, cols, 1 ,gdal.GDT_Float32)  # Open the file
output_raster = gdal.GetDriverByName('GTiff').Create(output,cols, rows, 1 ,gdal.GDT_Float32)
output_raster.SetGeoTransform(geotransform)  # Specify its coordinates
srs = osr.SpatialReference()                 # Establish its coordinate encoding
srs.ImportFromEPSG(4326)                     # This one specifies WGS84 lat long.
output_raster.SetProjection( srs.ExportToWkt() )   # Exports the coordinate system                                                    # to the file
output_raster.GetRasterBand(1).WriteArray(FSPL)   # Writes my array to the raster

# 7. Close the Tiff 
output_raster = None





from osgeo import gdal, ogr
import numpy

# Define pixel_size and NoData value of new raster
pixel_size = 1e-5
NoData_value = -9999

# 2. Filenames for input and output 
input = r"C:\Users\chang\Downloads\hsv hut point\OnePoly.shp"
output = r"C:\Users\chang\Downloads\hsv hut point\OnePoly1.tif"
cen_cols = cols/2
cen_rows = rows/2
# Filename of input OGR file
vector_fn = input

# Filename of the raster Tiff that will be created
raster_fn = output

# Open the data source and read in the extent
source_ds = ogr.Open(vector_fn)
source_layer = source_ds.GetLayer()
x_min, x_max, y_min, y_max = source_layer.GetExtent()

# Create the destination data source
x_res = int((x_max - x_min) / pixel_size)
y_res = int((y_max - y_min) / pixel_size)
target_ds = gdal.GetDriverByName('GTiff').Create(raster_fn, x_res, y_res, 1, gdal.GDT_Byte)
target_ds.SetGeoTransform((x_min, pixel_size, 0, y_max, 0, -pixel_size))
band = target_ds.GetRasterBand(1)
band.SetNoDataValue(NoData_value)

# 6. Find FSPL
rows = x_res
cols = y_res
FSPL = np.zeros((rows,cols))
freq = 2

for r in range(rows):
    row_diff = abs(cen_rows - r - 1)
    for c in range(cols):
        col_diff = abs(cen_cols - c - 1)
        P1 = ogr.Geometry(ogr.wkbPoint)
        P1.AddPoint(x_min + r* pixel_size, y_max - c* pixel_size)
        P2 = ogr.Geometry(ogr.wkbPoint)
        P2.AddPoint(x_min + (cen_rows - 1) * pixel_size, y_max - (cen_cols-1)* pixel_size)
        #P1 = Point(x_min + r* x_res, y_min + c* y_res)
        #P2 = Point(x_min + (cen_rows - 1) * x_res, y_min + (cen_cols-1)* y_res)
        #dist_km = math.sqrt((row_diff * x_res)**2 + (col_diff * y_res)**2)/1000
        dist_km = true_dist(P1,P2)
        if (dist_km == 0):
            FSPL[r,c] = 0
        elif geom.Intersects(P1):
            FSPL[r,c] = 20 * math.log10(dist_km) + 20 * math.log10(freq) + 92.45
        else:
            FSPL[r,c] = NoData_value
            
            
            
# Rasterize
gdal.RasterizeLayer(target_ds, [1], source_layer, burn_values=[0])
band.WriteArray(FSPL)
target_ds = None







input1 = r"C:\Users\chang\Downloads\hsv hut point\OnePolyKyle.tif"







