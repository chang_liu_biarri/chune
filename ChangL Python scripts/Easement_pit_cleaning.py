###  Python version of easement pit cleaning, for ALLO market ###
!workon Chang
from shapely.geometry import Point,LineString, Polygon, mapping,shape
from bncommon.gis.gisio import read_layer, create_layer, write_layer, read_features
import os
from fiona import collection
from osgeo import ogr


UG_link = r'C:\Users\chang\Documents\Solve\LNCC03\FsamData\LNCC03_02\DesignLayers\dual_sided_02.shp'
EASEPIT_link = r'C:\Users\chang\Documents\Solve\LNCC03\FsamData\LNCC03_02\DesignLayers\easement_pits_02.shp'
PARCEL_link = r'C:\Users\chang\Documents\Solve\LNCC03\FsamData\LNCC03_02\DesignLayers\parcels_02.shp'
LEADIN_link = r'C:\Users\chang\Documents\Solve\LNCC03\FsamData\LNCC03_02\DesignLayers\output_leadins\leadins.shp'
driver = ogr.GetDriverByName('ESRI Shapefile')


## Read in Shapefiles through Fiona ##
with collection(UG_link,'r') as input:
    for line in input:
        UGGEOM = shape(line['geometry'])  # one feature only 


c = fiona.open(UG_link)
line = c.next()
UG_geom = shape(line['geometry'])


UG = read_layer(UG_link)
UG = read_features(UG_link)


dataSource1 = driver.Open(UG_link,0)
dataSource2 = driver.Open(EASEPIT_link,0)
dataSource3 = driver.Open(PARCEL_link,0)
dataSource4 = driver.Open(LEADIN_link,0)



UG = dataSource1.GetLayer()
EASEPIT = dataSource2.GetLayer()
PARCEL = dataSource3.GetLayer()
LEADIN = dataSource4.GetLayer()

for feature in UG:
    UG_fid =  feature.GetField('FID')
    UG_geom = feature.geometry()

for feature1 in EASEPIT:
    #UG_fid =  feature.GetField('FID')
    EASEPIT_geom = feature1.geometry()
   
for feature in PARCEL:
    #UG_fid =  feature.GetField('FID')
    PARCEL_geom = feature.geometry()
    
for feature in LEADIN:
    #UG_fid =  feature.GetField('FID')
    LEADIN_geom = feature.geometry()


bufferDistance = 1/60/6075
UGBuf = UG_geom.Buffer(bufferDistance)

UG[1].geometry().Distance(UGBuf)

