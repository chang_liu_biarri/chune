###  Testing some shapely functionalities ###
from shapely.geometry import LineString, Point]
from pprint import pprint
## CUT LINES ##
def cut(line, distance):
    # Cuts a line in two at a distance from its starting point
    if distance <= 0.0 or distance >= line.length:
        return [LineString(line)]
    coords = list(line.coords)
    for i, p in enumerate(coords):
        pd = line.project(Point(p))
        if pd == distance:
            return [
                LineString(coords[:i+1]),
                LineString(coords[i:])]
        if pd > distance:
            cp = line.interpolate(distance)
            return [
                LineString(coords[:i] + [(cp.x, cp.y)]),
                LineString([(cp.x, cp.y)] + coords[i:])]
                
line =  LineString([(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0)]) 
pprint([list(x.coords) for x in cut(line, 2.5)])
