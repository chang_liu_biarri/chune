import numpy as np
from osgeo import gdal
from osgeo import gdal_array
from osgeo import osr
import matplotlib.pylab as plt
import ogr
import math
from shapely.geometry import Point
from shapely.geometry import Polygon

# 1. Define rows and columns of pixels and NoData_value of new raster 
NoData_value = -9999
cols = 30
rows = 30

pixel_size = 1

# 2. Filenames for input and output 
input = r"C:\Users\chang\Downloads\hsv hut point\OnePoly.shp"
output = r"C:\Users\chang\Downloads\hsv hut point\OnePoly1.tif"

# 3. Open Shapefile
source_ds = ogr.Open(input)
source_layer = source_ds.GetLayer()
x_min, x_max, y_min, y_max = source_layer.GetExtent()



for feature in source_layer:  # Python has to read layer using a for loop?! #
    geom = feature.GetGeometryRef()
    
    

# 4. pixel size
x_res = (x_max-x_min)/float(rows)
y_res = (y_max-y_min)/float(cols)
#geotransform = (x_min,x_res,0,y_max,0, -y_res)   
geotransform = (x_min,x_res,1,y_min,0, y_res) 
# That's (top left x, w-e pixel resolution, rotation (0 if North is up), 
#         top left y, rotation (0 if North is up), n-s pixel resolution)
# I don't know why rotation is in twice???


# 5. Define center and true distance

cen_cols = cols/2
cen_rows = rows/2

def true_dist(point1, point2):
    # Haversine formula
    dLat = math.radians(point2.GetY()) - math.radians(point1.GetY())
    dLon = math.radians(point2.GetX()) - math.radians(point1.GetX())
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(math.radians(point1.GetY())) * math.cos(math.radians(point2.GetY())) * math.sin(dLon/2) * math.sin(dLon/2)
    distance = 6371 * 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    return distance

# 6. Find FSPL
FSPL = np.zeros((rows,cols))
freq = 2

for r in range(rows):
    row_diff = abs(cen_rows - r - 1)
    for c in range(cols):
        col_diff = abs(cen_cols - c - 1)
        P1 = ogr.Geometry(ogr.wkbPoint)
        P1.AddPoint(x_min + r* x_res, y_min + c* y_res)
        P2 = ogr.Geometry(ogr.wkbPoint)
        P2.AddPoint(x_min + (cen_rows - 1) * x_res, y_min + (cen_cols-1)* y_res)
        #P1 = Point(x_min + r* x_res, y_min + c* y_res)
        #P2 = Point(x_min + (cen_rows - 1) * x_res, y_min + (cen_cols-1)* y_res)
        #dist_km = math.sqrt((row_diff * x_res)**2 + (col_diff * y_res)**2)/1000
        dist_km = true_dist(P1,P2)
        if (dist_km == 0):
            FSPL[r,c] = 0
        else: 
            FSPL[r,c] = 20 * math.log10(dist_km) + 20 * math.log10(freq) + 92.45
        # elif geom.Contains(P1):
            # FSPL[r,c] = 20 * math.log10(dist_km) + 20 * math.log10(freq) + 92.45
        # else:
            # FSPL[r,c] = NoData_value
            
            
geotransform = (x_min,x_res,0,y_min,0, y_res) 


output_raster = gdal.GetDriverByName('GTiff').Create(output,rows, cols, 1 ,gdal.GDT_Float32)
output_raster.SetGeoTransform(geotransform)  # Specify its coordinates
srs = osr.SpatialReference()                 # Establish its coordinate encoding
srs.ImportFromEPSG(4326)                     # This one specifies WGS84 lat long.
output_raster.SetProjection( srs.ExportToWkt() )   # Exports the coordinate system                                                    # to the file
output_raster.GetRasterBand(1).WriteArray(FSPL)   # Writes my array to the raster

# 7. Close the Tiff 
output_raster = None