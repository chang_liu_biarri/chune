import numpy as np
from osgeo import gdal
from osgeo import gdal_array
from osgeo import osr
import matplotlib.pylab as plt
import ogr
import math
from shapely.geometry import Point
from shapely.geometry import Polygon
import subprocess

### 1. Make Polygon to Tiff ###
subprocess.call("gdal_rasterize -tr 0.00001 0.00001 -burn 255 -l boundary boundary.shp boundary.tif")

### 2. Read in Tiff ###
input1 = r"C:\Users\chang\Downloads\qld_clutter\boundary.tif"
input_shp = r"C:\Users\chang\Downloads\qld_clutter\boundary.shp"

source_ds = ogr.Open(input_shp)
source_layer = source_ds.GetLayer()
x_min, x_max, y_min, y_max = source_layer.GetExtent()
    
tiff = gdal.Open(input1)
tiff_array = tiff.ReadAsArray()

rows = tiff.RasterYSize
cols = tiff.RasterXSize

### 4. pixel size
x_res = (x_max-x_min)/float(rows)
y_res = (y_max-y_min)/float(cols)

cen_cols = cols/2
cen_rows = rows/2

def true_dist(point1, point2):
    # Haversine formula
    dLat = math.radians(point2.GetY()) - math.radians(point1.GetY())
    dLon = math.radians(point2.GetX()) - math.radians(point1.GetX())
    a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(math.radians(point1.GetY())) * math.cos(math.radians(point2.GetY())) * math.sin(dLon/2) * math.sin(dLon/2)
    distance = 6371 * 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    return distance

# 6. Find FSPL
FSPL = np.zeros((rows,cols))
freq = 2

for r in range(rows):
    row_diff = abs(cen_rows - r - 1)
    for c in range(cols):
        col_diff = abs(cen_cols - c - 1)
        P1 = ogr.Geometry(ogr.wkbPoint)
        P1.AddPoint(x_min + r* x_res, y_max - c* y_res)
        P2 = ogr.Geometry(ogr.wkbPoint)
        P2.AddPoint(x_min + (cen_rows - 1) * x_res, y_max - (cen_cols-1)* y_res)
        #P1 = Point(x_min + r* x_res, y_min + c* y_res)
        #P2 = Point(x_min + (cen_rows - 1) * x_res, y_min + (cen_cols-1)* y_res)
        #dist_km = math.sqrt((row_diff * x_res)**2 + (col_diff * y_res)**2)/1000
        dist_km = true_dist(P1,P2)
        if (dist_km == 0):
            FSPL[r,c] = 0
        # else: 
            # FSPL[r,c] = 20 * math.log10(dist_km) + 20 * math.log10(freq) + 92.45
        elif tiff_array[r][c] != 0:
            FSPL[r,c] = 20 * math.log10(dist_km) + 20 * math.log10(freq) + 92.45
        else:
            FSPL[r,c] = tiff_array[r][c]

output = r"C:\Users\chang\Downloads\qld_clutter\Boundary1.tif"


def array2raster(newRasterfn,rasterOrigin,pixelWidth,pixelHeight,array):
    cols = array.shape[1]
    rows = array.shape[0]
    originX = rasterOrigin[0]
    originY = rasterOrigin[1]
    driver = gdal.GetDriverByName('GTiff')
    outRaster = driver.Create(newRasterfn, cols, rows, 1, gdal.GDT_Byte)
    outRaster.SetGeoTransform((originX, pixelWidth, 0, originY, 0, pixelHeight))
    outband = outRaster.GetRasterBand(1)
    outband.WriteArray(array)
    outRasterSRS = osr.SpatialReference()
    outRasterSRS.ImportFromEPSG(4326)
    outRaster.SetProjection(outRasterSRS.ExportToWkt())
    outband.FlushCache()


def main(newRasterfn,rasterOrigin,pixelWidth,pixelHeight,array):
    reversed_arr = array[::-1] # reverse array so the tif looks like the array
    array2raster(newRasterfn,rasterOrigin,pixelWidth,pixelHeight,reversed_arr) # convert array to raster

rasterOrigin = (x_min,y_min)
pixelWidth = 1e-5
pixelHeight = 1e-5
newRasterfn = output    
main(newRasterfn,rasterOrigin,pixelWidth,pixelHeight,FSPL)    










