###########################################################
### SNAPFLOW, Chang's version ###
from bncommon.spatial.shapelyutil import RegionTree
import logging
#from bncommon import reporting
from shapely.geometry import LineString,Point,Polygon, base

from bncommon.gis.gisio import read_layer, create_layer, write_layer
import os
from collections import defaultdict
import pandas as pd
#from rtree import index


## Set working directory ##
work_directory = r"C:\\Users\\chang\\Documents\\Solve\\A00H65C\\raw\\prepped_data"


pole_file = 'pole.shp'
address_file = 'addresses.shp'
pit_file = 'vault.shp'
parcel_file = 'parcels.shp'
leadin_file = 'leadins_manual.shp'

pole_path = os.path.join(work_directory, pole_file)
address_path = os.path.join(work_directory, address_file)
pit_path = os.path.join(work_directory, pit_file)
parcel_path = os.path.join(work_directory, parcel_file)
leadin_path = os.path.join(work_directory,leadin_file)


## Read layers ##
pole_layer = read_layer(pole_path)
address_layer = read_layer(address_path)
pit_layer = read_layer(pit_path)
parcel_layer = read_layer(parcel_path)
leadin_layer = read_layer(leadin_path)

## Read features in each layer ##
pole = pole_layer.features
address = address_layer.features
pit = pit_layer.features
parcel = parcel_layer.features
leadin = leadin_layer.features

## Combine pole and pits into 1 layer ##
def pole_and_pit(pole, pit):
    service_location = []
    for feat in pole:
        #feat.data = {'id': None}
        service_location.append(feat)
    for feat in pit:
        service_location.append(feat)
    return service_location

## Set up spatial index ##
#idx = index.Index()

def build_node_map(features):
    node_map = {}
    for i,feature in enumerate(features,0):
        node_map[i] = feature

    node_tree = RegionTree([feature.geom for feature in features])
    return node_tree, node_map

def linestring_points(linestring):
    return [Point(point) for point in linestring.coords[:]]

def find_nearby_nodes(node_tree,other_features,buffer1=None):
    nearby_nodes = []

    for feature in other_features:
        if buffer1:
            geom = feature.geom.buffer(buffer1)
        else:
            geom = feature.geom
        hits = node_tree.query_region(geom)
        nearby_nodes.append(hits)
    return nearby_nodes
    
    
ser_loc = pole_and_pit(pole,pit)
ser_loc_tree, ser_loc_map = build_node_map(ser_loc) 
par_tree, par_map = build_node_map(parcel)
lead_tree, lead_map = build_node_map(leadin)
addie_tree, addie_map = build_node_map(address)
#lead_point = linestring_points(leadin.geom)
nearby_parcel_location =  find_nearby_nodes(par_tree, leadin, buffer1 = 10.0/60/6075)
nearby_addie = find_nearby_nodes(addie_tree, leadin, buffer1 = 0.01/60/6075)
def snap_point_generate(nearby_parcel_location, nearby_addie, leadin):
    new_snap_points = []
    for feat in leadin:
        for near_par_loc in nearby_parcel_location:
            intersection_point = []
            intersection_line = []
            for i, item in enumerate(near_par_loc):
                ## pop only outputs the first element of the set ##
                one_addie = address[nearby_addie[i].pop()]
                one_parcel = parcel[item]
                parcel_ring = LineString(list(one_parcel.geom.exterior.coords))
                intsct = feat.geom.intersection(parcel_ring)
                inside_leadin = feat.geom.intersection(one_parcel.geom)
                if intsct and one_addie.geom.within(one_parcel.geom):
                    intersection_point.append(intsct)
                    intersection_line.append(inside_leadin)
            for j, f1 in enumerate(intersection_point):
                buf1 = f1.buffer(1.0/60/6075)
                buf1_ring = LineString(list(buf1.geom.exterior.coords))
                snap_point = intersection_line[j].intersection(buf1_ring)
                if snap_point:
                    new_snap_points.append(snap_point)
                
SNAP_POINT_MOVED = snap_point_generate(nearby_parcel_location,nearby_addie, leadin)



