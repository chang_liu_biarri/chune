### rpy2 template ###

# Import R itermm #

import rpy2.robjects as robjects 

# Import packages #
from rpy2.robjects.packages import importr
 
base = importr('base')
utils = importr('utils')
MASS = importr('MASS')
 

# Use an R object #
pi = robjects.r['pi']
print pi
pi[0]
# Addition with R objects #
pi0plus2 = pi[0] + 2
print pi0plus2

# Use an R function. Remember, it has to be a character string. #
robjects.r('''
        # create a function `f`
        f <- function(r, verbose=FALSE) {
            if (verbose) {
                cat("I am calling f().\n")
            }
            2 * pi * r
        }
        # call the function `f` with argument value 3
        f(3)
        ''')
# To call this function, use one of the following two. #
r_f = robjects.r['f']
r_ff = robjects.globalenv['f']
print r_ff.r_repr()


# Interpolating R objects into R code strings. #
letters = robjects.r['letters']
rcode = 'paste(%s,collapse = "-")'%(letters.r_repr())
res = robjects.r(rcode)
print res 

# Creating rpy2 vectors #
res = robjects.StrVector(['abc','def'])
res1 = robjects.r("c('abcd','efgh')")

res = robjects.IntVector([1,2,3])
# as.numeric will give a FloatVector, instead of an IntVector #
res1 = robjects.r("as.numeric(1:3)")
res.r_repr()

res = robjects.FloatVector([1.1,2.2,3.3])
res1 = robjects.r("c(1.1,2.2,3.3)")
print(res.r_repr())

v = robjects.FloatVector([1.1,2.2,3.3,4.4,5.5,6.6])
m = robjects.r['matrix'](v, ncol = 2)



# Plots #
import rpy2.robjects as robjects
r = robjects.r
x = robjects.IntVector(range(10))
y = r.rnorm(10)
r.X11()

r.layout(r.matrix(robjects.IntVector([1,2,3,2]), nrow=2, ncol=2))
r.plot(r.runif(10), y, xlab="runif", ylab="foo/bar", col="red")


# Linear model #
from rpy2.robjects import FloatVector
from rpy2.robjects.packages import importr
stats = importr('stats')
base = importr('base')

ctl = FloatVector([4.17,5.58,5.18,6.11,4.50,4.61,5.17,4.53,5.33,5.14])
trt = FloatVector([4.81,4.17,4.41,3.59,5.87,3.83,6.03,4.89,4.32,4.69])
group = base.gl(2, 10, 20, labels = ["Ctl","Trt"])
weight = ctl + trt

robjects.globalenv["weight"] = weight
robjects.globalenv["group"] = group
lm_D9 = stats.lm("weight ~ group")
print(stats.anova(lm_D9))

# omitting the intercept
lm_D90 = stats.lm("weight ~ group - 1")
print(base.summary(lm_D90))
print(lm_D9.names)
print(lm_D9.rx('coefficients'))

# PCA analysis #
import rpy2.robjects as robjects

r = robjects.r

m = r.matrix(r.rnorm(100), ncol=5)
pca = r.princomp(m)
r.plot(pca, main="Eigen values")
r.biplot(pca, main="biplot")






