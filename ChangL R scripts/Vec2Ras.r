#vector layer to raster layer #
library(sp)
library(rgdal)
library(rgeos)
library(raster)
library(geosphere)
library(ncdf4)

pol = readOGR("C:\\Users\\chang\\Downloads\\hsv hut point", layer = 'OnePoly',stringsAsFactors = F)

pixels_row = 30
pixels_col = 30
r = raster(ncol = pixels_col, nrow = pixels_row)
extent(r) = extent(pol)
pol_ras = rasterize(pol, r, "Shape_Area")
old_val = pol_ras@data@values
new_val = old_val

# #pol_ras@data@values = runif(900,min = 0, max = 10)
# for ( i in 1 : 900)
# {
#   if(!is.na(old_val[i]))
#   {
#     #new_val[i] = runif(1,min = 1, max = 2)
#     new_val[i] = 0
#   }
# }
# pol_ras@data@values = new_val
# pol_ras@data@values[450:490] = 1000
# writeRaster(pol_ras,"C:\\Users\\chang\\Downloads\\hsv hut point\\ras2", overwrite = T,format = 'HFA')




### place a point at a pixel in the middle - call it the transmitter ###
### calculate free space path loss from this point to all other pixel centroid and store the value for each pixel##
tr_loc = 465 # row 16, col 15
cen_row = 16
cen_col = 15
dist_hor = distm(c(pol_ras@extent@xmin,pol_ras@extent@ymin),
                     c(pol_ras@extent@xmax,pol_ras@extent@ymin),
                     fun = distHaversine)
dist_ver = distm(c(pol_ras@extent@xmin,pol_ras@extent@ymin),
                 c(pol_ras@extent@xmin,pol_ras@extent@ymax),
                 fun = distHaversine)

dist_cell_hor = as.numeric(dist_hor)/pixels_row
dist_cell_ver = as.numeric(dist_ver)/pixels_col


#distm(c(-96.72351,40.86656), c(-96.7211,40.86813), fun = distHaversine)
FSPL = matrix(0, nrow = pixels_row, ncol = pixels_col)
frequency = 2 #unit GHz
for (r1 in 1: pixels_row)
{
  row_diff = abs(cen_row - r1)
  col_diff = c()
  for (c1 in 1: pixels_col)
  {
    col_diff = abs(cen_col - c1)
    dist_km = sqrt((row_diff*dist_cell_hor)^2 + (col_diff*dist_cell_ver)^2)/1000
    # FSPL formula #
    FSPL[r1,c1] = 20* log10(dist_km) + 20 * log10(frequency) + 92.45
  } 
}
FSPL_vec = as.vector(FSPL)
FSPL_vec[which(FSPL_vec <0)] = 0

for ( i in 1 : 900)
{
  if(!is.na(old_val[i]))
  {
    #new_val[i] = runif(1,min = 1, max = 2)
    new_val[i] = FSPL_vec[i]
  }
}
pol_ras@data@values= new_val
writeRaster(pol_ras,"C:\\Users\\chang\\Downloads\\hsv hut point\\ras2", overwrite = T,format = 'HFA')



### Read in gcd ###
auss = raster("C:\\Users\\chang\\Downloads\\qld_clutter\\Brisbane_TerraClass20.grc")
newproj = "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84
+towgs84=0,0,0"

AusNew = auss
projection(AusNew) = newproj
### Write out Tiff ###
writeRaster(auss,"C:\\Users\\chang\\Downloads\\hsv hut point\\BT00", overwrite = T,format='GTiff')

writeRaster(AusNew,"C:\\Users\\chang\\Downloads\\hsv hut point\\BT", overwrite = T,format='GTiff')


