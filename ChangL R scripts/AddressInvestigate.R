library(sp)
library(rgdal)
library(rgeos)

SATADD = readOGR("C:\\Users\\chang\\Documents\\City Data\\SAT_20160421_FULL_GID.gdb", layer = 'ADDRESS',stringsAsFactors = F)
dim(SATADD)
length(unique(SATADD@data$AddressID))
unique.id = sort(unique(as.character(SATADD@data$AddressID)))
SATADDID = as.character(SATADD@data$AddressID)
No.Add = c()
for ( i in 1: length(unique.id))
{
  No.Add[i] = length(which(SATADDID == unique.id[i]))
}
sat.df = data.frame(sat.df = SATADDID)
Nr.of.dup = aggregate(x = sat.df, by = list(unique.id = sat.df$sat.df), FUN = length)

small_group = sort(unique(substr(SATADDID, start = 1, stop = 4)))
dup.id = c()
dup.num.instance = c()
for ( j in 1: length(small_group))
{
  loc = which(substr(SATADDID, start = 1, stop = 4) == small_group[j])
  Section = SATADDID[loc]
  Nr.of.dup = aggregate(x = Section, by = list(unique.id = Section), FUN = length)
  dup.loc = which(Nr.of.dup[,2] > 1)
  dup.id = c(dup.id, Nr.of.dup[dup.loc,1])
  dup.num.instance = c(dup.num.instance, Nr.of.dup[dup.loc,2])
}

res.df = data.frame(dup.id = dup.id, dup.num.instance = dup.num.instance)
#write.csv(res.df,'C:\\Users\\chang\\Documents\\City Data\\Some Investigation\\SAT_Dup_Addie.csv')
res.df = read.csv('C:\\Users\\chang\\Documents\\City Data\\Some Investigation\\SAT_Dup_Addie.csv')



SATFIB = readOGR("C:\\Users\\chang\\Documents\\City Data\\SAT_20160421_FULL_GID.gdb", layer = 'FIBERCABLE',stringsAsFactors = F)
#fib.drop = SATFIB[SATFIB@data$Category == 'DROP',]
Addin = c()
for (d1 in 1: length(res.df$dup.id))
{
  Addie.loc = which(SATADD@data$AddressID == res.df$dup.id[d1])
  Addin = c(Addin,Addie.loc)
}
add.dup = SATADD[Addin,]
writeOGR(add.dup,dsn = "C:\\Users\\chang\\Documents\\City Data\\Some Investigation", layer = 'DUP_ADDIE_SAT', driver = 'ESRI Shapefile')

fib = readOGR("C:\\Users\\chang\\Documents\\City Data\\Some Investigation", layer = 'FIB_ON_DUP',stringsAsFactors = F)

fib.buf = gBuffer(fib,width = 0.5/60/6075, byid = T)
Dup = c()
for (f1 in 1: dim(add.dup)[1])
{
  Buffer = gBuffer(add.dup[f1,],width = 5/60/6075)
  #fib.loc = gIntersects(fib.buf,Buffer, byid = T)
  fib.loc = gContains(fib.buf,add.dup[f1,], byid = T)
  fiblog = as.logical(fib.loc)
  fibin = fib[fiblog,]
  if(dim(fibin)[1] == 0)
  {
    Dup[f1] = 0
  }else{
    Hutname = substr(fibin@data$Cablename, start = 1, stop = 6)
    if(length(unique(Hutname)) == 1)
    {
      Dup[f1] = 0
    }else{
      Dup[f1] = length(unique(Hutname)) - 1
    }
  }
}

DUP.df = data.frame(addid = add.dup@data$AddressID, Dup)
dup = aggregate(x = DUP.df, by = list(unique.id = as.character(DUP.df$addid)), FUN = mean)
dup = aggregate(x = DUP.df,  FUN = length)

No.Huts.Served = c()
for(k1 in 1: dim(res.df)[1])
{
  loc = which(DUP.df$addid == res.df$dup.id[k1])
  No.Huts.Served[k1] = ifelse(DUP.df$Dup[loc[1]] == 0, 1, DUP.df$Dup[loc[1]]+1)
}
res.df$No.Huts.Served = No.Huts.Served
names(res.df) = c('id','ADDRESSID','Number of addresses','Number of huts served by')
write.csv(res.df[,-1],'C:\\Users\\chang\\Documents\\City Data\\Some Investigation\\SAT_Report.csv')


#####################################################################
########### SFO DROP LENGTH CHECK ###################################
SFOFIB = readOGR("C:\\Users\\chang\\Documents\\Solve\\SFO_Lewis_0818\\SFO118UG_preCRO_20160818.gdb", layer = 'FIBERCABLE',stringsAsFactors = F)
drop = SFOFIB[SFOFIB@data$Category == 'DROP',]
droplength = SpatialLinesLengths(drop,longlat = F) * 60 * 6075
droplR = SpatialLinesLengths(drop,longlat = T) * 3280.84 
range(droplength)
dropsort = sort(droplength)
tail(dropsort,100)
drop.comp = data.frame(CalculatedLength = drop@data$CalculatedLength, Actual = droplength)
drop.comp$Diff = drop.comp$ActualR - drop.comp$CalculatedLength
drop.comp$ActualR = droplR

length(which(drop@data$CalculatedLength > 300))





AUSaddNN= readOGR("C:\\Users\\chang\\Documents\\Solve\\AUS112", layer = 'addresses_gf',stringsAsFactors = F)
AUSaddGDB= readOGR("C:\\Users\\chang\\Documents\\Solve\\AUS112\\AUS_biarri_gid_20160817.gdb", layer = 'ADDRESS',stringsAsFactors = F)
AUSHUTGDB= readOGR("C:\\Users\\chang\\Documents\\Solve\\AUS112\\AUS_biarri_gid_20160817.gdb", layer = 'HUTAREA',stringsAsFactors = F)

AUS112AREA = AUSHUTGDB[AUSHUTGDB@data$HUTID == 'AUS112',]

add.loc = gContains(AUS112AREA,AUSaddGDB, byid = T)
AUSaddGDB.small = AUSaddGDB[as.vector(add.loc),]

names(AUSaddNN@data)
names(AUSaddGDB.small@data)

AddOld = AUSaddNN
#AddOld@data = AddOld@data[,c(37,12,)]

AUSaddNN@data$AddressID = AUSaddNN@data$address_id
test_point = FileCompare(Old.Data =AUSaddNN, New.Data = AUSaddGDB.small, Geo.Focus = T, Moved.Feature.ID = 'AddressID')
writeOGR(test_point$Moved, dsn = "C:\\Users\\chang\\Documents\\Solve\\AUS112", layer = 'Moved_Addie', driver = 'ESRI Shapefile')
writeOGR(test_point$Removed, dsn = "C:\\Users\\chang\\Documents\\Solve\\AUS112", layer = 'Removed_Addie', driver = 'ESRI Shapefile')
writeOGR(test_point$Added, dsn = "C:\\Users\\chang\\Documents\\Solve\\AUS112", layer = 'Added_Addie', driver = 'ESRI Shapefile')



### proj issue ###
library(maptools)
library(rgdal)

library(sp)
library(rgdal)
library(rgeos)
cond = readOGR("C:\\Users\\chang\\Downloads\\tim", layer = 'Conduit Geo Line',stringsAsFactors = F)
fib = readOGR("C:\\Users\\chang\\Downloads\\tim", layer = 'Fiber Cable Geo Line',stringsAsFactors = F)


COND = spTransform(cond, CRS('+proj=longlat +datum=WGS84 +no_defs+
                             ellps=WGS84 +towgs84=0,0,0'))

FIBER = spTransform(fib, CRS('+proj=longlat +datum=WGS84 +no_defs+
                             ellps=WGS84 +towgs84=0,0,0'))

writeOGR(COND,dsn = 'C:\\Users\\chang\\Downloads\\tim', layer = 'Conduit_CP', driver = 'ESRI Shapefile')
writeOGR(COND,dsn = 'C:\\Users\\chang\\Downloads\\tim', layer = 'FIBER_CP', driver = 'ESRI Shapefile')


