library(sp)
library(rgdal)
library(rgeos)
library(readxl)
#mir_np  = readOGR("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS",layer = "sl_to_copperpair_2BOT", stringsAsFactors = F)
mir_new  = readOGR("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS",layer = "sl_to_copperpair_2SIL", stringsAsFactors = F)
#mon_np  = readOGR("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS",layer = "sl_to_copperpair_mon_np", stringsAsFactors = F)
#mon_new  = readOGR("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS",layer = "sl_to_copperpair_mon_new", stringsAsFactors = F)

#MIR_NP_STC = mir_np@data
MIR_NEW_STC= mir_new@data
cup_no_cps_mir = MIR_NEW_STC[which(MIR_NEW_STC$cupid != '' & MIR_NEW_STC$copper_path_status == ''),]
unique(substr(cup_no_cps_mir$cupid, start = 1, stop = 6))

mir_s11 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/HUHL_NPAMS_20170203.xlsx", skip = 2, sheet = 1, col_names = T)
mir_s12 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/HUHL_NPAMS_20170203.xlsx", skip = 2, sheet = 2, col_names = T)
mir_s13 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/HUHL_NPAMS_20170203.xlsx", skip = 2, sheet = 3, col_names = T)

#mon_s11 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/MON_NPAMS/MONA_NPAMS_20161230.xlsx", skip = 2, sheet = 1, col_names = T)
#mon_s12 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/MON_NPAMS/MONA_NPAMS_20161230.xlsx", skip = 2, sheet = 2, col_names = T)
#mon_s13 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/MON_NPAMS/MONA_NPAMS_20161230.xlsx", skip = 2, sheet = 3, col_names = T)

mir_s11_s = mir_s11[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
mir_s12_s = mir_s12[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
mir_s13_s = mir_s13[,c("PAIR_STAT","CUUNIQUEPATHID")]
names(mir_s13_s) = c("PLT_SUMM_STAT","CUUNIQUEPATHID")

#mon_s11_s = mon_s11[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
#mon_s12_s = mon_s12[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
#mon_s13_s = mon_s13[,c("PAIR_STAT","CUUNIQUEPATHID")]
#names(mon_s13_s) = c("PLT_SUMM_STAT","CUUNIQUEPATHID")

mir_s21 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/LIDC_NPAMS_20161230.xlsx", skip = 2, sheet = 1, col_names = T)
mir_s22 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/LIDC_NPAMS_20161230.xlsx", skip = 2, sheet = 2, col_names = T)
mir_s23 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/LIDC_NPAMS_20161230.xlsx", skip = 2, sheet = 3, col_names = T)

#mon_s21 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/MON_NPAMS/NARR_NPAMS_20161230.xlsx", skip = 2, sheet = 1, col_names = T)
#mon_s22 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/MON_NPAMS/NARR_NPAMS_20161230.xlsx", skip = 2, sheet = 2, col_names = T)
#mon_s23 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/MON_NPAMS/NARR_NPAMS_20161230.xlsx", skip = 2, sheet = 3, col_names = T)

mir_s21_s = mir_s21[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
mir_s22_s = mir_s22[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
mir_s23_s = mir_s23[,c("PAIR_STAT","CUUNIQUEPATHID")]
names(mir_s23_s) = c("PLT_SUMM_STAT","CUUNIQUEPATHID")


mir_s31 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/HOME_NPAMS_20170209.xlsx", skip = 2, sheet = 1, col_names = T)
mir_s32 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/HOME_NPAMS_20170209.xlsx", skip = 2, sheet = 2, col_names = T)
mir_s33 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/HOME_NPAMS_20170209.xlsx", skip = 2, sheet = 3, col_names = T)
mir_s31_s = mir_s31[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
mir_s32_s = mir_s32[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
mir_s33_s = mir_s33[,c("PAIR_STAT","CUUNIQUEPATHID")]
names(mir_s33_s) = c("PLT_SUMM_STAT","CUUNIQUEPATHID")

mir_s41 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/SILV_NPAMS_20161230.xlsx", skip = 2, sheet = 1, col_names = T)
mir_s42 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/SILV_NPAMS_20161230.xlsx", skip = 2, sheet = 2, col_names = T)
mir_s43 = read_excel("C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/NPAMS/SILV_NPAMS_20161230.xlsx", skip = 2, sheet = 3, col_names = T)
mir_s41_s = mir_s41[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
mir_s42_s = mir_s42[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
mir_s43_s = mir_s43[,c("PAIR_STAT","CUUNIQUEPATHID")]
names(mir_s43_s) = c("PLT_SUMM_STAT","CUUNIQUEPATHID")

#mon_s21_s = mon_s21[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
#mon_s22_s = mon_s22[,c("PLT_SUMM_STAT","CUUNIQUEPATHID")]
#mon_s23_s = mon_s23[,c("PAIR_STAT","CUUNIQUEPATHID")]
#names(mon_s23_s) = c("PLT_SUMM_STAT","CUUNIQUEPATHID")

#mir_s1 = rbind(mir_s11_s,mir_s12_s,mir_s13_s)
mir_s1 = rbind(mir_s11_s,mir_s12_s,mir_s13_s,mir_s21_s,mir_s22_s,mir_s23_s,
               mir_s31_s,mir_s32_s,mir_s33_s,mir_s41_s,mir_s42_s,mir_s43_s)
#mon_s1 = rbind(mon_s11_s,mon_s12_s,mon_s13_s,mon_s21_s,mon_s22_s,mon_s23_s)






#MON_NP_STC = mon_np@data
#MON_NEW_STC= mon_new@data

# count unique sl_id #
length(unique(MIR_NP_STC$sl_id))
length(unique(MIR_NEW_STC$sl_id))
length(unique(MON_NP_STC$sl_id))
length(unique(MON_NEW_STC$sl_id))

# count unique cupid #
length(unique(MIR_NP_STC$cupid))
length(unique(MIR_NEW_STC$cupid))
length(unique(MON_NP_STC$cupid))
length(unique(MON_NEW_STC$cupid))

# count features with no cupid #
length(which(MIR_NP_STC$cupid == ''))
length(which(MIR_NEW_STC$cupid == ''))
length(which(MON_NP_STC$cupid == ''))
length(which(MON_NEW_STC$cupid == ''))

# count features with no copper_path_status #
length(which(MIR_NP_STC$copper_path_status == ''))
length(which(MIR_NEW_STC$copper_path_status == ''))
length(which(MON_NP_STC$copper_path_status == ''))
length(which(MON_NEW_STC$copper_path_status == ''))

# count features with no cupid and no copper_path_status #
length(which(MIR_NP_STC$cupid == '' & MIR_NP_STC$copper_path_status == ''))
length(which(MIR_NEW_STC$cupid == '' & MIR_NEW_STC$copper_path_status == ''))
length(which(MON_NP_STC$cupid == '' & MON_NP_STC$copper_path_status == ''))
length(which(MON_NEW_STC$cupid == '' & MON_NEW_STC$copper_path_status == ''))

# count features with correct cupid and no copper_path_status #
length(which(MIR_NP_STC$cupid != '' & MIR_NP_STC$copper_path_status == ''))
length(which(MIR_NEW_STC$cupid != '' & MIR_NEW_STC$copper_path_status == ''))
length(which(MON_NP_STC$cupid != '' & MON_NP_STC$copper_path_status == ''))
length(which(MON_NEW_STC$cupid != '' & MON_NEW_STC$copper_path_status == ''))

# count features with no cupid and correct copper_path_status #
length(which(MIR_NP_STC$cupid == '' & MIR_NP_STC$copper_path_status != ''))
length(which(MIR_NEW_STC$cupid == '' & MIR_NEW_STC$copper_path_status != ''))
length(which(MON_NP_STC$cupid == '' & MON_NP_STC$copper_path_status != ''))
length(which(MON_NEW_STC$cupid == '' & MON_NEW_STC$copper_path_status != ''))

# match features with correct cupid but no copper_path_status to NPAMS #
cup_no_cps_mir = MIR_NEW_STC[which(MIR_NEW_STC$cupid != '' & MIR_NEW_STC$copper_path_status == ''),]
#cup_no_cps_mon = MON_NEW_STC[which(MON_NEW_STC$cupid != '' & MON_NEW_STC$copper_path_status == ''),]

### FUNCTION to generate csv for each NPAMS sheet ###
Ingest_NPAMS = function(cup_no_cps_mir, mir_s1)
{
  sl_id_ctm = cup_no_cps_mir$sl_id
  cup_to_match = cup_no_cps_mir$cupid
  match_loc = rep(0, length(cup_to_match))
  match_number = rep(0, length(cup_to_match))
  match_status = rep(0, length(cup_to_match))
  for ( i1 in 1: length(cup_to_match))
  {
    tmp_loc = which(mir_s1$CUUNIQUEPATHID == cup_to_match[i1])
    
    if(length(tmp_loc) > 0)
    {
      match_number[i1] = length(tmp_loc)
    }
    if (length(tmp_loc) == 1)
    {
      match_loc[i1] = tmp_loc
    }
    if (length(tmp_loc) >= 1)
    {
      if(length(unique(mir_s1$PLT_SUMM_STAT[tmp_loc])) == 1)
      {
        match_status[i1] = unique(mir_s1$PLT_SUMM_STAT[tmp_loc])
        
      }else{
        match_status[i1] = paste(mir_s1$PLT_SUMM_STAT[tmp_loc], collapse = ',')
      }
      match_loc[i1] = paste(tmp_loc, collapse = ',')
    }  
  }
  
  o1 = cbind(sl_id_ctm, cup_to_match, match_loc, match_status)
  o1
}

#BOT_NPAMS = Ingest_NPAMS(cup_no_cps_mir = cup_no_cps_mir, mir_s1 = mir_s1)

MIR_NPAMS = Ingest_NPAMS(cup_no_cps_mir = cup_no_cps_mir, mir_s1 = mir_s1)
#MON_NPAMS = Ingest_NPAMS(cup_no_cps_mir = cup_no_cps_mon, mir_s1 = mon_s1)
### FUNCTION to clean PLT_SUMM_STAT ###
CLEAN_PLT_SUMM_STAT = function(NPAMS)
{
  c_row = which(grepl('C',NPAMS[,"match_status"]))
  NPAMS[c_row,"match_status"] = 'C'
  zero_row = which(NPAMS[,"match_status"] == 0)
  NPAMS[zero_row,"match_status"] = 'X'
  shrink_status = substr(NPAMS[,"match_status"], start = 1, stop = 1)
  NPAMS[,"match_status"] = shrink_status
  
  NPAMS
}

SIL_NPAMS_CLEAN = CLEAN_PLT_SUMM_STAT(NPAMS = MIR_NPAMS)
HUH_NPAMS_CLEAN = CLEAN_PLT_SUMM_STAT(NPAMS = MIR_NPAMS)
HOR_NPAMS_CLEAN = CLEAN_PLT_SUMM_STAT(NPAMS = MIR_NPAMS)
CRO_NPAMS_CLEAN = CLEAN_PLT_SUMM_STAT(NPAMS = MIR_NPAMS)
CMO_NPAMS_CLEAN = CLEAN_PLT_SUMM_STAT(NPAMS = MIR_NPAMS)
BOT_NPAMS_CLEAN = CLEAN_PLT_SUMM_STAT(NPAMS = BOT_NPAMS)
MIR_NPAMS_CLEAN = CLEAN_PLT_SUMM_STAT(NPAMS = MIR_NPAMS)
MON_NPAMS_CLEAN = CLEAN_PLT_SUMM_STAT(NPAMS = MON_NPAMS)

write.csv(o1,"C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/MIR_NPAMS/MIRA1.csv")


### FUNCTION to update sl_to_copperpair layer ###
UPDATE_STC = function(stc_layer, NPAMS_CLEAN)
{
  for (u1 in 1: dim(NPAMS_CLEAN)[1])
  {
    loc = which(stc_layer$sl_id == NPAMS_CLEAN[,"sl_id_ctm"][u1] &
                stc_layer$cupid == NPAMS_CLEAN[,"cup_to_match"][u1])
    for ( u2 in 1: length(loc))
    {
      if (stc_layer$copper_path_status[loc[u2]] == '')
      {
        stc_layer$copper_path_status[loc[u2]] = NPAMS_CLEAN[,"match_status"][u1]
      }
    }
  }
  stc_layer
}

updated_mir_stc = UPDATE_STC(stc_layer = mir_new , NPAMS_CLEAN = SIL_NPAMS_CLEAN)
#updated_mon_stc = UPDATE_STC(stc_layer = mon_new , NPAMS_CLEAN = MON_NPAMS_CLEAN)

writeOGR(updated_mir_stc,dsn = "C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff", layer = 'updated_SIL_stc', driver = 'MapInfo File', dataset_options="FORMAT=MIF")
#writeOGR(updated_mon_stc,dsn = "C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff", layer = 'updated_mon_stc', driver = 'MapInfo File', dataset_options="FORMAT=MIF")


#############################################################################
uni_sl_id = unique(MIR_NP_STC$sl_id)
change_count = rep(0, length(uni_sl_id))
change_cps = rep(0, length(uni_sl_id))
cupid_match = rep(0, length(uni_sl_id))
for (i in 1: length(uni_sl_id))
{
  old_loc = which(MIR_NP_STC$sl_id == uni_sl_id[i])
  new_loc = which(MIR_NEW_STC$sl_id == uni_sl_id[i])
  old_count = length(old_loc)
  new_count = length(new_loc)
  change_count[i] = new_count - old_count
  
  one_sl = MIR_NP_STC[old_loc,]
  for (j in 1: dim(one_sl)[1])
  {
    status_change = rep('CupidDNE',dim(one_sl)[1])
    cupid_equal = rep(0,dim(one_sl)[1])
    cupid = one_sl[j,]$cupid

    cupid_new_loc = which(MIR_NEW_STC[new_loc,]$cupid == cupid)
    if (length(cupid_new_loc) >0)
    {
      cupid_equal[j] = 1
      current_status = MIR_NEW_STC[new_loc,][cupid_new_loc,]$copper_path_status
      for (k in 1: length(current_status))
      {
        if (one_sl$copper_path_status[j]!=current_status[k] )
        {
          
          status_change[j] = paste(ifelse(one_sl$copper_path_status[j] == "", 'NULL',one_sl$copper_path_status[j]), 'to', current_status[k], sep = '' )
        }else{
          status_change[j] = 'Same'
        }
      }
      
    }
    

  }
  change_cps[i] = ifelse(dim(one_sl)[1]==1, status_change, paste(status_change, collapse = ','))
  cupid_match[i] = ifelse(dim(one_sl)[1]==1,cupid_equal, paste(cupid_equal, collapse = ','))
}

output = cbind(uni_sl_id, change_cps, cupid_match)
write.csv(output,"C:/Users/chang/Documents/Downer/sl_to_copperpair_stuff/stc_diff.csv")
