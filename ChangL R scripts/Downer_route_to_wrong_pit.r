###Downer ROUTE that may connect to wrong pit ###
library(sp)
library(rgdal)
library(rgeos)


#What to do?#
#For each route, search for the start and end structure id. Find if the pit exists. If so, is it the closest pit? If no, generate the corresponding pit at the end?#


route_hld = readOGR("C:\\Users\\chang\\Documents\\Downer\\HOR\\downer_2hor_02\\02_hld\\input_layers\\PP_inputs",layer = "route", stringsAsFactors = F)
structure_hld = readOGR("C:\\Users\\chang\\Documents\\Downer\\HOR\\downer_2hor_02\\02_hld\\input_layers\\PP_inputs",layer = "structurepoint", stringsAsFactors = F)



dim(route_hld)
dim(structure_hld)


error_route_row = c()
message = c()

for(i1 in 1: dim(route_hld)[1])
{
  start_str_id = route_hld$start_structure_id[i1]
  end_str_id = route_hld$end_structure_id[i1]
  start_str_found = which(structure_hld$id == start_str_id)
  end_str_found = which(structure_hld$id == start_str_id)
  if(length(start_str_found) < 1)
  {
    tmp_message = paste('cannot find pit ', start_str_id, ', the start_structure_id of ROUTE ', route_hld$id[i1])
    message = c(message, tmp_message)
    error_route_row = c(error_route_row,i1)
  }
  if(length(end_str_found) < 1)
  {
    tmp_message = paste('cannot find pit ', end_str_id, ', the end_structure_id of ROUTE ', route_hld$id[i1])
    message = c(message, tmp_message)
    error_route_row = c(error_route_row,i1)
  }
  if(length(start_str_found) > 1)
  {
    tmp_message = paste('hmmm, duplicate structurepoint ', start_str_id, ', how very weird')
    message = c(message, tmp_message)
    error_route_row = c(error_route_row,i1)
  }
  if(length(end_str_found) > 1)
  {
    tmp_message = paste('hmmm, duplicate structurepoint ', end_str_id, ', how very weird')
    message = c(message, tmp_message)
    error_route_row = c(error_route_row,i1)
  }
  
  
  
}

error_message = as.data.frame(message)
error_route_row_unique = unique(error_route_row)
write.csv(error_message,"C:\\Users\\chang\\Documents\\Downer\\HOR\\downer_2hor_02\\02_hld\\notes_and_feedback\\message_to_maree.csv")
writeOGR(route_hld[error_route_row_unique,],dsn = "C:\\Users\\chang\\Documents\\Downer\\HOR\\downer_2hor_02\\02_hld\\notes_and_feedback", layer = 'Error_Route_Perhaps', driver = 'MapInfo File', dataset_options="FORMAT=MIF")




