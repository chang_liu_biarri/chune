###Downer Route Length Comparison ###
library(sp)
library(rgdal)
library(rgeos)

route_ddd = readOGR("C:\\Users\\chang\\Documents\\Downer\\HOR\\downer_2hor_02\\04_ddd\\input_layers\\PP_inputs",layer = "route", stringsAsFactors = F)
structure_ddd = readOGR("C:\\Users\\chang\\Documents\\Downer\\HOR\\downer_2hor_02\\04_ddd\\input_layers\\PP_inputs",layer = "structurepoint", stringsAsFactors = F)

route_csv = read.csv('C:\\Users\\chang\\Documents\\Downer\\HOR\\downer_2hor_02\\04_ddd\\input_layers\\PP_inputs\\length_csv')
plot(route_csv$DIFFERENCE)

Ratio = route_csv$DIFFERENCE/route_csv$PROVIDED_LENGTH
t.test(Ratio)

plot(Ratio)
