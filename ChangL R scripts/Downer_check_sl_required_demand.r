library(sp)
library(rgdal)
library(rgeos)



### Need PP_inputs\land parcel (DDD), FTTC MDU polygon, PP_input\SLs (DDD), and sl_to_copperpair. ###
sl_ddd = readOGR("C:\\Users\\chang\\Documents\\Downer\\downer_2mon_20\\04_ddd\\input_layers\\PP_inputs",layer = "sl", stringsAsFactors = F)
stc_ddd = readOGR("C:\\Users\\chang\\Documents\\Downer\\downer_2mon_20\\04_ddd\\input_layers\\PP_inputs",layer = "sl_to_copperpair", stringsAsFactors = F)
#parcel = readOGR("C:\\Users\\chang\\Documents\\Downer\\downer_2mon_23\\04_ddd\\input_layers\\PP_inputs",layer = "land parcels", stringsAsFactors = F)
#MDU_poly = readOGR("C:\\Users\\chang\\Documents\\Downer\\downer_2mon_23\\04_ddd\\notes_and_feedback",layer = "Polygon_with_sls_to_spread", stringsAsFactors = F)


### 61 LOCID's from NBN feedback register ###

sl_to_check = read.table("C:\\Users\\chang\\Documents\\Downer\\downer_2mon_20\\04_ddd\\notes_and_feedback\\LOCIDs-2mon-20.csv", header = F)


loc_id = c()
cup_id = c()
copper_path_status = c()
for (i in 1: dim(sl_to_check)[1])
{
  stc_loc = which(stc_ddd$sl_id == sl_to_check[i,])
  for (j in 1: length(stc_loc))
  {
    loc_id = c(loc_id, as.character(sl_to_check[i,]))
    cup_id = c(cup_id, stc_ddd$cupid[stc_loc[j]])
    copper_path_status = c(copper_path_status, stc_ddd$copper_path_status[stc_loc[j]])
  }
}


res = cbind(loc_id, cup_id, copper_path_status)

write.csv(res,"C:\\Users\\chang\\Documents\\Downer\\downer_2mon_20\\04_ddd\\notes_and_feedback\\SLs_to_check.csv" )
