library(sp)
library(rgdal)
library(rgeos)
POLE = readOGR("C:\\Users\\chang\\Documents\\Solve\\HU_FireStation\\Base Data", layer = 'POLE', stringsAsFactors = F)

dim(POLE)[1] - length(unique(PARCEL$xfm_id))

xfm_uni = unique(POLE$xfm_id)
dup = c()

for(x1 in 1: length(xfm_uni))
{
  loc = which(POLE$xfm_id == unique(POLE$xfm_id)[x1])
  if(length(loc) > 1)
  {
    dup = c(dup, loc)
  }
}

POLE_DUP_XFM = POLE[dup,]
writeOGR(POLE_DUP_XFM, dsn = "C:\\Users\\chang\\Documents\\Solve\\HU_FireStation\\Potential Issue", overwrite_layer = T, layer = 'POLE_DUP_xfm', driver = 'ESRI Shapefile')

