library(sp)
library(rgdal)
library(rgeos)



### Need PP_inputs\land parcel (DDD), FTTC MDU polygon, PP_input\SLs (DDD), and sl_to_copperpair. ###
JTs_sls = readOGR("C:\\Users\\chang\\Documents\\Downer\\downer_2mir_25\\04_ddd\\notes_and_feedback\\ForAlyssa",layer = "JTs_sls", stringsAsFactors = F)
use_these_sls = readOGR("C:\\Users\\chang\\Documents\\Downer\\downer_2mir_25\\04_ddd\\notes_and_feedback\\ForAlyssa",layer = "use_these_sls", stringsAsFactors = F)

### Match and replace ###

#sl_to_check = read.table("C:\\Users\\chang\\Documents\\Downer\\downer_2mon_20\\04_ddd\\notes_and_feedback\\LOCIDs-2mon-20.csv", header = F)




message = c()
use_new = use_these_sls
for (i in 1: dim(use_these_sls)[1])
{
  locid = use_these_sls$id[i]
  dp_old = use_these_sls$dp_change[i]
  if(locid == '')
  {
    locid = use_these_sls$ref_id[i]
    row_num = which(JTs_sls$ref_id == locid)
  }else{
    row_num = which(JTs_sls$id == locid)
  }
  
  if(length(row_num)>0)
  {
    dp_new = JTs_sls$dp_change[row_num]
  }else{
    dp_new = 'CANNOTFINDID'
  }
  
  if(dp_old != dp_new )
  {
    message_new = paste(locid, ' updated dp_change from ', ifelse(dp_old=='', 'NULL', dp_old), ' to ', dp_new)
    message = c(message, message_new)
    use_new$dp_change[i] = dp_new
  }
  
}

Message = as.data.frame(message)

writeOGR(use_new,dsn = "C:\\Users\\chang\\Documents\\Downer\\downer_2mir_25\\04_ddd\\notes_and_feedback\\ForAlyssa", layer = 'use_these_sls_Alyssa', driver = 'MapInfo File', dataset_options="FORMAT=MIF")


write.csv(Message,"C:\\Users\\chang\\Documents\\Downer\\downer_2mir_25\\04_ddd\\notes_and_feedback\\ForAlyssa\\Message_sl_dp_update.csv" )
