### INSTALL all following packages if you see an error. ###
library(sp)
library(rgdal)
library(rgeos)

PiP = function(points, polygons, weighted = F, weight.name =NULL)
{
  if(class(polygons)!= 'SpatialPolygonsDataFrame')
  {
    print('Incorrect class for polygons')
  }
  if(class(points)!= 'SpatialPointsDataFrame')
  {
    print('Incorrect class for polygons')
  } 
  new.points = SpatialPoints(points, proj4string = points@proj4string)
  if(weighted == T)
  {
    weight.loc = which(names(points@data) == weight.name)
    chr.wt = points@data[,weight.loc]
    chr.loc = which(is.na(as.numeric(chr.wt)))
    if (length(chr.loc)>0)
    {
      incorrect.value = chr.wt[chr.loc]
      guess.value = c()
      for(k1 in 1: length(incorrect.value))
      {
        guess.value[k1] = strsplit(incorrect.value[k1], split = ',')[[1]][1]
      }
      chr.wt[chr.loc]= guess.value
    }
    Weight = as.numeric(chr.wt)
  }else{
    Weight = rep(1,dim(points)[1])
  }
  count = c()
  for(i1 in 1: dim(polygons)[1] )
  {
    points_in_bbox_loc = which(new.points@coords[,1]>= polygons[i1,]@bbox[1,1]&
                                 new.points@coords[,1]<= polygons[i1,]@bbox[1,2]&
                                 new.points@coords[,2]>= polygons[i1,]@bbox[2,1]&
                                 new.points@coords[,2]<= polygons[i1,]@bbox[2,2])
    points_scanned = new.points[points_in_bbox_loc,]  
    Weight_scanned = Weight[points_in_bbox_loc]
    #count[i1] = sum(gContains(polygons[i1,],points_scanned, byid = T))
    count[i1] = sum(Weight_scanned[gContains(polygons[i1,],points_scanned, byid = T)])
  }
  polygons@data = cbind(polygons@data, count)
  names(polygons)[length(names(polygons))] = 'PNTCNT'
  polygons
}

### READ IN points and polygon layers like this ###
parcel = readOGR("C:\\Users\\chang\\Documents\\Solve\\HU_FireStation\\Base Data", layer = 'cd_no_leadin_buffer', stringsAsFactors = F)
addie = readOGR("C:\\Users\\chang\\Documents\\Solve\\HU_FireStation\\Base Data", layer = 'POLE_NO_DUPLICATE', stringsAsFactors = F)


#Define starting time
start.time = Sys.time()

### RUN function like this. If you just want to count the points, then ignore the last two parameters. ###
### If you want a weighted count, then set weight = T, and weight.name = 'aaa' where aaa is the attribute that contains weight information. ###

#count_add4lewis = PiP(addie, parcel, weighted = T, weight.name = 'ADDRESS_CO' )
count_add4lewis = PiP(addie, parcel )

#Define ending time
end.time = Sys.time()

#Calculate how long it takes to run the script
time.taken = end.time - start.time
time.taken

### WRITE an output shapefile for the polygon layer ###
writeOGR(count_add4lewis, dsn = "C:\\Users\\chang\\Documents\\Solve\\HU_FireStation\\Base Data", overwrite_layer = T, layer = 'cd_no_leadin_buff_polecount', driver = 'ESRI Shapefile')
