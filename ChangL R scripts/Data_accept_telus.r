#################################################################
#################################################################
##### DATA ACCEPTANCE -- Majorly for telus ##################################
library(sp)
library(rgdal)
library(rgeos)
library(ggmap)
### To read in the layers ###
ADDRESS = readOGR("C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs", layer = 'ADDRESS', stringsAsFactors = F)
RAN_ADD = readOGR("C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs", layer = 'rand_addie', stringsAsFactors = F)
POLE = readOGR("C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs", layer = 'POLE', stringsAsFactors = F)
RAN_POLE = readOGR("C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs", layer = 'rand_pole', stringsAsFactors = F)

RAN_PAR = readOGR("C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs", layer = 'rand_parcel', stringsAsFactors = F)

DUCT = readOGR("C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs", layer = 'DUCT', stringsAsFactors = F)
DNODE = readOGR("C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs", layer = 'duct_nodes', stringsAsFactors = F)


### ADDRESS ###
add_data = ADDRESS@data
names(add_data)
dim(add_data) 
#[1] 7580   10
build_count = sort(unique(add_data$building_a))
num_add =  c()
for ( i in 1: length(build_count))
{
  num_add[i] = length(which(add_data$building_a == build_count[i]))
}

BC_df = rbind(build_count, num_add)
write.csv(BC_df, file = "C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs\\BC.csv")

length(unique(add_data$building_f))

co1 = ADDRESS@coords[,1]
co2 = ADDRESS@coords[,2]
dt = cbind(co1,co2)
co3 = paste(co1,co2, sep = ',')
length(unique(co3))

table(add_data$building_t)
table(add_data$city)
length(unique(add_data$id_bn))
length(unique(add_data$id_string))

## RAN_ADD, T_TEST ##
d_add = RAN_ADD@data$ToRoofTop
plot(d_add)
t.test(d_add, mu = 15, conf.level = 0.99, alternative = 'greater' )



### POLE ###
dim(POLE)
length(unique(POLE$id))
length(unique(POLE$id_string))
length(unique(POLE$pole_fk))

## RAN_POLE, T_TEST ##
d_pole = RAN_POLE$Distance
t.test(d_pole, mu = 1.5, conf.level = 0.99, alternative = 'greater' )


### SPLIT duct, by its nodes ###

duct_split = SplitNearPoints(point_layer = DNODE, line_layer= DUCT, search_radius_feet = 1)

dsplit_val = duct_split$Valid


D1Node = DNODE[1,]
DLLoc = c()
for(n1 in 1:length(DNODE))
{
  buff = gBuffer(DNODE[n1,], width = 0.1/60/6075)
  Degree = sum(gIntersects(dsplit_val, buff, byid = T))
  
  if(Degree == 1)
  {
    lead_loc = which(gIntersects(dsplit_val, buff, byid = T))
    DLLoc = rbind(DLLoc,lead_loc)
    D1Node = rbind(D1Node, DNODE[n1,])
  }
}
D1Node = D1Node[-1,]

DLead = dsplit_val[DLLoc,s]

writeOGR(DLead, dsn = "C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs", layer = 'LEADIN', driver = 'ESRI Shapefile')
writeOGR(D1Node, dsn = "C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs", layer = 'MISSING_ADD', driver = 'ESRI Shapefile')



### GEOCODE ###
myw = add_data$myw_title
street_name = c()
for(m1 in 1:length(myw))
{
  street_name[m1] = substr(myw[m1], start = 9, stop = nchar(myw[m1]))
}

To_Geo_Code = street_name[1:2000]
ADD_Geo_Code = geocode(To_Geo_Code)
dim(ADD_Geo_Code)

add_d = add_data[1:2000,]
add_d$lon = ADD_Geo_Code[,1]
add_d$lat = ADD_Geo_Code[,2]

write.csv(add_d,file ="C:\\Users\\chang\\Documents\\Solve\\Telus\\SHPs\\ADDRESS_GEOCODE.csv" )


### RAN_PAR, binom-test ###
names(RAN_PAR@data)
HasBuild = RAN_PAR$HasBuild
GoodSize = RAN_PAR$GoodSize
sum(HasBuild)
sum(GoodSize)

binom.test(50-sum(HasBuild), 50, p = 0.13, alternative = 'greater', conf.level = 0.99)
binom.test(50-sum(GoodSize), 50, p = 0.14, alternative = 'greater', conf.level = 0.99)


###PARCEL VS ADDRESS###
library(s20x)
hit = read.csv(file ="C:\\Users\\chang\\Downloads\\more_stats.csv", header = T )
hit$pva = hit$perimeter/hit$size
hit$hvw = hit$m_height/hit$m_width
range(hit$hit_count)
range(hit$size)
plot(sort(hit$size), ylim = c(0,1e6))
plot(sort(hit$hit_count), ylim = c(0,5))
summary(hit$size)


to.del = which(hit$size < 200)
to.del.add = which(hit$hit_count == 0)
to.del.large = which(hit$size > 20000)
hit.clean = hit[-to.del,]
hit.add = hit[-to.del.add,]
hit.normalsize = hit[-c(to.del,to.del.large),]

length(which(hit$size > 1690 & hit$size < 5081 & hit$hit_count ==0))

area.single = hit$size[to.del.add]
area.single.normal = area.single[-which(area.single > 40000 | area.single < 200)]
summary(area.single.normal)
hist(area.single.normal, xlab = 'parcel size', main = 'Histogram of single-address-parcel area')

hit1 = lm(hit_count ~ size * m_height * m_width *
            h_l_ration * nearest_neighbor * perimeter * nearest_centroid
            ,
          data = hit.normalsize)
summary(hit1)

hit1 = lm(hit_count ~   m_height * m_width * pva * hvw,
          data = hit.normalsize)
summary(hit1)
pairs20x(hit_count ~  size * m_height * m_width *
           h_l_ration * nearest_neighbor * perimeter * nearest_centroid,
      data = hit.normalsize)

hit.lm = lm(hit_count~size, data = hit.normalsize)
summary(hit.lm)

hc.lm = lm(log(size)~hit_count, data = hit.clean)
summary(hc.lm)
#############################################################3
### CHORUS ###
add1 = read.csv(file ="C:\\Users\\chang\\Downloads\\Waihi_addresses_test.csv", header = T )

names(add1)
wr_street = as.character(add1$Wr_street, na.rm =T)
wr_number = as.character(add1$Wr_number, na.rm =T)

bs = length(which(wr_street == 'Y'))
bn = length(which(wr_number == 'Y'))
  
street = c(rep(1, bs), rep(0, 50-bs))
number = c(rep(1, bn), rep(0, 50-bn))

binom.test(bs, 50, p = 0.01, alternative = 'greater')
binom.test(bn, 50, p = 0.085, alternative = 'greater')

binom.test(bs, 50, p = 0.01, alternative = 'two.sided')
binom.test(bn, 50, p = 0.085, alternative = 'two.sided')



##########################################################################
### HUAWEI ###
POLE = readOGR("C:\\Users\\chang\\Documents\\Huawei\\data_20170104\\Base Data", layer = 'POLE', stringsAsFactors = F)
CAND_NODE = readOGR("C:\\Users\\chang\\Documents\\Huawei\\data_20170104\\Preprocessor", layer = 'candidate_nodes', stringsAsFactors = F)

CAND_EDGE = readOGR("C:\\Users\\chang\\Documents\\Huawei\\data_20170104\\Preprocessor", layer = 'candidate_edges', stringsAsFactors = F)

DEMAND_EDGE = CAND_EDGE[which(CAND_EDGE$TYPE == 'DEMAND'),]

PIT = CAND_NODE[which(CAND_NODE$TYPE == 'PIT'),]

POLETOFIX = c()
count = c()
for (i in 1: length(POLE))
{
  buf = gBuffer(POLE[i,], width = 0.1/60/6075)
  count[i] = sum(gIntersects(buf, DEMAND_EDGE, byid = T))
  
}


count_p = c()
for (i in 1: length(PIT))
{
  buf = gBuffer(PIT[i,], width = 0.1/60/6075)
  count_p[i] = sum(gIntersects(buf, DEMAND_EDGE, byid = T))
  
}
PITTOFIX = PIT[which(count_p >7),]
writeOGR(PITTOFIX, dsn = "C:\\Users\\chang\\Documents\\Huawei\\data_20170104", layer = 'PITTOFIX', overwrite_layer = T, driver = 'ESRI Shapefile')



T4_fiber = readOGR("C:\\LinkVB\\BlueSteel_Repo\\test_data\\data_20170104\\To Huawei\\Solver_Tier3", layer = 'T4_fiber_cable', stringsAsFactors = F)


ID_new = paste('T4',substr(T4_fiber$ID,start = 3, stop = 100), sep = '')

Source_new = paste('T4',substr(T4_fiber$Source,start = 3, stop = 100), sep = '')

StartID_new = T4_fiber$StartID
EndID_new = T4_fiber$EndID


for(i in 1: length(StartID_new))
{
  string1 = substr(StartID_new[i], start = 1, stop = 2)
  string2 = substr(EndID_new[i], start = 1, stop = 2)
  if(string1 =='T1')
  {
    StartID_new[i] = paste('T3',substr(T4_fiber$StartID[i],start = 3, stop = 100), sep = '')
  }
  if(string1 =='T2')
  {
    StartID_new[i] = paste('T4',substr(T4_fiber$StartID[i],start = 3, stop = 100), sep = '')
  }
  if(string2 =='T1')
  {
    EndID_new[i] = paste('T3',substr(T4_fiber$EndID[i],start = 3, stop = 100), sep = '')
  }
  if(string2 =='T2')
  {
    EndID_new[i] = paste('T4',substr(T4_fiber$EndID[i],start = 3, stop = 100), sep = '')
  }
}

T4_fiber$ID = ID_new
T4_fiber$Source = Source_new

T4_fiber$StartID = StartID_new
T4_fiber$EndID = EndID_new
writeOGR(T4_fiber, dsn = "C:\\LinkVB\\BlueSteel_Repo\\test_data\\data_20170104\\To Huawei\\Solver_Tier3", layer = 'T4_fiber_cable_1', overwrite_layer = T, driver = 'ESRI Shapefile')
