library(sp)
library(rgdal)
library(rgeos)

### CHANGE LAYER PROJECTION ###

par = readOGR("C:\\Users\\chang\\Documents\\Solve\\A00H65C\\raw", layer = 'PARCELS',stringsAsFactors = F)
fib = readOGR("C:\\Users\\chang\\Downloads\\tim", layer = 'Fiber Cable Geo Line',stringsAsFactors = F)


PAR1 = spTransform(par, CRS('+proj=longlat +datum=WGS84 +no_defs+
                             ellps=WGS84 +towgs84=0,0,0'))

FIBER = spTransform(fib, CRS('+proj=longlat +datum=WGS84 +no_defs+
                             ellps=WGS84 +towgs84=0,0,0'))

writeOGR(PAR1,dsn = "C:\\Users\\chang\\Documents\\Solve\\A00H65C\\raw\\prepped_data", layer = 'parcels', overwrite_layer = T, driver = 'ESRI Shapefile')
writeOGR(COND,dsn = 'C:\\Users\\chang\\Downloads\\tim', layer = 'FIBER_CP', driver = 'ESRI Shapefile')

