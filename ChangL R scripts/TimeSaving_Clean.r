### FOND CALIBRATION -- TIME SAVING ###server_waldo = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\Waldo_With_Input_Parameters.csv',header = T)

server_waldo = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\Waldo_With_Input_Parameters.csv',header = T)
server_kingkong1 = read.csv('C:\\Users\\chang\\Documents\\DS work\\Solve Time\\KingKong1_With_Input_Parameters.csv',header = T)
server = as.data.frame(rbind(server_waldo,server_kingkong1))


server = server[server$solve_finish == 'True' & server$run_time!= 'No info' & server$last_modified!= 'No info',]
server = server[server$user!='jenkins'& server$Demand != 'No Run',]

# Daniel ran two solves, with MST_packing >65. We remove them.#
server = server[which(as.numeric(paste(server$MST_packing)) <=8),]


server_date = as.character(substr(server$submit_time, start = 1, stop = 10))
server_date = as.Date(server_date)
server_user = as.character(server$user)
server$date = server_date
run_time = as.numeric(paste(server$run_time))

server$runtime = run_time
server$version = as.factor(as.character(server$version))
server$user = as.factor(as.character(server$user))
server$Demand = as.numeric(paste(server$Demand))


server$MSTs = as.numeric(paste(server$MSTs))
server$MST_packing = as.character(paste(server$MST_packing))
server$MST_packing[1] = 40/7
server$MST_packing[server$MST_packing =='No Run'] = 0
server$MST_packing = as.numeric(server$MST_packing)
server$FAPs = as.character(paste(server$FAPs))
server$FAPs[server$FAPs =='No Run'] = 0
server$FAPs = as.numeric(server$FAPs)
server$FAP_packing = as.character(paste(server$FAP_packing))
server$FAP_packing[server$FAP_packing =='No Run'] = 0
server$FAP_packing = as.numeric(server$FAP_packing)
server$FDHs = as.character(paste(server$FDHs))
server$FDHs[server$FDHs =='No Run'] = 0
server$FDHs = as.numeric(server$FDHs)
server$FDH_packing = as.character(paste(server$FDH_packing))
server$FDH_packing[server$FDH_packing =='No Run'] = 0
server$FDH_packing = as.numeric(server$FDH_packing)
server$Dist = as.factor(as.character(server$Dist))
server$COF = as.factor(as.character(server$COF))
server$Arc_UG = as.numeric(paste(server$Arc_UG))
server$Arc_AER = as.numeric(paste(server$Arc_AER))
server$Arcs = server$Arc_UG + server$Arc_AER
server$Length = server$Length_AER + server$Length_UG
#new parameters #
server$use_hub_balancing = as.factor(as.character(server$use_hub_balancing))
server$cable_cost_mst = as.numeric(paste(server$cable_cost_mst))
server$max_drop_length_ar = as.numeric(paste(server$max_drop_length_ar))
server$reuse_factor = as.numeric(paste(server$reuse_factor))
server$min_fdh_to_use = as.numeric(paste(server$min_fdh_to_use))
server$prune_distribution_input_design_graph = as.factor(as.character(server$prune_distribution_input_design_graph))
server$max_drop_length_ug = as.numeric(paste(server$max_drop_length_ug))
server$run_fdh_packing = as.factor(as.character(server$run_fdh_packing))
server$hub_installation_cost_mst = as.numeric(paste(server$hub_installation_cost_mst))
#server$fdh_max_solver_duration_1_hub_packing = as.numeric(paste(server$fdh_max_solver_duration_1_hub_packing))
server$run_nap_packing = as.factor(as.character(server$run_nap_packing))
server$run_distribution_solver = as.factor(as.character(server$run_distribution_solver))
server$treat_hubs_as = as.factor(as.character(server$treat_hubs_as))
#server$max_candidate_connections_mst = as.numeric(paste(server$max_candidate_connections_mst))
server$mip_stop_gap_fdh = as.numeric(paste(server$mip_stop_gap_fdh))
server$mip_stop_gap_nap = as.numeric(paste(server$mip_stop_gap_nap))
server$mip_stop_gap_dist = as.numeric(paste(server$mip_stop_gap_dist))
server$mip_stop_gap_mst = as.numeric(paste(server$mip_stop_gap_mst))
server$discount_preference_value = as.numeric(paste(server$discount_preference_value))
server$access_cabling_algo = as.factor(as.character(server$access_cabling_algo))
server$MST_Demand = as.character(server$MST_Demand)
server$AF_FAP_Demand = as.character(server$AF_FAP_Demand)
server$MST_Demand[server$MST_Demand == 'No Run'] = 0
server$AF_FAP_Demand[server$AF_FAP_Demand == 'No Run'] = 0
server$MST_Demand = as.numeric(server$MST_Demand)
server$AF_FAP_Demand = as.numeric(server$AF_FAP_Demand)

### Create two more variables. MSTALL = MSTs + MST_Demand. FAPALL = FAPs + AF_FAP_Demand
server$MSTALL = server$MSTs + server$MST_Demand
server$FAPALL = server$FAPs + server$AF_FAP_Demand




library(s20x)

### MST solve data only ###
server_mst = server[which(server$FAPs ==0 & server$FDHs ==0 & server$run_nap_packing == 'False'),]
loc = which(row.names(server_mst) == 727 | row.names(server_mst) == 1538 |
              row.names(server_mst) == 1841)
loc = which(row.names(server_mst) == 727| row.names(server_mst) == 1538|
              row.names(server_mst) == 1841|row.names(server_mst) == 462|row.names(server_mst) == 1507|
              row.names(server_mst) == 1870|row.names(server_mst) == 397|row.names(server_mst) == 3082|row.names(server_mst) ==3451 )
server_mst_no_outlier = server_mst[-loc,]
# Ignore any data with mst_packing less than 4, as they are not very feasible. #
# weirdmst = which(server_mst_no_outlier$MST_packing < 4)
# server_mst_no_outlier = server_mst_no_outlier[-weirdmst,]
### FAP solve data only ###
server_fap = server[which(server$FAPs!= 0 & server$FDHs == 0),]
# Ignore any data with FAP_packing less than 6, as they are not very feasible. #
#weirdfap = which(server_fap$FAP_packing < 6)
#server_fap = server_fap[-weirdfap,]
### FDH solve data only ###
server_fdh = server[which(server$Dist != 'Yes' & server$FDHs != 0),]
tr = which(rownames(server_fdh) == 241 | rownames(server_fdh) == 762 | rownames(server_fdh) == 601)
server_fdh_no_outlier = server_fdh[-tr,]
# Ignore any data with FAP_packing less than 8, as they are not very feasible. #
# weirdfdh = which(server_fdh$FDH_packing < 8)
# server_fdh_no_outlier = server_fdh_no_outlier[-weirdfdh,]
### Dist solve data only ###
server_dist = server[server$Dist == 'Yes',]
tr = which(server_dist$FAP_packing < 5 | server_dist$FDH_packing < 7)
server_dist = server_dist[-tr,]
tr1 = which(rownames(server_dist) == 5010)
server_dist_no_outlier = server_dist[-tr1,]

###########################################################################
###########################################################################
###########################################################################
### Function to drop terms, based on p-values ###
dropTerm  = function(lm1, category.variable = c() )
{
  lin = lm1
  varname = names(lin$coefficients)
  rem.loc = c()
  if(length(category.variable) > 0)
  {
    for( i in 1:length(category.variable))
    {
      rem.loc = c(rem.loc,grep(paste(category.variable[i], sep = ''), varname))
    }
  }
  
  pval = summary(lin)$coefficients[-c(1,rem.loc),4]
  insig = which(pval >= 0.05)
  while ( length(insig) > 0 )
  {
    todrop = pval[insig][which(pval[insig] == max(pval[insig]))]
    variabletodrop = names(todrop)
    txt = as.character(lin$call)
    txtnew = paste(txt[1], '(',txt[2],'-', variabletodrop,',', txt[3],')')
    lmupdate = eval(parse(text = txtnew))
    lin = lmupdate 
    rem.loc = c()
    #varname = names(lin$coefficients)
    varname = names(summary(lin)$coefficients[,4])
    if( length(category.variable)> 0)
    {
      for( i in 1:length(category.variable))
      {
        rem.loc = c(rem.loc,grep(category.variable[i], varname))
      }
    }
    
    pval = summary(lin)$coefficients[-c(1,rem.loc),4]
    if(length(pval) == 1)
    {
      names(pval) = names(summary(lin)$coefficients[-rem.loc,4])[2]
    }
    insig = which(pval >= 0.05)
  }
  lin
}


### lm for MST tier ###

lm_mst_0 = lm(log(runtime)~Demand * MSTs  * Arc_AER * Length_AER * Arc_UG * Length_UG, data = server_mst_no_outlier)
summary(lm_mst_0)

lm_mst_fin = dropTerm(lm_mst_0)
summary(lm_mst_fin) #0.8259

### lm for FAP tier ###
lm_fap_0 = lm(log(runtime)~Demand *MSTALL*FAPALL * Arc_AER * Length_AER * Arc_UG * Length_UG *
                mip_stop_gap_nap * COF + reuse_factor * treat_hubs_as * discount_preference_value, data = server_fap)
summary(lm_fap_0)

lm_fap_fin = dropTerm(lm_fap_0, category.variable = c('COF','treat_hubs_as'))
summary(lm_fap_fin) #0.8467

### lm for FDH tier ###
lm_fdh_0 = lm(log(runtime) ~ Demand * MSTALL * FAPALL * FDHs* Arc_AER * Length_AER * Arc_UG * Length_UG *
                mip_stop_gap_fdh * COF + access_cabling_algo * treat_hubs_as * reuse_factor * discount_preference_value, data = server_fdh_no_outlier)

summary(lm_fdh_0)
lm_fdh_fin = dropTerm(lm_fdh_0, category.variable = c('COF','treat_hubs_as', 'access_cabling_algo'))
summary(lm_fdh_fin) #0.9335
### lm for DIST tier ###
lm_dist_0 = lm(formula = log(runtime) ~  MSTALL * FAPALL * FDHs*Arc_AER * Length_AER * Arc_UG * Length_UG*
                   mip_stop_gap_dist * prune_distribution_input_design_graph * COF + discount_preference_value* 
                   reuse_factor, data = server_dist)
summary(lm_dist_0)
lm_dist_fin = dropTerm(lm_dist_0, category.variable = c('prune_distribution_input_design_graph','COF'))
summary(lm_dist_fin) #0.9394
### Turns out, there is a strong relationship between MSTs & Arcs ###
pairs20x(MSTs~Arc_AER * Arc_UG * Length_AER * Length_UG, data = server)

#############################################################################
#############################################################################
#############################################################################
arcug_lm_00 = lm(Arc_UG ~ Demand * Arc_AER * Length_AER, data = server)
summary(arcug_lm_00)

arcug_lm_fin = dropTerm(arcug_lm_00)
summary(arcug_lm_fin)

lenug_lm_00 = lm(Length_UG ~ Demand * Arc_AER * Length_AER * Arc_UG, data = server)
summary(lenug_lm_00)

lenug_lm_fin = dropTerm(lenug_lm_00)
summary(lenug_lm_fin)


#mst_lm_0 = lm(MSTALL~Arc_AER * Arc_UG * Length_AER * Length_UG, data = server)
#summary(mst_lm_0)
mst_lm_00 = lm(MSTs~Arc_AER * Arc_UG * Length_AER * Length_UG, data = server)
summary(mst_lm_00)

mst_lm = lm(MSTs~Arc_AER * Arc_UG * Length_AER * Length_UG -Arc_UG:Length_AER:Length_UG -
              Arc_UG:Length_AER -Arc_AER:Arc_UG:Length_UG -Arc_UG:Length_UG-Arc_UG , data = server)
summary(mst_lm)
# Read the coefficients quicker #
as.character(summary(mst_lm)$coefficients[,1])
names(summary(mst_lm)$coefficients[,1])


#mpack_lm = lm(MST_packing~Demand*MSTs, data = server)
#summary(mpack_lm)

fap_lm_0 = lm(FAPs~ MSTALL *Arc_AER * Arc_UG * Length_AER * Length_UG, data = server_fap)
summary(fap_lm_0)
# MSTs are less useful than MSTALL#
fap_lm = lm(FAPs~ MSTs *Arc_AER * Arc_UG * Length_AER * Length_UG, data = server_fap)
summary(fap_lm)

f1 = fap_lm_0
f1 = update(f1, .~. -MSTALL:Arc_AER:Length_UG)
summary(f1)

# A complicated model, with 0.9751 R2 #
fap_lm_c1 = lm(formula = FAPs ~ MSTALL + Length_AER + Length_AER:Arc_AER + 
              MSTALL:Length_UG + Length_UG:Arc_UG + Length_AER:Length_UG + 
              MSTALL:Arc_AER:Arc_UG + MSTALL:Length_AER:Arc_AER + Length_AER:Arc_AER:Arc_UG + 
              MSTALL:Arc_AER:Length_UG + Arc_AER:Length_UG:Arc_UG + MSTALL:Length_AER:Length_UG + 
              Length_AER:Arc_AER:Length_UG + Length_AER:Length_UG:Arc_UG + 
              MSTALL:Length_AER:Arc_AER:Arc_UG + MSTALL:Arc_AER:Length_UG:Arc_UG + 
              MSTALL:Length_AER:Length_UG:Arc_UG + Length_AER:Arc_AER:Length_UG:Arc_UG + 
              MSTALL:Length_AER:Arc_AER:Length_UG:Arc_UG, data = server_fap)
summary(fap_lm_c1)

# A much simplified model, with 0.9748 R2. But I prefer this one. #
fap_lm_s1 = lm(formula = FAPs ~ MSTALL + Length_AER:Arc_AER:Arc_UG + MSTALL:Arc_AER:Arc_UG:Length_UG + 
                 Length_AER:Arc_AER:Arc_UG:Length_UG, data = server_fap)
summary(fap_lm_s1)

fap_lmm = lm(formula = FAPs ~ MSTALL, data = server_fap)
summary(fap_lmm)


fdh_lm = lm(FDHs~FAPALL , data = server_fdh)
summary(fdh_lm)

##############################################################################
##############################################################################
##############################################################################
### Actually, the models can be much much easier. MSTs are related to Demand,
### FAPs are related to MSTALL, FDHs are related to FAPALL. R2 will drop,
### However, interpretation is way simpler, plus we avoid multicollinearity
### at least for a bit.

################################################################
weirdall = which(server$MST_packing < 4)
server_mst_lm = server[-weirdall,]
mst_lm = lm(MSTs ~ Demand, data = server_mst_lm)
summary(mst_lm)

server_fap_lm = server[server$FAPs != 0,]
wfap = which(server_fap_lm$FAP_packing < 6)
server_fap_lm = server_fap_lm[-wfap,]
fap_lm = lm(FAPs ~ MSTALL, data = server_fap_lm)
summary(fap_lm)

server_fdh_lm = server[server$FDHs != 0,]
wfdh = which(server_fdh_lm$FDH_packing < 8)
server_fdh_lm = server_fdh_lm[-wfdh,]
fdh_lm = lm(FDHs ~ FAPALL, data = server_fdh_lm)
summary(fdh_lm)


# simple prediction #
sample1 = server[1,]
sample1$Arc_AER = 6000
sample1$Length_AER = 245100
sample1$Demand = 9300
sample1$Arc_UG = 0
sample1$Length_UG = 0

#sample1 = data.frame(Arc_AER = 6600, Length_AER = 245100, Demand = 9300, Arc_UG = 0, Length_UG = 0)
predict(mst_lm,sample1, interval = 'prediction')
#sample1 = data.frame(sample1, MSTALL = 1804 )
sample1$MSTALL = sample1$MSTs = 1804
predict(fap_lm,sample1, interval = 'prediction')
#sample1 = data.frame(sample1, FAPALL = 255)
sample1$FAPs = sample1$FAPALL = 255
predict(fdh_lm,sample1, interval = 'prediction')

# sample1 = data.frame(sample1, MSTs = 1804, FAPs = 255, FDHs = 25, COF = 'Cache',
#                      mip_stop_gap_mst = 0.05, mip_stop_gap_nap = 0.05,
#                      mip_stop_gap_fdh = 0.1, mip_stop_gap_dist = 0.05,
#                      treat_hubs_as = 'fixed_positions', reuse_factor = 0.75,
#                      discount_preference_value = 0.8,
#                      max_drop_length_ug = 91.44, max_drop_length_ar = 91.44)
# then predict runtime #
sample1$FDHs = 25
sample1$COF = 'Cache'
sample1$mip_stop_gap_mst = 0.001
sample1$mip_stop_gap_nap = 0.05
sample1$mip_stop_gap_fdh = 0.1
sample1$mip_stop_gap_dist = 0.1
sample1$treat_hubs_as = 'fixed_positions'
sample1$reuse_factor = 0.75
sample1$discount_preference_value = 0.8
sample1$max_drop_length_ug = 91.44
sample1$max_drop_length_ar = 91.44

exp(predict(lm_mst_fin,sample1, interval = 'prediction'))
exp(predict(lm_fap_fin,server_fap[1,], interval = 'prediction'))

### a plot of devices ###
Demand_set = seq(10000:40000)
MST_set = 0.196336*Demand_set
FAP_set = 0.1413487*MST_set + 0.025785
FDH_set = 0.0964609*FAP_set + 0.1773975

plot(Demand_set, MST_set, type = 'l', col = 'red' )
points(Demand_set, FAP_set, type = 'l', col = 'blue')
points(Demand_set, FDH_set, type = 'l', col = 'yellow')

plot(Demand_set, Demand_set/FDH_set, ylim = c(0,768), type = 'l', col = 'green')
points(Demand_set, Demand_set/MST_set, type = 'l', col = 'cyan')
points(Demand_set, Demand_set/FAP_set, type = 'l', col = 'purple')

mean(Demand_set/MST_set)
mean(MST_set/FAP_set)
mean(FAP_set/FDH_set)


