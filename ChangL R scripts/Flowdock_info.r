### FLOWDOCK INFO ###

## lets start with one flow, lisa-going-to-irish-murphy
#directory = "C:\\Users\\chang\\Documents\\Consultant\\lisagoingtoirishmurphy\\"
directory = "C:\\Users\\chang\\Documents\\Consultant\\Flowdock_one_to_ones\\"
lisa = read.csv(paste(directory,"messages-202467-Lisa.csv", sep = ''), header = T)
alyssa = read.csv(paste(directory,"messages-202464-something_else.csv", sep = ''), header = T, skipNul = T) 
camille = read.csv(paste(directory,"messages-296038-Crazy_Camille.csv", sep = ''), header = T)
june = read.csv(paste(directory,"messages-127146-June.csv", sep = ''), header = T, skipNul = T) 
trevor = read.csv(paste(directory,"messages-128384-Trevor.csv", sep = ''), header = T, skipNul = T) 
kyle = read.csv(paste(directory,"messages-122129-Kyle.csv", sep = ''), header = T, skipNul = T) 
laura = read.csv(paste(directory,"messages-119129-Laura.csv", sep = ''), header = T, skipNul = T) 

alyssa = lisa
other_user_name = 'Lisa Mayo'
owner = 'Chang Liu'

## for owner ##
#---------------------------------------------------------------------------------------#
all_df = rbind(lisa, alyssa, camille, june, trevor, kyle, laura)
## make sent_at to a date ##

sent_at_to_date = as.Date(all_df[,'sent_at'], format = "%m/%d/%Y %I:%M%p %z")

#date_correct_format = strftime(sent_at_to_date, format = "%m/%d/%Y %I:%M%p %z")
all_df[, "date"] = sent_at_to_date
spoken_dates = unique(sort(sent_at_to_date))

## Task ONE : When did I become a swearbox ##

assumed_swear_words = c("wth", "wtf", "fuck", "shit", "ass", "bloody", "hell", "moron", "ffs")
swear_ch = rep(0, length(spoken_dates))
sent_ch = rep(0, length(spoken_dates))
for (j in 1: length(spoken_dates))
{
  exact_date = spoken_dates[j]
  rows_me = which(all_df[, "date"] == exact_date & all_df[, "user"] == owner)
  sent_ch[j] = length(rows_me)
  if (length(rows_me) > 0)
  {
    for (k1 in 1: length(rows_me))
    {
      row = rows_me[k1]
      string_to_search = tolower(all_df[row, "content"])
      is_swear = sapply(assumed_swear_words, grepl, string_to_search)
      if (any(is_swear))
      {
        swear_ch[j] = swear_ch[j] + 1
      }
      
    }
  }
}
# Standardize the count a bit #
swear_ch = ifelse(sent_al != 0, swear_ch/sent_ch * 100, 0)


par(mar = c(7, 4.1, 4.1, 2.1))
subset = seq(1, length(spoken_dates), by = 20)
partial_sd = spoken_dates[subset]
swear_max = max(c(0, swear_ch))
plot(spoken_dates, swear_ch, ylim = c(0, swear_max), main = 'When Did I Become A Swearbox', type = 'o', col = 'pink4', lty = 2, pch = 7, xaxt = 'n', xlab = '', ylab = 'swears')

axis(1, at = partial_sd, labels = partial_sd, padj = 1, las = 2, cex = 0.1)
#legend(spoken_dates[1], 0.9 * swear_max, legend = c(other_user_name, owner), col = c('yellow2', 'blue3'), text.col = 'black', pch = c(5,6), lty = c(1,2), cex = 0.7)

#--------------------------------------------------------------------------------------#



# users = as.character(unique(lisa[,"user"]))
# user_tot_messages = c()
# user_words = c()
# most_common_word = c()
# coffee_intake = c()
# for (i in 1:length(users))
# {
#   user = users[i]
#   user_rows = which(lisa[,"user"] == user)
#   user_words[i] = paste(lisa[user_rows, "content"], collapse = ' ')
#   user_unique_words = strsplit(user_words[i], split = ' ')[[1]]
#   words_df = as.data.frame(table(user_unique_words))
#   most_common_word[i] = as.character(words_df[which(words_df[,"Freq"] == max(words_df[,"Freq"]))[1], "user_unique_words"])
#   coffee_intake[i] = sum(words_df[which(words_df[,"user_unique_words"] == 'coffee' | words_df[,"user_unique_words"] == 'Coffee' | words_df[,"user_unique_words"] == 'COFFEE'), "Freq"])
#   user_tot_messages[i] = length(user_rows)
# }
# 
# total_messages = cbind(users, user_tot_messages, most_common_word, coffee_intake)
# 
# top_ten = head(sort(user_tot_messages, decreasing = T), 10)
# 
# top_ten_users = c()
# for (j in 1:10)
# {
#   user = total_messages[which(total_messages[, 2] == top_ten[j]), 1]
#   top_ten_users[j] = user
# }
# 
# top_ten_user_info = cbind(top_ten_users, top_ten)




dim(alyssa)

## make sent_at to a date ##

sent_at_to_date = as.Date(alyssa[,'sent_at'], format = "%m/%d/%Y %I:%M%p %z")

#date_correct_format = strftime(sent_at_to_date, format = "%m/%d/%Y %I:%M%p %z")
alyssa[, "date"] = sent_at_to_date


## Task ONE : who started the converstaion ##
spoken_dates = unique(sent_at_to_date)

who_spoke_fisrt = c()


for (i in 1: length(spoken_dates))
{
  exact_date = spoken_dates[i]
  spoken_date_row = which(alyssa[, "date"] == exact_date)
  first_time_row = spoken_date_row[1]
  person_who_started_first = alyssa[first_time_row, "user"]
  who_spoke_fisrt[i] = as.character(person_who_started_first)
}

who_spoke_fisrt
table(who_spoke_fisrt)

## total number of messages ##
tot_mess_alyssa = length(which(alyssa[,"user"] == other_user_name))
tot_mess_chang = dim(alyssa)[1] - tot_mess_alyssa

## #words said ##
alyssa_rows = which(alyssa[, "user"] == other_user_name)
chang_rows = which(alyssa[, "user"] == owner)
words_str_alyssa = paste(alyssa[alyssa_rows, "content"], collapse = ' ')
split_words_alyssa = strsplit(words_str_alyssa, split = ' ')[[1]]
length(split_words_alyssa)

words_str_chang = paste(alyssa[chang_rows, "content"], collapse = ' ')
split_words_chang = strsplit(words_str_chang, split = ' ')[[1]]
length(split_words_chang)

## most used words ##
unique_words_alyssa = unique(tolower(split_words_alyssa))
length(unique_words_alyssa)
count_words = c()
for (j in 1: length(unique_words_alyssa))
{
  word_said = unique_words_alyssa[j]
  word_sum = sum(split_words_alyssa == word_said)
  count_words[j] = word_sum
}

unique_words_chang = unique(tolower(split_words_chang))
length(unique_words_chang)

## accept/reject ##
acceptance = c('yes', 'yup', 'yeah')
rejection = c('no', 'nah', 'nope')
al_accept = ch_accept = 0
al_reject = ch_reject = 0
for (i in 1: length(acceptance))
{
  accept_word = acceptance[i]
  al_y = sum(grepl(accept_word, split_words_alyssa))
  al_accept = al_accept + al_y
  ch_y = sum(grepl(accept_word, split_words_chang))
  ch_accept = ch_accept + ch_y
}
for (i in 1: length(rejection))
{
  reject_word = rejection[i]
  al_r = sum(grepl(reject_word, split_words_alyssa))
  al_reject = al_accept + al_r
  ch_r = sum(grepl(reject_word, split_words_chang))
  ch_reject = ch_accept + ch_r
}

## how about any word ##
sum(grepl('never', split_words_alyssa))
sum(grepl('hungry', split_words_alyssa))
sum(grepl('bored', split_words_alyssa))

## happiness index ##
assumed_happy_words = c(":)", ";)", "haha", 'yay', "hehe", "lol", ":D", ":P")
happiness = rep(0, length(spoken_dates))
happiness_me = rep(0, length(spoken_dates))
sent_al = sent_ch = rep(0, length(spoken_dates))
for (j in 1: length(spoken_dates))
{
  exact_date = spoken_dates[j]
  rows = which(alyssa[, "date"] == exact_date & alyssa[, "user"] == other_user_name)
  sent_al[j] = length(rows)
  if (length(rows) > 0)
  {
    for (k in 1: length(rows))
    {
      row = rows[k]
      string_to_search = tolower(alyssa[row, "content"])
      is_happy = sapply(assumed_happy_words, grepl, string_to_search)
      if (any(is_happy))
      {
        happiness[j] = happiness[j] + 1
      }
      
    }
  }
  rows_me = which(alyssa[, "date"] == exact_date & alyssa[, "user"] == owner)
  sent_ch[j] = length(rows_me)
  if (length(rows_me) > 0)
  {
    for (k1 in 1: length(rows_me))
    {
      row = rows_me[k1]
      string_to_search = tolower(alyssa[row, "content"])
      is_happy = sapply(assumed_happy_words, grepl, string_to_search)
      if (any(is_happy))
      {
        happiness_me[j] = happiness_me[j] + 1
      }
      
    }
  }
}
# Standardize the count a bit #
happiness = ifelse(sent_al != 0, happiness/sent_al * 100, 0)
happiness_me = ifelse(sent_ch != 0, happiness_me/sent_ch * 100, 0)

par(mar = c(7, 4.1, 4.1, 2.1))
subset = seq(1, length(spoken_dates), by = 10)
partial_sd = spoken_dates[subset]
happiness_max = max(c(happiness, happiness_me))
plot(spoken_dates, happiness, ylim = c(0, happiness_max), main = 'How often do you smile', type = 'o', col = 'red2', lty = 2, pch = 1, xaxt = 'n', xlab = '')
points(spoken_dates, happiness_me, type = 'o', col = 'cyan', lty = 3, pch = 2)
axis(1, at = partial_sd, labels = partial_sd, padj = 1, las = 2, cex = 0.1)
legend(spoken_dates[1], 0.9 * happiness_max, legend = c(other_user_name, owner), col = c('red2', 'cyan'), text.col = 'black', pch = c(1,2), lty = c(2,3), cex = 0.7)


## politeness index ##
assumed_polite_words = c("thank you", "thanks", "no problem", "sorry", "my bad", "my apologies", "my apology", "ty", "ta ", "well done", "fantastic", "excellent", "awesome", "great", "wonderful")
politeness = rep(0, length(spoken_dates))
sent_al = sent_ch = rep(0, length(spoken_dates))
politeness_me = rep(0, length(spoken_dates))
for (j in 1: length(spoken_dates))
{
  exact_date = spoken_dates[j]
  rows = which(alyssa[, "date"] == exact_date & alyssa[, "user"] == other_user_name)
  sent_al[j] = length(rows)
  if (length(rows) > 0)
  {
    for (k in 1: length(rows))
    {
      row = rows[k]
      string_to_search = tolower(alyssa[row, "content"])
      is_polite = sapply(assumed_polite_words, grepl, string_to_search)
      if (any(is_polite))
      {
        politeness[j] = politeness[j] + 1
      }
      
    }
  }
  rows_me = which(alyssa[, "date"] == exact_date & alyssa[, "user"] == owner)
  sent_ch[j] = length(rows_me)
  if (length(rows_me) > 0)
  {
    for (k1 in 1: length(rows_me))
    {
      row = rows_me[k1]
      string_to_search = tolower(alyssa[row, "content"])
      is_polite = sapply(assumed_polite_words, grepl, string_to_search)
      if (any(is_polite))
      {
        politeness_me[j] = politeness_me[j] + 1
      }
      
    }
  }
}
# Standardize the count a bit #
politeness = ifelse(sent_al != 0, politeness/sent_al * 100, 0)
politeness_me = ifelse(sent_ch != 0, politeness_me/sent_ch * 100, 0)

par(mar = c(7, 4.1, 4.1, 2.1))
subset = seq(1, length(spoken_dates), by = 10)
partial_sd = spoken_dates[subset]
politeness_max = max(c(politeness, politeness_me))
plot(spoken_dates, politeness, ylim = c(0, politeness_max), main = 'Politeness Index', type = 'o', col = 'green2', lty = 4, pch = 3, xaxt = 'n', xlab = '')
points(spoken_dates, politeness_me, type = 'o', col = 'purple1', lty = 5, pch = 4)
axis(1, at = partial_sd, labels = partial_sd, padj = 1, las = 2, cex = 0.1)
legend(spoken_dates[1], 0.9 * politeness_max, legend = c(other_user_name, owner), col = c('green2', 'purple1'), text.col = 'black', pch = c(3,4), lty = c(4,5), cex = 0.7)



## SWEARING index ##
assumed_swear_words = c("wth", "wtf", "fuck", "shit", "ass", "bloody", "hell", "moron", "ffs")
swear = rep(0, length(spoken_dates))
sent_al = sent_ch = rep(0, length(spoken_dates))
swear_me = rep(0, length(spoken_dates))
for (j in 1: length(spoken_dates))
{
  exact_date = spoken_dates[j]
  rows = which(alyssa[, "date"] == exact_date & alyssa[, "user"] == other_user_name)
  sent_al[j] = length(rows)
  if (length(rows) > 0)
  {
    for (k in 1: length(rows))
    {
      row = rows[k]
      string_to_search = tolower(alyssa[row, "content"])
      is_swear = sapply(assumed_swear_words, grepl, string_to_search)
      if (any(is_swear))
      {
        swear[j] = swear[j] + 1
      }
      
    }
  }
  rows_me = which(alyssa[, "date"] == exact_date & alyssa[, "user"] == owner)
  sent_ch[j] = length(rows_me)
  if (length(rows_me) > 0)
  {
    for (k1 in 1: length(rows_me))
    {
      row = rows_me[k1]
      string_to_search = tolower(alyssa[row, "content"])
      is_swear = sapply(assumed_swear_words, grepl, string_to_search)
      if (any(is_swear))
      {
        swear_me[j] = swear_me[j] + 1
      }
      
    }
  }
}
# Standardize the count a bit #
#swear = ifelse(sent_al != 0, swear/sent_al * 100, 0)
#swear_me = ifelse(sent_ch != 0, swear/sent_ch * 100, 0)

par(mar = c(7, 4.1, 4.1, 2.1))
subset = seq(1, length(spoken_dates), by = 10)
partial_sd = spoken_dates[subset]
swear_max = max(c(swear, swear_me))
plot(spoken_dates, swear, ylim = c(0, swear_max), main = 'Angst Index', type = 'o', col = 'yellow2', lty = 1, pch = 5, xaxt = 'n', xlab = '')
points(spoken_dates, swear_me, type = 'o', col = 'blue3', lty = 2, pch = 6)
axis(1, at = partial_sd, labels = partial_sd, padj = 1, las = 2, cex = 0.1)
legend(spoken_dates[1], 0.9 * swear_max, legend = c(other_user_name, owner), col = c('yellow2', 'blue3'), text.col = 'black', pch = c(5,6), lty = c(1,2), cex = 0.7)


