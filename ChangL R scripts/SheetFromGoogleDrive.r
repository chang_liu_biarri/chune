##########################################################################
### READ google sheet, from google drive ###
library(bitops)
library(RCurl)
library("googlesheets")


## Enter authorization code: 4/e2SeeK-_NcS0SCihFCHL-AwEoELZ7cv0EVFSc6FqI8Q ##

## get authority ##
gs_auth()
gs_user()

## PMO MASTER V2 key##
master_key = "1dvwJZGER5-JcPTB6-l4EiMmceSO1joZZi0QQON6IA4I" # Jeeves
#master_key = '1YjjnAjy7qA6M30pPmBJO-2EwAkokvTJlmxhg-3AYPho' # Jeeves-autoupdate



master_key_gs = gs_key(master_key, verbose = T)

Toolbox_ID_Sheet = gs_read(master_key_gs, ws = 'Toolbox List', header = T, col_names = T)
names(Toolbox_ID_Sheet)
ID_Non_Empty_LOC = which(!is.na(Toolbox_ID_Sheet$`Toolbox Sheet ID`))

Toolbox_ID = Toolbox_ID_Sheet$`Toolbox Sheet ID`[ID_Non_Empty_LOC]


Client = Toolbox_ID_Sheet$`Project ID`[ID_Non_Empty_LOC]

Hyperlink_ds = function(input)
{
  row_m = max(input$row)
  col_m = max(input$col)
  DF = data.frame()
  for( r1 in 1: row_m)
  {
    for( c1 in 1: col_m)
    {
      cell_loc = which(input$row == r1 & input$col == c1)
      if( length(cell_loc) >0)
      {
        HYP = grep('=HYPERLINK', input[cell_loc,6])
        if (length(HYP) > 0)
        {
          DF[r1,c1] = input[cell_loc,6]
        }else{
          DF[r1,c1] = input[cell_loc,5]
        }
      }
    }
  }
  DF
}

PRO_SCH = c()
RIS_REG = c()
LES_LEA = c()
LAS_UPD = c()
DOC_REG = c()
for (c1 in 1: length(Client))
{
  key = gs_key(Toolbox_ID[c1],lookup = F, visibility = 'private')
  
  ## Read in production schedule by cell, as it contains hyperlink. ##
  PS_CELL = gs_read_cellfeed(key,ws = 'Production Schedule')
  PS_NC = Hyperlink_ds(PS_CELL)
  PS_TIT = PS_NC[1,]
  PS = PS_NC[-1,]
  if(sum(is.na(PS)) == dim(PS_NC)[2])
  {
    PS = c()
  }
  RR = gs_read(key, ws = 'Risk Register', header = T)
  LL = gs_read(key, ws = 'Lessons Learned', header = T)
  LU = gs_read(key, ws = 'Last Updated',skip = 1, header = T)
  ## Document Register title has 2 rows!! Using col_names = F ##
  DR_CELL = gs_read_cellfeed(key, ws = 'Document Register')
  #DR_ALL = gs_read(key, ws = 'Document Register', col_names = F)
  DR_NC = Hyperlink_ds(DR_CELL)
  DR_TIT = DR_NC[1,]
  DR = DR_NC[-1,]
  if(sum(is.na(DR)) == dim(DR_NC)[2])
  {
    DR = c()
  }
  
  
  PRO_SCH = rbind(PRO_SCH,PS)
  RIS_REG = rbind(RIS_REG,RR)
  LES_LEA = rbind(LES_LEA,LL)
  LAS_UPD = rbind(LAS_UPD,LU)
  if(class(DOC_REG) =='data.frame')
  {
    if(c1 != 1 & dim(DOC_REG)[2] != dim(DR)[[2]])
    {
      DR = DR[, 1:dim(DOC_REG)[2]]
      names(DR) = names(DOC_REG)
    }
  }

  DOC_REG = rbind(DOC_REG,DR)
}

names(PRO_SCH) = PS_TIT
dif.col = length(DR_TIT) - dim(DOC_REG)[2]
# for( d1 in 1: dif.col)
# {
#   DOC_REG = cbind(DOC_REG, 0)
# }
DR_TIT = DR_TIT[-rev(1:length(DR_TIT))[1:dif.col]]
names(DOC_REG) = DR_TIT 

## Clean the data frames and remove those rows with no Project ID ##
Docname = c("PRO_SCH","RIS_REG","LES_LEA","LAS_UPD","DOC_REG")
for (d1 in 1: length(Docname))
{
  doc = eval(parse(text = Docname[d1]))
  pid_col = eval(parse(text = "which(names(doc) == 'Project ID' |names(doc) == 'PID' )"))
  if(length(pid_col) == 1)
  {
    #no_pid_row = eval(parse(text = 'which(is.na(doc[,pid_col]))'))
    no_pid_row = eval(parse(text = 'which(nchar(doc[,pid_col]) <=5 | is.na(doc[,pid_col]))'))
  }else{
    no_pid_row = c()
    print(paste('Cannot find ProjectID column for ', Docname[d1]))
  }
  
  if(length(no_pid_row) > 0)
  {
    eval(parse(text = paste(Docname[d1], "= doc[-no_pid_row,]")))
  }
  
  print(paste('Removed', length(no_pid_row), 'rows from', Docname[d1], sep = ' '))
}



# fake_key = '1KC3H1QnLOFrCZK5fWL2WfmZjFJkC-t_x5CevKuAkyFY'
# fake_gs_key = gs_key(fake_key)
# #fake_df = gs_read(fake_gs_key)
# gs_ws_ls(fake_gs_key)
# gs_ws_new(fake_gs_key, ws_title = 'Production Schedule', input = PRO_SCH)
# gs_ws_new(fake_gs_key, ws_title = 'Risk Register', input = RIS_REG)
# gs_ws_new(fake_gs_key, ws_title = 'Knowledge Base', input = LES_LEA)
# gs_ws_new(fake_gs_key, ws_title = 'Last Updated', input = LAS_UPD)
# gs_ws_new(fake_gs_key, ws_title = 'Document Register', input = DOC_REG)
# 
# 
# gs_ws_rename(fake_gs_key,from = 'Production Schedule', to = 'PS')
# gs_ws_delete(fake_gs_key, ws = 'Production Schedule')
# gs_edit_cells(fake_gs_key, ws = 'Production Schedule', input = PRO_SCH)


### TEST ON JEEVES-CHANG ###
#jc_key = '1YjjnAjy7qA6M30pPmBJO-2EwAkokvTJlmxhg-3AYPho'
#jc_gs_key = gs_key(jc_key)

#jc_ps = gs_read(jc_gs_key, ws = 'Production Schedule', header = T)


## Remove exiting data by an empty matrix, while keeping the title ##
#empty_mat = matrix(nrow = dim(jc_ps)[1]-1, ncol = dim(jc_ps)[2])

#gs_edit_cells(jc_gs_key,ws = 'Production Schedule', anchor = 'A2', input = empty_mat)

## Add production schedule data frame into the tab ##
#gs_edit_cells(jc_gs_key,ws = 'Production Schedule', anchor = 'A1', input = PRO_SCH)

###############################################
###### TANYA'S REQUEST  #####################

# Go to Jeeves - Production Schedule. 
# Filter 'Status' - Finalised.
# Filter 'Current End Date' - filter out the relevant dates.
# Count the 'Actual Premesis'
# Go to 'BAT Planning and Replicon Metrics' - Prems per week sheet
# Update B11
library(lubridate)
names(PRO_SCH)
#loc_1 = which(PRO_SCH$Status == 'Finalised')
#PRO_SCH$`Current End Date`[loc_1]
Whatistoday = wday(Sys.Date(), label = T)

Date_Seq = seq(Sys.Date()-7,Sys.Date(), length = 7)
Start_Mon = Date_Seq[which(wday(Date_Seq,label = T) == 'Mon')[1]]

Search_Period = as.Date(seq(Start_Mon, Start_Mon + 5, length = 5)) 
SP = format(Search_Period, format ='%d/%m/%Y' )

loc_2 = which(PRO_SCH$Status == 'Submitted' & PRO_SCH$`Current End Date` %in% SP)

Actual_Premises = PRO_SCH$`Actual Premises`[loc_2]

BAT_key = '1b84dkUnXxFKgu16jRAOaWZKvwf-01KbJG-tBRDzLwBI'
BAT_gs_key = gs_key(BAT_key)
Old_Record = gs_read(BAT_gs_key, ws = 'Prems per week', skip = 22, header = T)
 






Update_jeeves = function(jeeves_key)
{
  jkey = gs_key(jeeves_key)
  sheets = c('Production Schedule','Risk Register','Knowledge Base','Last Updated','Document Register')
  input_df = c('PRO_SCH','RIS_REG','LES_LEA','LAS_UPD','DOC_REG')
  line_skip = c(1,1,1,2,1)
  anchor_vec = paste('A', line_skip, sep = '')
  anchor_vec_emp = paste('A', line_skip+1, sep = '')
  for(s1 in 1:length(sheets))
  {
    jeeves_tab = gs_read(jkey, ws = sheets[s1], header = T)
    
    ## Remove exiting data by an empty matrix, while keeping the title ##
    if(dim(jeeves_tab)[1] > 1)
    {
      empty_mat = matrix('',nrow = dim(jeeves_tab)[1], ncol = dim(jeeves_tab)[2])
      
      gs_edit_cells(jkey,ws = sheets[s1], anchor = anchor_vec_emp[s1], input = empty_mat)
    }
    
    ## Add production schedule data frame into the tab ##
    eval_txt = paste('gs_edit_cells(jkey,ws = sheets[s1], anchor = anchor_vec[s1], input =', input_df[s1], ')', sep = '')
    eval(parse(text = eval_txt))
  }
}


jfake_key = '1F2ojgHo9JVq69lOZ7r1u_kjbIhM4Ocp7eXz7y50SC08' # Jeeves-autoupdate
Update_jeeves(jeeves_key = jfake_key)

#eval(parse(text = 'PRO_SCH'))




### TEST ON GOOGLE TOOLBOX ###
# gf_key = '18C6talLimkOBpetJ1KFYmEAnuI0243FFslT5C4Nz3JI'
# key0 = gs_key(gf_key)
# PS = gs_read_cellfeed(key0,ws = 'Production Schedule' )
# grep('=HYPERLINK',PS$input_value)
# link1 = PS$input_value[27]
# PS_reshape = gs_reshape_cellfeed(PS,literal = F)
# 
# 
# tit_loc = which(PS$row == 1)
# PS_NT = PS[-tit_loc,]
# row_m = max(PS$row)
# col_m = max(PS$col)
# PS_DF = data.frame()
# for( r1 in 1: row_m)
# {
#   for( c1 in 1: col_m)
#   {
#     cell_loc = which(PS$row == r1 & PS$col == c1)
#     if( length(cell_loc) >0)
#     {
#       HYP = grep('=HYPERLINK', PS[cell_loc,6])
#       if (length(HYP) > 0)
#       {
#         PS_DF[r1,c1] = PS[cell_loc,6]
#       }else{
#         PS_DF[r1,c1] = PS[cell_loc,5]
#       }
#     }
#   }
# }
# PS_DF_TIT = PS_DF[1,]
# PS_DF = PS_DF[-1,]




##################################################
##################################################
### APPENDIX ###

#url = 'https://docs.google.com/spreadsheets/d/1lZv4n7LmMX_4ihX2ioHAnkz6tmkMRjbe1TheZbG-lrE/edit#gid=86406336'
#extract_key_from_url(url)

#gs_title('BN Production Schedule PROJECTS', verbose = T)
#gf_dr_title = gs_read_cellfeed(gf_gs_key, ws = 'Document Register', range = 'A1:H1', col_names = F)

#gs_browse(BN_PS, ws = 5)

