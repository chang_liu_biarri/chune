### Polygon Vertices To Points ###
library(maptools)
library(rgdal)

library(sp)
library(rgdal)
library(rgeos)

parcel = readOGR("C:\\Users\\chang\\Documents\\Solve\\LNCC01\\DesignData", layer = 'parcels',stringsAsFactors = F)
cand_node = readOGR("C:\\Users\\chang\\Documents\\Solve\\LNCC01\\hut_solve", layer = 'candidate_nodes',stringsAsFactors = F)


vertices = matrix(0, nrow = 1, ncol = 2)
for ( i in 1: dim(parcel)[1])
{
  co = parcel[i,]@polygons[[1]]@Polygons[[1]]@coords
  vertices = rbind(vertices, co)
}


cd_1 = cand_node[1,]@coords
loc = which(abs(vertices[,1] - cd_1[1,1]) <= 1e-5 & abs(vertices[,2] - cd_1[1,2])<= 1e-05 )


