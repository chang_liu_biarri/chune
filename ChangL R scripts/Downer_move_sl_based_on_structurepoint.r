library(sp)
library(rgdal)
library(rgeos)



### Need PP_inputs\SLs (HLD), PP_inputs\land parcel (DDD), FTTC MDU polygon, PP_input\SLs (DDD). ###
sl_ddd = readOGR("C:\\Users\\chang\\Documents\\Downer\\downer_2mon_25\\04_ddd\\input_layers\\PP_inputs",layer = "sl", stringsAsFactors = F)
sl_hld = readOGR("C:\\Users\\chang\\Documents\\Downer\\downer_2mon_25\\02_hld\\input_layers\\PP_inputs",layer = "sl", stringsAsFactors = F)
parcel = readOGR("C:\\Users\\chang\\Documents\\Downer\\downer_2mon_25\\04_ddd\\input_layers\\PP_inputs",layer = "land parcels", stringsAsFactors = F)
MDU_poly = readOGR("C:\\Users\\chang\\Documents\\Downer\\downer_2mon_25\\04_ddd\\notes_and_feedback",layer = "for_chang", stringsAsFactors = F)

#Find all sls in DDD 
loc_sinp = c()
for(i1 in 1:dim(sl_ddd)[1])
{
  is_in = gWithin(sl_ddd[i1,], MDU_poly, byid = F)
  if(is_in == T)
  {
    loc_sinp = c(loc_sinp,i1)
  }
}
sl_tm_ddd = sl_ddd[loc_sinp,]
#see if there is a match in HLD
for(j1 in 1:dim(sl_tm_ddd)[1])
{
  match_id_loc = which(sl_hld$id == sl_tm_ddd$id[j1])
  #if so, change the geom to that in HLD
  if (length(match_id_loc) == 1)
  {
    sl_tm_ddd@coords[j1,][1] = sl_hld[match_id_loc,]@coords[1]
    sl_tm_ddd@coords[j1,][2] = sl_hld[match_id_loc,]@coords[2]
  }
}

#output

writeOGR(sl_tm_ddd,dsn = "C:\\Users\\chang\\Documents\\Downer\\downer_2mon_25\\04_ddd\\notes_and_feedback", layer = 'SL_inside_FTTC_MDU_moved', driver = 'MapInfo File', dataset_options="FORMAT=MIF")
