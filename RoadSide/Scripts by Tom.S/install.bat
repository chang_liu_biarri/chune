:: BASTARDISED INSTALLER NOT FOR USE ON PRODUCTION HUTS!
:: Save as a .bat file and run to make a new virtual environment


:: Automatically install google fibre for on windows.
:: This script creates a virtualenv called env. 
:: Comment that bit out if you don't want it.

:: If a virtualenv doesn't exist...
IF NOT EXIST "%CD%\env" (
    :: Create it
    virtualenv "--prompt=(BAT_DEV)" env
)
CALL env\Scripts\activate

python -m pip install pip setuptools wheel --upgrade
pip install --upgrade google_fibre -i https://devpi:YJyIqWmjqPvG4__m@devpi.biarrinetworks.com/biarri/master

:: BAT DEV OPTIONAL EXTRAS
pip install jupyter
pip install pandas
pip install pulp

:: Once all that is done you should be able to run gf_preprocessor, hippo_habitat_client
:: and gf_cabling as commands. Enjoy!