# -*- coding: utf-8 -*-
"""
Created on Wed Aug 19 15:47:49 2015

@author: thomasalexanderstubbs
"""

from qgis.core import *
from PyQt4.QtCore import *
import qgis.utils
import processing
from time import time
from math import *
import os

arcs = "candidates"


filter_length = 1.0
filter_connectivity = 1.0

calibrate = True
mult = 1.0

CHUNK_SIZE = 5000
CHUNK_THRESHOLD = 2 * CHUNK_SIZE


dx = -1e-9 #splitting line dx
cut_dx = 1e-7
ctol = 1e-8 #crossing tolerance
intersect_radius = 1e-1

dec = 7
dec2 = 7
branches = 3

class pit2pit(object):
        def __init__(self):
                #self.layer, self.layerDict, self.layerIndex = self.makeLayerDict(layer_name)
                self.map_canvas = qgis.utils.iface.mapCanvas()

        def refreshMap(self):
                self.map_canvas.refresh()


        def getLayer(self, layer_keyword):
                self.refreshMap()
                possible_layers = []
                for layer in qgis.utils.iface.mapCanvas().layers():
                        if layer_keyword == layer.name():
                                return layer
                        else:
                                if layer_keyword in layer.name():
                                        possible_layers.append(layer)

                for layer in possible_layers:
                        if layer_keyword in layer.name():
                                return layer

                possible_layers = [(layer_name, layer) for layer_name, layer in
                               QgsMapLayerRegistry.instance().mapLayers().items() if layer_keyword in layer_name]
                if possible_layers:
                        return sorted(possible_layers)[-1][1]
                print "Couldn't find a layer containing '{0}'".format(layer_keyword)
                return None

        def getLayerDictAndIndex(self, layer):
                layerDict = {feature.id(): feature for feature in layer.getFeatures()}
                index = QgsSpatialIndex()
                for feature in layer.getFeatures():
                        index.insertFeature(feature)
                return layerDict, index

        def sign(self,x):
                if x>0:
                        return 1.0
                else:
                        return -1.0

        def makeLayerDict(self, name):
                layer = None
                for layer_name in qgis.utils.iface.mapCanvas().layers():
                    if name in layer_name.name():
                        layer = layer_name
                        layerDict = {feature.id(): feature for feature in layer.getFeatures()}
                if layer == None:
                    print "Couldn't find a layer containing %s" % name
                index = QgsSpatialIndex()
                for f in layerDict.values():
                    index.insertFeature(f)
                return layer, layerDict, index

        def cleangeometry(self,lines1):
                temp = []
                for line in lines1:
                    if len(line)>1:
                        temp.append(line)

                lines = temp
                new_lines = []
                for line in lines:
                        new_line =[]
                        for point in line:
                                if len(new_line) == 0:
                                        new_line.append(point)
                                elif point == new_line[-1]:
                                        0
                                else:
                                        new_line.append(point)

                        new_lines.append(new_line)

                return new_lines

        def getLineVertices(self, layer):
                assert layer.wkbType() == 2
                if 1:
                        line_vertices = [[[round(point.x(),dec), round(point.y(),dec)] for point in line.geometry().asPolyline()] for line in layer.getFeatures()]
                else:
                        line_vertices = [[[point.x(), point.y()] for point in line.geometry().asPolyline()] for line in layer.getFeatures()]

                return self.cleangeometry(line_vertices)


        def linestopoints(self,lines,name,attribute = 0,field_name=0,attributes_list=0):
                if attribute:
                    layer_type = "Point"
                    new_layer_name = name
                    new_layer = QgsVectorLayer(layer_type + "?crs=epsg:4326", new_layer_name, "memory")
                    new_layer.startEditing()
                    layer_attributes = []
                    for field in field_name:
                        layer_attributes.append(QgsField(field, QVariant.String, "Float", 80, 0, None))
                    new_layer.dataProvider().addAttributes(layer_attributes)

                    new_features = []
                    new_dp = new_layer.dataProvider()
                    ptr = 0
                    for line in lines:
                            for point in line:
                                    new_feature = QgsFeature()
                                    new_feature.setGeometry(QgsGeometry.fromPoint(QgsPoint(point[0], point[1])))
                                    new_feature.setAttributes(attributes_list[ptr])
                                    new_features.append(new_feature)
                                    ptr+=1
                    if len(new_features) > CHUNK_THRESHOLD:
                            new_feature_chunks = [new_features[x:x + CHUNK_SIZE] for x in xrange(0, len(new_features), CHUNK_SIZE)]
                            for chunk in new_feature_chunks:
                                    new_dp.addFeatures(chunk)
                                    new_layer.updateExtents()
                    else:
                            new_dp.addFeatures(new_features)
                    new_layer.commitChanges()
                    QgsMapLayerRegistry.instance().addMapLayer(new_layer)
                    return new_layer
                else:
                    layer_type = "Point"
                    new_layer_name = name
                    new_layer = QgsVectorLayer(layer_type + "?crs=epsg:4326", new_layer_name, "memory")
                    new_layer.startEditing()
                    new_features = []
                    new_dp = new_layer.dataProvider()

                    for line in lines:
                            if len(line)>0:
                                for point in line:
                                    if len(point)>0:
                                        new_feature = QgsFeature()
                                        new_feature.setGeometry(QgsGeometry.fromPoint(QgsPoint(point[0], point[1])))
                                        new_features.append(new_feature)
                    if len(new_features) > CHUNK_THRESHOLD:
                            new_feature_chunks = [new_features[x:x + CHUNK_SIZE] for x in xrange(0, len(new_features), CHUNK_SIZE)]
                            for chunk in new_feature_chunks:
                                    new_dp.addFeatures(chunk)
                                    new_layer.updateExtents()
                    else:
                            new_dp.addFeatures(new_features)
                    new_layer.commitChanges()
                    QgsMapLayerRegistry.instance().addMapLayer(new_layer)
                    return new_layer


        def verticestolineshape(self,lines,name):
                layer_type = "LineString"
                new_layer_name = name
                new_layer = QgsVectorLayer(layer_type + "?crs=epsg:4326", new_layer_name, "memory")
                new_layer.startEditing()
                new_features = []
                new_dp = new_layer.dataProvider()

                for line in lines:
                        if len(line)<2:
                                0
                        else:
                                qlines = []
                                for point in line:
                                        qlines.append(QgsPoint(point[0],point[1]))
                                new_feature = QgsFeature()
                                new_feature.setGeometry(QgsGeometry.fromPolyline(qlines))

                                new_features.append(new_feature)
                if len(new_features) > CHUNK_THRESHOLD:
                        new_feature_chunks = [new_features[x:x + CHUNK_SIZE] for x in xrange(0, len(new_features), CHUNK_SIZE)]
                        for chunk in new_feature_chunks:
                                new_dp.addFeatures(chunk)
                                new_layer.updateExtents()
                else:
                        new_dp.addFeatures(new_features)
                new_layer.commitChanges()
                QgsMapLayerRegistry.instance().addMapLayer(new_layer)
                return new_layer


        def getPolygonVertices(self, layer):
                assert layer.wkbType() == 3
                polygons = []
                iter = layer.getFeatures()
                for feature in iter:
                        geom = feature.geometry()
                        x = geom.asPolygon()
                        for i in range(len(x)):
                                temp = []
                                for point in x[i]:
                                        temp.append([point[0],point[1]])

                                polygons.append(temp)

                return self.cleangeometry(polygons)


        def getpointvertices(self, layer):
                assert layer.wkbType() == 1
                points = []
                for feature in layer.getFeatures():
                        geom = feature.geometry()
                        points.append([round(geom.asPoint()[0],dec),round(geom.asPoint()[1],dec)])
                return points

        def boundingbox(self,line):
                point1 = line[0]
                point2 = line[-1]
                x1 = point1[0]
                y1 = point1[1]
                x2 = point2[0]
                y2 = point2[1]

                if x1>x2:
                        xr = x1
                        xl = x2
                else:
                        xr = x2
                        xl = x1

                if y1>y2:
                        ya = y1
                        yb = y2
                else:
                        ya = y2
                        yb = y1

                return xl,xr,yb,ya

        def lineinfo(self,line):
                point1 = line[0]
                point2 = line[-1]
                tol = 1e-8
                x1 = point1[0]
                x2 = point2[0]
                y1 = point1[1]
                y2 = point2[1]

                if x1 == x2 and y1 != y2:
                        m = 'inf'
                        c = 0
                elif x1 == x2 and y1==y2:
                        m = 0
                        c = 0
                else:
                        m = (y2-y1)/(x2-x1)
                        c = y1-(y2-y1)/(x2-x1)*x1


                return m,c


        def ecrossing(self,line1,line2):
                m1,c1 = self.lineinfo(line1)
                m2,c2 = self.lineinfo(line2)
                #print m1,m2
                if m1 == m2:
                        if c1 == c2:
                                return [-1] #parallel
                        else:
                                return [0]

                elif m1 == 'inf':
                        xc = line1[0][0]
                        xl,xr,yb,ya = self.boundingbox(line2)
                        xl += dx
                        xr -= dx
                        yb += dx
                        ya -= dx
                        if xl <= xc and xc <= xr:
                                yc = m2*xc+c2
                                if yb <= yc and yc <= ya:
                                        xl,xr,yb,ya = self.boundingbox(line1)
                                        xl += dx
                                        xr -= dx
                                        yb += dx
                                        ya -= dx
                                        if yb <= yc and yc <= ya:
                                                if xl <= xc and xc <= xr:
                                                        return [xc,yc]
                        else:
                                return [0]

                elif m2 == 'inf':
                        xc = line2[0][0]
                        xl,xr,yb,ya = self.boundingbox(line1)
                        xl += dx
                        xr -= dx
                        yb += dx
                        ya -= dx
                        if xl <= xc and xc <= xr:
                                yc = m1*xc+c1
                                if yb <= yc and yc <= ya:
                                        xl,xr,yb,ya = self.boundingbox(line2)
                                        xl += dx
                                        xr -= dx
                                        yb += dx
                                        ya -= dx
                                        if yb <= yc and yc <= ya:
                                                if xl <= xc and xc <= xr:
                                                        return [xc,yc]

                        else:
                                return [0]
                else:
                        xc = (c2-c1)/(m1-m2)
                        yc = m1*xc + c1

                #check is contained in first arc
                xl,xr,yb,ya = self.boundingbox(line1)
                xl += dx
                xr -= dx
                yb += dx
                ya -= dx
                if xl <= xc and xc <= xr:
                        if yb <= yc and yc <= ya:
                                xl,xr,yb,ya = self.boundingbox(line2)
                                xl += dx
                                xr -= dx
                                yb += dx
                                ya -= dx
                                if xl <= xc and xc <= xr:
                                        if yb <= yc and yc <= ya:
                                                return [xc,yc]
                return [0]


        def index(self,ptlist):
                points = []
                for point in ptlist:
                        points.append((point[0],point[1]))
                ptlist = list(set(points))
                sort = sorted(ptlist)
                return sort

        def norm2(self, point1,point2):
                norm = ((point1[0]-point2[0])**2.0+(point1[1]-point2[1])**2.0)**0.5
                return norm


        def endpoints(self,lines):
                lines1 = []
                for line in lines:
                        lines1.append([line[0],line[-1]])
                return lines1

        def buildindex(self,lines):
                print "building spatial index"
                print "building vertices set"
                vertices = []
                for line in lines:
                        for point in line:
                                vertices.append((point[0],point[1]))

                index_vertices = sorted(list(set(vertices)))
                print "Unique vertices found = %g" % len(index_vertices)

                print "building segments library"

                Segments = []
                Lines = []
                Segmentsindex = []
                Linesindex = []
                Linevertexindex = []
                m = len(index_vertices)
                a = 0

                tempsegments = []
                c = 0
                rr = 0
                m = len(lines)
                for line in lines:
                        n = len(line)
                        ct = 0
                        c +=1

                        if n <2:
                                0
                        else:
                                for i in range(n-1):
                                        tempsegments.append([line[i],line[i+1],"segment",rr,c-1])
                                        tempsegments.append([line[i+1],line[i],"segment",rr,c-1])
                                        rr+=1


                                tempsegments.append([line[0],line[-1],"line",c-1,0])
                                tempsegments.append([line[-1],line[0],"line",c-1,-1])




                tempsegments1 = sorted(tempsegments)

                j = 0
                m = len(index_vertices)
                n = len(tempsegments1)

                print "indexing vertex segments"

                status = 0
                ptr = 0.0
                display = "index"

                TEMPLINES = []
                TEMPSEGMENTS = []
                for i in range(c):
                    TEMPLINES.append([])

                for i in range(n):

                    TEMPSEGMENTS.append([])

                for point in index_vertices:
                        status = self.printstatus(status,ptr,m,display)

                        templine = []
                        templineindex = []
                        segmentindex = []
                        linevertexindex = []

                        a+=1
                        #print "indexing %g of %g vertices" % (a,m)
                        segments = []
                        x = point[0]
                        found = 0

                        i = j
                        x0 = x
                        #print "warms start from %g" % i
                        while x0<x+1e-8 and i<n:
                                seg = tempsegments1[i]
                                x0 = seg[0][0]
                                if seg[0] == [point[0],point[1]]:
                                        if seg[2] == "line":
                                                position = tempsegments1[i][3]
                                                #linevertexindex.append(position)
                                                TEMPLINES[position].append(int(ptr))
                                        else:
                                                segments.append(seg[0:2])
                                                templineindex.append(seg[4])
                                                position = tempsegments1[i][3]
                                                TEMPSEGMENTS[position].append(int(ptr))
                                                if found == 0:
                                                        found = 1
                                                        j = i

                                i+= 1


                        #for segment in tempsegments1:
                        #delete duplicate segments
                        p = 0
                        new_seg = []
                        for seg in segments:
                                if p == 0:
                                        new_seg.append(sorted(seg))
                                        p = 1
                                else:
                                        q = 0
                                        for new_se in new_seg:
                                                rr = sorted(seg)
                                                if new_se == rr:
                                                        q = 1
                                                        break
                                        if q == 0:
                                                new_seg.append(rr)

                        temp = list(set(templineindex))
                        Linesindex.append(temp)
                        Segmentsindex.append(segmentindex)
                        Segments.append((new_seg))
                        Linevertexindex.append(linevertexindex)
                        ptr+=1

                print "fleshing out segment connectivity index"

                ptr = 0
                #print TEMPLINES

                for line in TEMPLINES:
                    n = len(line)
                    for i in range(n):
                        point = int(line[i])
                        for j in range(n):
                            if i != j:
                                Linevertexindex[point].append(line[j])
                    ptr+=1

                ptr = 0
                #print TEMPSEGMENTS
                #   print TEMPLINES
                for line in TEMPSEGMENTS:
                    n = len(line)
                    for i in range(n):
                        point = int(line[i])
                        for j in range(n):
                            if i != j:
                                Segmentsindex[point].append(line[j])
                    ptr+=1


                for i in range(len(Segmentsindex)):
                        Segmentsindex[i] = list(set(Segmentsindex[i]))

                for line in Linesindex:
                    temp = []
                    for index in line:
                        temp.append(lines[index])
                    Lines.append(temp)
                """
                for indexlist in Segmentsindex:
                        for point in indexlist:
                                Segmentsindex[point].append(kk)
                        kk += 1


                for i in range(len(Segmentsindex)):
                        Segmentsindex[i] = list(set(Segmentsindex[i]))

                #print Segmentsindex

                print "fleshing out Line connectivity index"
                kk = 0

                #print Linevertexindex
                for indexlist in Linevertexindex:
                        for point in indexlist:
                                Linevertexindex[point].append(kk)
                        kk += 1

                #print Linevertexindex
                for i in range(len(Linevertexindex)):
                        Linevertexindex[i] = list(set(Linevertexindex[i]))
                """

                print "indexing vertex lines"
                print "Library complete"

                return index_vertices,Segments,Segmentsindex,Lines,Linesindex, Linevertexindex




        def densify(self,lines,rcal):
                new_lines = []
                for line in lines:
                        m = len(line)
                        new_line = []
                        new_line.append(line[0])
                        for i in range(m-1):
                                r = self.norm2(line[i],line[i+1])
                                if r > rcal:
                                        n = int(r/rcal)
                                        dx = line[i+1][0]-line[i][0]
                                        dy = line[i+1][1]-line[i][1]
                                        point = line[i]


                                        for j in range(1,n-1):
                                                x = point[0]+dx*j/float(n)
                                                y = point[1]+dy*j/float(n)
                                                new_line.append([x,y])

                                new_line.append(line[i+1])
                        new_lines.append(new_line)

                return new_lines


        def distancebetweenlatlong(self,point1,point2):
            """
            var R = 6371000; // metres
            var φ1 = lat1.toRadians();
            var φ2 = lat2.toRadians();
            var Δφ = (lat2-lat1).toRadians();
            var Δλ = (lon2-lon1).toRadians();

            var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                    Math.cos(φ1) * Math.cos(φ2) *
                    Math.sin(Δλ/2) * Math.sin(Δλ/2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

            var d = R * c;
            """
            R = 6373000.0

            lat1 = radians(point1[1])
            lon1 = radians(point1[0])
            lat2 = radians(point2[1])
            lon2 = radians(point2[0])

            dlon = lon2 - lon1
            dlat = lat2 - lat1

            a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
            c = 2 * atan2(sqrt(a), sqrt(1 - a))

            distance = R * c

            return distance

        def nearby(self,vertex_set1,vertex_set2,rcal,rmin = 1e-9):
                print "finding nearby vertices"
                nearby_vertices = []
                j = 0
                i = 0
                m = len(vertex_set2)
                mm = len(vertex_set1)
                status = 0
                ptr = 0


                if mm > 1e6:
                        decimals = 3
                elif mm  > 4e4:
                        decimals = 2
                else:
                        decimals = 0

                for point in vertex_set1:
                        ptr+= 1
                        status1 = round(ptr/float(mm),decimals)
                        if status1 == status:
                                0
                        else:
                                print "Completed nearby search for %g pct of network" % (status1*100)
                                status = status1
                        temp = []
                        found = 0
                        i = j
                        x = point[0]
                        y = point[1]
                        x0 = x
                        y0 = y
                        r1 = 1000
                        while x0<x+rcal and i <m:

                                point1 = vertex_set2[i]
                                x0 = point1[0]
                                y0 = point1[1]

                                if found == 0:
                                        if x - rcal <= x0:
                                                found = 1
                                                j = i

                                if (y0>y-rcal) and (y0<y+rcal):
                                        r = self.norm2(point,point1)
                                        if r<rcal and r>=rmin:
                                                r = self.distancebetweenlatlong(point,point1)
                                                temp.append([r,i])

                                i+= 1
                        nearby_vertices.append(sorted(temp))
                return nearby_vertices


        def branch(self,point_index,vertex_index,line_vertex_index):

                #straight line approximation between segment ends

                tree = [[[point_index]]]
                candidate_points = line_vertex_index[point_index]
                temp = []
                for point in candidate_points:
                    temp.append([point_index,point])
                tree.append(temp)
                for i in range(branches):
                        temp = []
                        for index in tree[-1]:
                                #print tree
                                #print index
                                for new_branches in line_vertex_index[index[-1]]:
                                        tempb = []
                                        for point in index:
                                            tempb.append(point)
                                        tempb.append(new_branches)
                                        temp.append(tempb)
                        tree.append(temp)
                return tree

        def mappointset(self,vertex_set1,vertex_set2):
                #assume 1 to 1 mapping
                index = []
                n = len(vertex_set2)
                i = 0
                ctr = 0
                j = 0
                ptr = 0
                for point in vertex_set1:

                        ctr = 0
                        if n>10:
                            i = j-10
                        else:
                            i = 0
                        ptr = 0

                        while i <n and ctr == 0:
                                point1 = vertex_set2[i]
                                if ptr == 0:
                                        if point[0] == point1[0]:
                                                ptr = 1
                                                j = i
                                if [point[0],point[1]] == [point1[0],point1[1]]:
                                        index.append(i)
                                        ctr = 1
                                else:
                                        i+=1
                        if ctr == 0:
                                0
                                #print "Failed to find"
                                #print point,j
                                index.append(j)
                #print index
                #print vertex_set1
                #print vertex_set2
                print len(index),len(vertex_set1)
                assert len(index) == len(vertex_set1)
                return index



        def nearbypits(self,pits_indexed,rcal):
                #return list of nearby pits ordered by distance
                nearby_pits = self.nearbybox(pits_indexed,pits_indexed,10,10,rcal)
                #print nearby_pits
                return nearby_pits

        def streetcross(self,pits_indexed,street_vertices,street_segments,rcal,nearby_pits):
                Lineofsight = []
                Lineofsightindex = []


                nearby_street_vertices = self.nearbybox(pits_indexed,street_vertices,10,10,rcal)
                n = len(pits_indexed)
                m = len(nearby_street_vertices)
                assert m == n

                Segments = []
                for i in range(n):
                        nearby_segments = []
                        nearby_street = nearby_street_vertices[i]
                        for index in nearby_street:
                                for segment in street_segments[index[1]]:
                                        nearby_segments.append(segment)

                        segments = sorted(nearby_segments)
                        temp = []
                        if len(segments)>0:
                                temp = [segments[0]]
                                for i in range(1,len(segments)):
                                        if segments[i] == temp[-1]:
                                                0
                                        else:
                                                temp.append(segments[i])

                        Segments.append(sorted(temp))


                for i in range(n):
                        point1 = pits_indexed[i]
                        ctr = 0
                        Line = []
                        Index = []
                        for point in nearby_pits[i]:
                                index = point[1]
                                point2 = pits_indexed[index]
                                for segment in Segments[i]:
                                        line = [[point1[0],point1[1]],[point2[0],point2[1]]]
                                        #print line,segment
                                        cross = self.ecrossing(line,segment)
                                        #print cross
                                        #Lineofsight.append(segment)
                                        if len(cross)>1:
                                                ctr = 1
                                                Line = line
                                                Index.append(index)
                                                break

                                if ctr == 1:
                                        0
                                        break

                        Lineofsight.append(Line)
                        Lineofsightindex.append(Index)

                return Lineofsight,Lineofsightindex,nearby_street_vertices


        def minpath(self,end_point,tree,vertex_index,line_vertex_index):
                path_length = 0
                start = tree[0][0][-1]
                n = len(tree)
                subtrees = []
                ctr = 0

                a = set([end_point])
                for i in range(n):
                        tempb = []
                        for point in tree[i]:
                                tempb.append(point[-1])

                        b = set(tempb)
                        if a.issubset(b):
                                ctr = 1
                                break


                if ctr == 1:
                        for j in range(len(tree[i])):
                                #print tree[i][j][-1]
                                if tree[i][j][-1] == end_point:
                                        temp = []
                                        for point in tree[i][j]:
                                                temp.append(vertex_index[point])

                                        m = len(tree[i][j])
                                        d  = 0
                                        for k in range(m-1):
                                                d += self.norm2(temp[k],temp[k+1])
                                        subtrees.append(d)
                                        #print "subtree"
                                        #print subtrees

                        return min(subtrees)

                else:
                        return "no"





        def existingconnectivity(self,pit2trenchmap, lineofsight,lineofsightindex,vertex_index,line_vertex_index,rcal):
                New_connections = []
                New_connections_index = []
                n = len(lineofsight)

                for i in range(n):
                        new_connections = []
                        indexxx = []
                        if len(lineofsight[i])>1:
                                point_index = pit2trenchmap[i]
                                desired_index = pit2trenchmap[lineofsightindex[i][0]]
                                tree = self.branch(point_index,vertex_index,line_vertex_index)
                                tempa = []
                                branch = []
                                #print tree
                                for seta in tree:
                                        for point in seta:
                                                if 0:
                                                    branch.append(point)
                                                else:
                                                    branch.append(point[-1])
                                final = set(tuple(branch))
                                test = set(tuple([desired_index,point_index]))
                                #print final, test,point_index, test.issubset(final)
                                if test.issubset(final):
                                        r = self.minpath(desired_index,tree,vertex_index,line_vertex_index)
                                        if r>rcal*filter_connectivity:
                                                new_connections = lineofsight[i]
                                                indexxx.append([point_index,desired_index])


                                else:
                                        new_connections = lineofsight[i]
                                        indexxx.append([point_index,desired_index])


                        New_connections.append(new_connections)
                        New_connections_index.append(indexxx)

                return New_connections, New_connections_index


        def streetorthogonal(self,new_connections_index,pits_indexed,street_segments,nearby_street_vertices,rcal):
                #find closest street and return street orthogonal
                print "finding pit orthogonal"
                Orthogonal = []
                n = len(new_connections_index)

                for i in range(n):
                        Segments = []
                        sega = 0
                        if len(new_connections_index[i])>0 and len(nearby_street_vertices[i])>0:
                                point = pits_indexed[i]
                                nearest_street_index = nearby_street_vertices[i][0][1]
                                segments = street_segments[nearest_street_index]
                                r1 = rcal*1000
                                for seg in segments:
                                        ra = self.norm2(seg[0],point)
                                        rb = self.norm2(seg[1],point)
                                        if ra+rb < r1:
                                                r1 = ra + rb
                                                sega = seg

                                m,c = self.lineinfo(sega)

                                if m == 0 or m == 'inf':
                                        0
                                else:
                                        m1 = -1.0/m
                                        c1 = point[1]-m1*point[0]
                                        x = point[0]+ rcal*cos(atan(m1))*self.sign(((c1-c)/(m-m1)-point[0]))
                                        #x = self.sign(x)*rcal
                                        y = -1.0/m*(x)+c1

                                        Segments = [[point[0],point[1]],[x,y]]

                        Orthogonal.append(Segments)


                return Orthogonal

        def mappitstodemandpoints(self,pits,demand):
                mapping = []
                pitset = set(pits)
                for line in demand:
                        if len(line)>1:
                                if set((line[0][0],line[0][1])).issubset(pitset):
                                        demand_point = line[1]
                                        pit = line[0]
                                        r = self.norm2(demand_point,pit)
                                        mapping.append((demand_point,pit,r))
                                else:
                                        demand_point = line[0]
                                        pit = line[1]
                                        r = self.norm2(demand_point,pit)
                                        mapping.append((demand_point,pit,r))

                return mapping


        def shouldisplit(self,point,mapping):
                for line in mapping:
                        r = line[2]
                        demand_point = line[0]
                        if self.norm2(demand_point,point)<=r:
                                return "bad_node"
                return 0


        def orthogonaltovalid(self,pits_indexed,lineofsight,orthogonal,trench_vertex,trench_Lines,mapping,rcal,nearby_trench_vertex):
                n = len(lineofsight)
                new_lines = []
                new_pits = []
                new_splits = []
                new_arcs = []
                delete_arcs = []
                m = len(lineofsight)
                j = 0

                assert len(pits_indexed) == len(lineofsight)


                for z in range(n):
                        if len(lineofsight[z])>1 and len(orthogonal[z])>1:
                                point1 = lineofsight[z][0]
                                point2 = lineofsight[z][1]
                                point3 = orthogonal[z][1]
                                line = orthogonal[z]
                                contact_points =[]

                                print "checking crossing of arc %g of %g" % (j,m)
                                cro = 0
                                Trench = []
                                for index in nearby_trench_vertex[z]:
                                        index1 = index[1]
                                        Lines = trench_Lines[index1]
                                        for lineaa in Lines:
                                                Trench.append(lineaa)
                                #print Trench

                                for line1 in Trench:
                                        #print "line1=",line1
                                        n = len(line1)
                                        for i in range(n-1):
                                                #print i
                                                line2 = [line1[i],line1[i+1]]
                                                #print line,line2
                                                cross = self.ecrossing(line,line2)
                                                #print line,line2
                                                #print cross,line,line2
                                                if len(cross)<2:
                                                        0
                                                else:
                                                        contact_points.append([cross,line1,i])
                                                        #print "hit"
                                                        #print cross,len(cross)
                                        if cro == 1:
                                                break
                                if len(contact_points)> 0:
                                        r = 1000
                                        x0 = 0
                                        y0 = 0
                                        pointa = 0
                                        for data in contact_points:
                                                point = data[0]
                                                lineb = data[1]
                                                index = data[2]


                                                r1 = self.norm2(orthogonal[z][0], point)
                                                if r1<r and abs(r1)>1e-8:
                                                        r = r1
                                                        pointa = point
                                                        linec = lineb
                                                        ida = index
                                        if pointa != 0:
                                            r = self.norm2(point1,point2)
                                            r1 = self.norm2(point2,pointa)
                                            if 0:
                                                    0
                                            else:
                                                    if point2 == linec[0]:
                                                            #print "case1"

                                                            new_lines.append([orthogonal[z][0], pointa])
                                                            line = [orthogonal[z][0], pointa]
                                                            for poiint in linec[ida:0:-1]:
                                                                    line.append(poiint)
                                                            line.append(linec[0])
                                                            #print line
                                                    elif point2 == linec[-1]:
                                                            #print "case2"
                                                            new_lines.append([orthogonal[z][0], pointa])
                                                            line = [orthogonal[z][0], pointa]
                                                            for poiint in linec[ida+1:len(linec)]:
                                                                    line.append(poiint)
                                                    else:
                                                            line = []
                                                            #print line
                                                    new_pits.append(line)
                                        else:
                                            new_lines.append([])
                                            new_pits.append([])

                                else:
                                        new_lines.append([])
                                        new_pits.append([])
                        j+=1
                return new_lines,new_pits, new_splits

        def linelength(self,line):
                n = len(line)
                r = 0
                for i in range(n-1):
                        r += self.norm2(line[i],line[i+1])
                return r

        def filterlength(self,lines,rcal):
                rcal = filter_length*rcal
                new_lines = []
                for line in lines:
                        n = len(line)
                        r = 0
                        for i in range(n-1):
                                r += self.norm2(line[i],line[i+1])
                        if r> rcal:
                                0
                        else:
                                new_lines.append(line)
                return new_lines

        def checkmultiplepaths(self,lines):
                n = len(lines)
                simplified_line = []
                new_lines = []
                for line in lines:
                        if len(line)>1:
                                simplified_line.append(sorted([line[0],line[-1]]))
                        else:
                                simplified_line.append([])
                for i in range(n-1):
                        line = simplified_line[i]
                        ctr = 0
                        for j in range(i+1,n):
                                line1 = simplified_line[j]
                                if line == line1:
                                        ctr = 1
                                        break
                        if ctr == 0:
                                new_lines.append(lines[i])

                return new_lines

        def simplifyline(self,line):
                return [line[0],line[-1]]


        def shouldlinebesimple(self,lines):
                new_lines = []
                for line in lines:
                        if len(line)>1:
                                simpleline = self.simplifyline(line)
                                lensimple = self.linelength(simpleline)
                                lencomplex = self.linelength(line)

                                if lencomplex>1.1*lensimple:
                                        new_lines.append(line)
                                else:
                                        new_lines.append(simpleline)
                return new_lines

        def tosplitornottosplit(self, arcs,mapping):
                new_arcs = []
                new_splits = []
                for arc in arcs:
                        if len(arc)>0:
                            point = arc[1]
                            if self.shouldisplit(point,mapping) == 0:
                                    new_arcs.append(arc[0:2])
                                    new_splits.append(point)
                            else:
                                    new_arcs.append(arc)
                return new_arcs,new_splits

        def withinbounds(self,point,xbound,ybound, xend =0,yend=0):

            x = point[0]
            y= point[1]
            if xend == 1:
                if x>=xbound[0] and x<=xbound[1]:
                    if yend ==1:
                        if y>= ybound[0] and y<=ybound[1]:
                            return 1
                    else:
                        if y>= ybound[0] and y<ybound[1]:
                                return 1

            else:
                if x>=xbound[0] and x<xbound[1]:
                    if yend ==1:
                        if y>= ybound[0] and y<=ybound[1]:
                            return 1
                    else:
                        if y>= ybound[0] and y<ybound[1]:
                            return 1
            return 0

        def nearbybounds(self,points,n,m,rcal):
            x = points[0][0]
            y = points[0][1]
            xmax = x
            xmin = x
            ymax = y
            ymin = y

            for point in points:
                x1 = point[0]
                y1 = point[1]
                if x1<xmin:
                    xmin = x1
                elif x1>xmax:
                    xmax = x1

                if y1<ymin:
                    ymin = y1
                elif y1>ymax:
                    ymax= y1


            xbounds =[xmin]
            ybounds = [ymin]

            dx = (xmax-xmin)/float(n)
            dy = (ymax-ymin)/float(m)

            #creat bounds lists

            for i in range(1,n+1):
                xbounds.append(xbounds[-1]+dx)
            xbounds[-1] = xmax + 1
            for j in range(1,m+1):
                ybounds.append(ybounds[-1]+dy)
            ybounds[-1] = ymax + 1

            print "subdivisions identified"
            return xbounds,ybounds

        def printstatus(self,status,i,n, display = ""):
                if n> 1e5:
                        decimals = 4
                else:
                        decimals = 2

                status1 = round(i/float(n),decimals)
                if status1 == status:
                        0
                else:
                        print "Completed %s for %g pct of network" % (display,status1*100)
                        status = status1
                return status

        def buildbox(self,points,points1,n,m,rcal):

            xbounds, ybounds = self.nearbybounds(points,n,m,rcal)

            dx = xbounds[1] - xbounds[0]
            dy = ybounds[1] - ybounds[0]

            fast = 0
            if dy > rcal and dx< rcal:
                    fast = 1
            k = 0
            boxes = []
            rboxes = []

            for i in range(n):
                for j in range(m):
                    boxes.append([])
                    rboxes.append([])

            ind = 0

            rr = len(points)
            status = 0
            counter = 0
            for point in points:
                #status = self.printstatus(status,ind,rr,"boxing")
                #print point
                for i in range(k,n):
                    for j in range(m):
                        ctr = j+ i*m
                        xbound = xbounds[i:i+2]
                        ybound = ybounds[j:j+2]
                        if i == n-1:
                            xend =1
                        else:
                            xend = 0
                        if j == m-1:
                            yend = 1
                        else:
                            yend = 0

                        if self.withinbounds(point,xbound,ybound, xend,yend):
                                boxes[ctr].append(ind)
                                k = i
                                counter= 1
                                break
                    if counter == 1:
                            break
                counter =0

                ind+=1

            k = 0
            ind = 0
            rr = len(points1)
            status = 0
            for point in points1:
                #status = self.printstatus(status,ind,rr,"boxing")
                #print "Point =",point
                for i in range(n):
                    for j in range(m):
                        #print ind,i,j
                        ctr = j+ i*m
                        xrbound = xbounds[i:i+2]
                        yrbound = ybounds[j:j+2]
                        xrbound[0] = xrbound[0]-rcal
                        xrbound[1] = xrbound[1]+rcal
                        yrbound[0] = yrbound[0]-rcal
                        yrbound[1] = yrbound[1]+rcal
                        #print xrbound
                        #print yrbound
                        if i == n-1:
                            xend =1
                        else:
                            xend = 0
                        if j == m-1:
                            yend = 1
                        else:
                            yend = 0
                        if self.withinbounds(point,xrbound,yrbound, xend,yend):
                            rboxes[ctr].append(ind)
                            #jk1 = i
                            #jk2 = j


                ind+=1


            l = 0
            for j in range(ctr):
                l+= len(boxes[j])

            #assert l == len(points)
            #print boxes
            #print
            #print rboxes
            return boxes, rboxes

        def nearbybox(self,points,points1,n,m,rcal,rmin=1e-9):
            print "starting nearby division"
            print "building subdivides"
            boxes,rboxes = self.buildbox(points,points1,n,m,rcal)
            output= []

            print "mapping subdivides"


            for i in range(n*m):
                box = boxes[i]
                rbox = rboxes[i]
                p1 = []
                p2 = []
                for ind in box:
                    p1.append(points[ind])

                for ind in rbox:
                    p2.append(points1[ind])

                print "completing search %g of %g" % (i+1,n*m)
                nearby_index =  self.nearby(p1,p2,rcal,rmin)
                pt = 0
                for indexset in nearby_index:
                    temp = []
                    for ind in indexset:
                        temp.append([ind[0],rbox[ind[1]]])
                    output.append([box[pt],tuple(temp)])
                    pt+=1


            OUTPUT = {output[i][0]:output[i][1] for i in range(len(output))}
            #print output
            return OUTPUT

        def roundlines(self,lines,dec):
            new_lines = []
            for line in lines:
                temp = []
                for point in line:
                    x = round(point[0],dec)
                    y = round(point[1],dec)
                    temp.append([x,y])
                new_lines.append(temp)
                #print temp
            return new_lines

        def simplifylines(self,lines):
            new_lines =[]
            for line in lines:
                new_lines.append(self.simplifyline(line))
            return new_lines


        def main(self):
                ti = time()
                print ("Initialising connectivity protocol")

                arc_index_vertices = self.getpointvertices(self.getLayer("candidates"))


                import os.path


                filepath = os.path.dirname(os.path.abspath(""))

                f = open('D:\GF\RoadSideTest\JAX_sample\RS output' + '\leftright.sol.text','r')
                mult = []
                ptr = 0
                for line in f:
                    if ptr !=0:
                        line = line.rstrip('\n')
                        if line != '[]':
                            line = line.split('[')
                            item1 = []
                            for item in line:
                                item = item.split(']')
                                for it in item:
                                    item1.append(it)
                            line = item1
                            item1 = []
                            for item in line:
                                item = item.split(',')
                                for it in item:
                                    item1.append(it)

                            line = item1
                            item1 = []
                            for item in line:
                                item = item.split(' ')
                                for it in item:
                                    item1.append(it)

                            line = item1
                            item1 = []
                            for item in line:
                                item = item.split('\r')
                                for it in item:
                                    item1.append(it)

                            line = item1
                            item1 = []
                            for item in line:
                                item = item.split('x_')
                                for it in item:
                                    item1.append(it)

                            line = item1
                            temp = []
                            for item in line:
                                if item == '[':
                                    0
                                elif item == ']':
                                    0
                                elif item == ',':
                                    0
                                elif item == '':
                                    0
                                elif item == 'r':
                                    0
                                else:
                                    #print item
                                    temp.append(int(item))
                            mult.append(temp)
                    ptr +=1

                solution = sorted(mult)
                active_pits = []
                pits_index = arc_index_vertices
                print len(solution),len(pits_index)

                for i in range(len(pits_index)):
                    print i, solution[i][0]
                    assert i == solution[i][0], "%g, %g" % (i,solution[i][0])

                    pit = pits_index[i]
                    use = solution[i][1]
                    if use == 1:
                        active_pits.append(pit)
                    else:
                        active_pits.append([])

                self.linestopoints([active_pits],"roadside")



                print "time taken = %g" % (time()-ti)

                print "done"



test = pit2pit()
test.main()
