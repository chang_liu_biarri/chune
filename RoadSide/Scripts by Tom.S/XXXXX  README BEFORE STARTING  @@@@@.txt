Those scripts were modefied slightly, with some extra comments/different path.
They have to be run in the correct order, as 2 of the scripts are dependant on others.


Step 1
-- Run leftright_demand1.py  in qgis project with python console

Check the script. There are three layers required, trench, crossings and demand_nodes.
Never heard of any GF output layers called 'trench', or 'crossings'?  Correct. These are output from a model (trench_and_crossings.model), created by Jack. 

Remember to set your filepath to whatever you want. Outputs will be saved there.

Step 2

-- Run leftright_optimise_demand.py

Tom's mixed integer programming script, to generate a Gurobi-readable lp file, a problem of finding solution of a linear function, with linear constraints.

You do not need to understand the script, or linear programming. However, you need pulp to run the script.

According to AlexB, the following is the proper way of getting pulp inside VMs.

2.1) Make a copy of test_preprocessor_deploy (GF solver). Lets name the saved copy 'GF_test'.
2.2) Delete the 'env' folder.
2.3) Replace the install.bat with the one inside this(CHUNE/RoadSide/Scripts by Tom.S) folder.
2.4) Run the updated install.bat.
2.5) Right click inside your filepath (the one that contains Step 1 outputs), to open GF_test environment.

Command prompt is opened? Nice. 

--- Type the following command after the '>' sign
#################################
> Python left_right_optimise.py
#################################

You will see an Error

NameError: name 'thomas' is not defined.

Do not worry. This is just a test print to see if the function is running properly. 


In your filepath, a 'leftright.lp' file will be created. 


--- Finally, type the following command after '>'
#############################################################
> gurobi_cl ResultFile="leftright.sol" "leftright.lp"
#############################################################

Step 3

--- From last step, a 'leftright.sol' is created. Open it with a text editor, and save it as a .txt file, like 'leftright.sol.txt'.

--- Back to Qgis project, run the last script, leftright_solution3.py.

It will use a layer called 'candidates', created at Step 1.
###############################  ATTENTION!!! #########################
One thing to notice, search for 'filepath' inside the script, and you can see

### f = open('D:\GF\RoadSideTest\JAX_sample\RS output' + '\leftright.sol.text','r')  ###

Replace the location inside the first quote by your filepath, the stuff in the second quote by your text file name.

--- The output is called 'roadside', a point layer.

Have a look. for any street, points should always follow one side. Which side? Depends on how many demands, and the settings of Tom's cost minimizing lp.

--- Last thing, run 'make_penalty_edges.model', created by Jack, to get penalty edges.


roadside layer will be used as preference hubs, along with penalty edges, during SOLVE.

Crossing multiplier should be something between -0.5 to +0.5. I am not sure the exact number to use. Please test it. 