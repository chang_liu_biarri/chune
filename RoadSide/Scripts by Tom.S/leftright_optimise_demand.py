# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 23:17:40 2015

@author: thomasalexanderstubbs
"""
from pulp import * 

f = open('mult.txt','r')
mult = []
for line in f:
    line = line.rstrip('\n')
    if line != '[]':
        line = line.split('[')
        item1 = []
        for item in line:
            item = item.split(']')
            for it in item:
                item1.append(it)
        line = item1
        item1 = []  
        for item in line:
            item = item.split(',')
            for it in item:
                item1.append(it)
        line = item1
        temp = []   
        for item in line:
            if item == '[':
                0
            elif item == ']':
                0
            elif item == ',':
                0
            elif item == '':
                0
            else:
                #print item
                temp.append(float(item))
        mult.append(temp)
    #print line


f = open('span_constraints.txt','r')
span = []
for line in f:
    line = line.rstrip('\n')
    if line != '[]':
        line = line.split('[')
        item1 = []
        for item in line:
            item = item.split(']')
            for it in item:
                item1.append(it)
        line = item1
        item1 = []  
        for item in line:
            item = item.split(',')
            for it in item:
                item1.append(it)
        line = item1
        temp = []   
        for item in line:
            if item == '[':
                0
            elif item == ']':
                0
            elif item == ',':
                0
            elif item == '':
                0
            else:
                #print item
                temp.append(float(item))
        span.append(temp)
        
f = open('demand.txt','r')
demand = []
for line in f:
    line = line.rstrip('\n')
    if line != '[]':
        line = line.split('[')
        item1 = []
        for item in line:
            item = item.split(']')
            for it in item:
                item1.append(it)
        line = item1
        item1 = []  
        for item in line:
            item = item.split(',')
            for it in item:
                item1.append(it)
        line = item1
        temp = []   
        for item in line:
            if item == '[':
                0
            elif item == ']':
                0
            elif item == ',':
                0
            elif item == '':
                0
            else:
                #print item
                temp.append(float(item))
        demand.append(temp)
        

f = open('crossing_constraints.txt','r')
crossing = []
for line in f:
    line = line.rstrip('\n')
    if line != '[]':
        line = line.split('[')
        item1 = []
        for item in line:
            item = item.split(']')
            for it in item:
                item1.append(it)
        line = item1
        item1 = []  
        for item in line:
            item = item.split(',')
            for it in item:
                item1.append(it)
        line = item1
        temp = []   
        for item in line:
            if item == '[':
                0
            elif item == ']':
                0
            elif item == ',':
                0
            elif item == '':
                0
            else:
                #print item
                temp.append(float(item))
        crossing.append(temp)
        
f = open('crossing_cost.txt','r')
crossing_cost = []
for line in f:
    line = line.rstrip('\n')
    if line != '[]':
        line = line.split('[')
        item1 = []
        for item in line:
            item = item.split(']')
            for it in item:
                item1.append(it)
        line = item1
        item1 = []  
        for item in line:
            item = item.split(',')
            for it in item:
                item1.append(it)
        line = item1
        temp = []   
        for item in line:
            if item == '[':
                0
            elif item == ']':
                0
            elif item == ',':
                0
            elif item == '':
                0
            else:
                #print item
                temp.append(float(item))
        crossing_cost.append(temp)

costs = []
register = []
assert len(span) == len(demand)
print demand 
for item in demand:
    if item[0] == 0:
        costs.append(0.01)
    else:
        costs.append(-0.1**item[0])
register.append(len(costs))


dummy_var = 0
for item in mult:
    if item[0] == 1:
        dummy_var+=2
        costs.append(1)
        costs.append(1)

    else:
        dummy_var+=1
        costs.append(2)
register.append(len(costs))

ptr = 0
for item in crossing:
    crossing_number = max(crossing_cost[ptr])
    if crossing_number > 1:
        costs.append(0)
    else:
        costs.append(6)
    ptr+=1
register.append(len(costs))


assert len(mult) == len(span)

index1 = []
index2 = []
index3 = [] 
for i in range(len(costs)):
    ind = '%07g' % i
    #print ind, int(ind)
    index1.append(ind)
    index2.append(i)    
    
for i in range(len(mult)):
    ind = '%07g' % i
    #print ind, int(ind)
    index3.append(ind)

#print index1
#print costs
# Create the 'prob' variable to contain the problem data
prob = LpProblem("Pit placement",LpMinimize)

# The 2 variables Beef and Chicken are created with a lower limit of zero
x = pulp.LpVariable.dicts("x_%s", index1 , 0, 1, LpInteger)
#print x
# The objective function is added to 'prob' first

#prob += sum([costs[int(i)]*x[i] for i in index])
temp = []
for i in index1:
    if 1:# int(i)>=register[0]:
        temp.append(costs[int(i)]*x[i])
        #print "cost",costs[int(i)],i
    #print i
    #temp.append(1.0*x[i])

    #print i
    
#prob += sum([costs[int(i)]*x[i] for i in index])
#print x
cost1 = lpSum(temp)
prob+= cost1

# The five constraints are entered
done = []
ctr = register[0]


for i in range(register[0]):
    sp = span[i]
    temp = 0
    #print sp
    for j in range(1,len(sp)):
        ind1 = '%07g' % sp[0]
        ind2 = '%07g' % sp[j]
        ind3 = '%07g' % ctr

        #print "span",ind1,ind2,ind3
        prob+= sum([x[ind1],-1.0*x[ind2], -1.0*x[ind3]]) <= 0
        prob+= sum([x[ind2],-1.0*x[ind1], -1.0*x[ind3]]) <= 0

        #prob+= sum([x[ind1],-1.0*x[ind2]]) == 0
        ctr+=1


"""
for i in range(register[0]):
    sp = span[i]
    temp = 0
    print sp
    for j in range(1,len(sp)):
        ind1 = '%07g' % sp[0]
        ind2 = '%07g' % sp[j]
        ind3 = '%07g' % ctr
        
        ctr+=1
"""

print ctr, register

#assert ctr == register[1]
ptr = 0
ctr = register[1]

for i in range(ctr,register[2]):
    #print "i",i
    cross = crossing[ptr]

    if len(cross) >1:
        ind1 = '%07g' % cross[0]
        ind2 = '%07g' % cross[1]
        ind3 = '%07g' % i
        print ind1,ind2,ind3,register[2]-ctr
        prob+= sum([x[ind1],x[ind2],-1.0*x[ind3]]) == 1.0
    ptr+=1
 
#print prob 
# The problem data is written to an .lp file
prob.writeLP("leftright.lp")
#solver = solvers.COIN(1,1, 1, 1, 1, 0, 1, 1, 1, 5,600, None)

prob.setSolver()

# The problem is solved using PuLP's choice of Solver
print thomas
prob.solve()


# The status of the solution is printed to the screen
print "Status:", LpStatus[prob.status]

# Each of the variables is printed with it's resolved optimum value
solution = [] 
for i in range(len(costs)):
    solution.append(0)
    
i = 0
for v in prob.variables():
    print v.name, "=", v.varValue
    index = v.name
    solution[i] = (v.varValue)
    i+=1

ptr = 0
with open('leftright_solution.txt', 'w') as file:
    for item in solution:
        if ptr<len(mult):
            file.write("{}\n".format(item))
        ptr+=1
        
    
# The optimised objective function value is printed to the screen
print "Total Cost of the solution = ", value(prob.objective)

        